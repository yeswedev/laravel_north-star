<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

        /* -- General | Not authenticated -- */

Route::get('/', [ 'uses' => 'WelcomeController@show', 'as' => 'login' ]);

        /* --  Authenticated Routes-- */

/**
 * Laravel Authentication
 */
Auth::routes();

/**
 * Registration pages
 */
Route::get('/registration/step-1', [ 'uses' => 'RegistrationStep1Controller', 'as' => 'registration' ]);
Route::group( [ 'middleware' => 'auth'], function () {

    Route::put('/registration/step-1/edit', [ 'uses' => 'EditRegistrationStep1Controller@save', 'as' => 'edit-registration-step-1' ]);

    Route::put('/registration/step-2/edit', [ 'uses' => 'EditRegistrationStep2Controller@save', 'as' => 'edit-registration-step-2' ]);

    Route::get('/registration/step-2', [ 'uses' => 'RegisterCompanyController@show', 'as' => 'registration-2' ]);

    Route::put('/registration/step-2', ['uses' => 'RegisterCompanyController@save', 'as' => 'company-registration']);

    Route::get('/registration/step-3', ['uses' => 'RegistrationStep3Controller@show', 'as' => 'registration-3']);

    Route::put('/registration/step-3', ['uses' => 'RegistrationStep3Controller@save', 'as' => 'confirmation-registration']);

    Route::get('/subscription', ['uses' => 'SubscriptionController@show', 'as' => 'subscription']);
    /**
     * User Edit pages
     */
    Route::get('/user/edit', ['uses' => 'EditUserController@show', 'as' => 'user-edit']);
    Route::put('/user/edit', ['uses' => 'EditUserController@save', 'as' => 'save-edit']);

});

                /* --  Club  -- */

Route::group( [ 'middleware' => 'subscribed', 'namespace' => 'Club'], function () {
    /**
     * Account
     */
    Route::get('/user/account', ['uses' => 'MemberController@show', 'as' => 'home']);
    Route::post('/user/bill',['uses' => 'MemberController@uploadBill', 'as' => 'bill']);

    /**
     * Contacts
     */
    Route::get('/user/contacts', ['uses' => 'ContactController@show', 'as' => 'contacts']);
    Route::post('/user/saler',['uses'=>'ContactController@displayContactFromZip', 'as' => 'saler']);

    /**
     * Shop pages
     */
    Route::get('/shop', ['uses' => 'ShopController@show', 'as' => 'shop']);

    /**
     * Cart pages
     */
    Route::get('/cart', ['uses' => 'CartController@show', 'as' => 'cart']);

});


/**
 * Laravel CMS
 */
Route::get('/pages/{slug}', 'PageController@show');


//Static Pages

Route::get('/user/update/password', function(){
    return view('user.password-update');
})->name('update-password');

/**
 * Redirect views generated with Spark we don't use
*/
Route::get('/login', function(){
    return redirect()->route('login');
});
Route::get('/settings',function(){
    return redirect()->route('home');
});

