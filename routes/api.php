<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register the API routes for your application as
| the routes are automatically authenticated using the API guard and
| loaded automatically by this application's RouteServiceProvider.
|
*/

Route::group([
    'middleware' => 'auth:api'
], function () {
    /**
     * Cart requests
     */
    // Load all Products in User Cart
    Route::get('/cart/products', 'Api\CartController@getProducts');
    // Add Product to User Cart
    Route::post('/cart/product', 'Api\CartController@updateCart');
    // Update Product to User Cart
    Route::put('/cart/product', 'Api\CartController@updateCart');
    // Delete Product from User Cart
    Route::put('/cart/product/delete', 'Api\CartController@deleteProduct');
    // Delete all Products from User Cart
    Route::put('/cart/products/delete', 'Api\CartController@deleteAllProducts');
    // Validate Cart
    Route::put('/cart/validate', 'Api\CartController@validateOrder');
    /**
     * User requests
     */
    // Get current User availabledPoints
    Route::get('/user/points', 'Api\UserController@getAvailabledPoints');
});