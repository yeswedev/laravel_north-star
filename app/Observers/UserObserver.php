<?php

namespace App\Observers;

use App\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {
        if ( $user->role_id == 3 ) {
            // Create Saler
            DB::table('salers')->insert([
                [
                    'user_id' => $user->id,
                    'title' => $user->firstname . ' ' . $user->lastname,
                    'created_at' => date('Y-m-d'),
                    'updated_at' => date('Y-m-d'),
                ],
            ]);
            $user->trial_ends_at='2035-01-01';
            $user->save();
        }
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        if ( $user->role_id == 3 ) {
            // Create Saler
            DB::table('salers')->where('user_id', '=', $user->id)->delete();
        }
    }

    /**
     * Handle the user "restored" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
