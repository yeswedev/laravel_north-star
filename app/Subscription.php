<?php

namespace App;

use Laravel\Spark\Subscription as SparkSubscription;
use Illuminate\Database\Eloquent\Model;
//use App\Mail\NewSubscription;



class Subscription extends SparkSubscription
{
    protected $attributes = [
        'user_id'       => null,
        'ends_at'       => null,
        'updated_at'    => null,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'ends_at',
        'updated_at',
    ];

    /**
     * Relationship BelongsTo User
     *
     * @return User Collection
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

}
