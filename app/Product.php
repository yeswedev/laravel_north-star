<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $attributes = [
      'name'    => null,
      'image'   => null,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name',
      'image',
    ];

    /**
     * Get the product variants link to the instance
     */
    public function variant(){
      return $this->hasMany('App\Variant');
    }
}
