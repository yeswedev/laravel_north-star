<?php

namespace App\Http\Middleware;

use Laravel\Spark\Http\Middleware\VerifyUserIsSubscribed as Middleware;

class VerifyUserIsSubscribed extends Middleware
{
    /**
     * Verify the incoming request's user has a subscription.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $subscription
     * @param  string  $plan
     * @return \Illuminate\Http\Response
     */
    public function handle($request, $next, $subscription = 'default', $plan = null)
    {
        if ($this->subscribed($request->user(), $subscription, $plan, func_num_args() === 2)) {
            return $next($request);
        }

        return $request->ajax() || $request->wantsJson()
                                ? response('Subscription Required.', 402)
                                : redirect()->route('subscription');
    }
}
