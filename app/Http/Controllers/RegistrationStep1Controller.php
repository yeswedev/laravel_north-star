<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegistrationStep1Controller extends Controller
{

    
    /**
     * Show registration step 1
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function __invoke()
    {
        // In case of edit
        $user_full = Auth::user();
        if ( $user_full !== null ) {
            return view('registration.registration-step-1', [ 'user_full' => $user_full ]);
        } else {
            return view('registration.registration-step-1');
        }
    }


}
