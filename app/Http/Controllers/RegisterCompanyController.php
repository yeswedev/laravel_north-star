<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Traits\HasMemberValidation;

class RegisterCompanyController extends Controller
{

    use HasMemberValidation;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show()
    {
        $user_full = Auth::user();
        $sizes = [
            'L',
            'XL'
        ];
        return view('registration.registration-step-2', [ 
            'user_full' => $user_full,
            'sizes' => $sizes,
        ]);
    }

    /**
     * Validate regstration step 2 datas
     *
     * @return redirect view register step 3
     */
    public function save(Request $request)
    {
        $request->flash();
        $user = Auth::user();

        $validator = $this->validateCompanyDatas($request);
        if ( $validator->fails() ) {
            return redirect()->route('registration-2')->withInput()->withErrors( $validator);
        }
        
        $data=$request->all();
        $user->update($data);
        $user->setDepartment();
        return redirect()->route('registration-3');
    }
}
