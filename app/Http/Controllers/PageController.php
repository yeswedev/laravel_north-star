<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class PageController extends Controller
{
    /**
     * Show the page
     *
     * @param [type] $slug
     * @return void
     */
    public function show($slug)
    {
        $page = Page::where('slug', $slug)->first();
        if ($page && $page->published === 1) {
            return view('pages.page', [
                'page' => $page,
            ]);
        }

        // No match was found
        abort(404);
    }
}
