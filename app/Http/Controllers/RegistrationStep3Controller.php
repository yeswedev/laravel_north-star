<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class RegistrationStep3Controller extends Controller
{
    /**
     * Show the register step 3 page (datas validation).
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show()
    {
    	$user = Auth::user();
        if($user->billing_address == null){
            return redirect()->route('registration-2');
        }
        return view('registration.registration-step-3',
        	[
        		'user' =>$user
        	]
        );
    }

    /**
     * Validate regstration step 3 datas
     *
     * @return redirect view subscription
     */
    public function save(Request $request)
    {
        $user = Auth::user();

        $validator = Validator::make( $request->all(),
        	[
        		'cvg_agrement'=>'required'
        	],
        	[
        		'cvg_agrement.required'=>'Le champs <span>Raison Sociale</span> est obligatoire.']);
        $data=$request->all();
        if($data['cgv_agrement']=='on'){
        	$user->cgv_agrement=1;
        	$user->update();
        	return redirect()->route('subscription');
        }else{
        	return Redirect::back()->withErrors(['msg', 'Votre consentement est requis']);
        }
    }
}
