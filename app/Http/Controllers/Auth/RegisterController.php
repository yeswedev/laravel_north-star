<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Get the path the user should be redirected to when they are registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo()
    {
        return route('registration-2');
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {        
        if (array_key_exists('title', $data)) {
            Session::put('title', $data['title']);
        }
        if (array_key_exists('lastname', $data)) {
            Session::put('lastname', $data['lastname']);
        }
        if (array_key_exists('firstname', $data)) {
            Session::put('firstname', $data['firstname']);
        }
        if (array_key_exists('birth', $data)) {
            $birth = date('Y-m-d', strtotime($data['birth']));
            Session::put('birth', $birth);
        }
        if (array_key_exists('function', $data)) {
            Session::put('function', $data['function']);
        }
        if (array_key_exists('phone', $data)) {
            Session::put('phone', $data['phone']);
        }
        if (array_key_exists('mobile', $data)) {
            Session::put('mobile', $data['mobile']);
        }
        if (array_key_exists('email', $data)) {
            Session::put('email', $data['email']);
        }

        $role=$data['user_role']=="member"?0:1;
        if($role==0){
            return Validator::make($data, $this->frontUserDatas, $this->messages);

            /*return Validator::make($data, [
                $this->frontUserDatas
            ]);*/
        }else{
            return Validator::make($data, [
                'email'     =>  ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password'  =>  ['required', 'string', 'min:9', 'regex:/[^\d]*((\d[^\d]*){3,}$)/', 'confirmed'],
            ]);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if($data['user_role']=="member"){
            $data['password']=Hash::make($data['password']);
            $data['name']=$data['firstname'].' '.$data['lastname'];
            $user=User::create($data);
            $user=$user->setDepartment();
            $user=$user->setRole();
            return $user;
        }else{
            return User::create([
                'name'      =>  'prov',
                'email'     =>  $data['email'],
                'password'  =>  Hash::make($data['password']),
            ]);
        }
    }

    protected $frontUserDatas = [
        'title'     =>  ['required', 'string', 'max:255'],
        'firstname' =>  ['required', 'string', 'max:255'],
        'lastname'  =>  ['required', 'string', 'max:255'],
        'function'  =>  ['required', 'string', 'max:255'],
        'birth'     =>  ['required', 'date', 'max:255'],
        'phone'     =>  ['digits:10', 'nullable'],
        'mobile'    =>  ['required', 'digits:10'],
        'email'     =>  ['required', 'string', 'email', 'max:255', 'unique:users'],
        'password'  =>  ['required', 'string', 'min:9', 'regex:/[^\d]*((\d[^\d]*){3,}$)/', 'confirmed'],
    ];

    private $messages = [
        'required'              => 'Le champs <span> :attribute </span>est obligatoire.',
        'email.required'        => 'Le champs <span>Adresse mail</span> est obligatoire.',
        'email.unique'          => 'L\' <span>Adresse mail</span> renseignée est déjà utilisée.',
        'email.email'           => 'Le format du champs <span>Adresse mail</span> est incorrect.',
        'password.required'     => 'Le champs <span>Mot de passe</span> est obligatoire.',
        'password.confirmed'    => 'Le champs <span>Confirmation mot de passe</span> ne correspond pas au <span>Mot de passe</span> entré.',
        'password.min'          => 'Pour des raison de sécurité, votre <span>mot de passe</span> doit comporter au moins 9 caractères dont minimum 3 chiffres.',
        'password.regex'        => 'Le champs <span>mot de passe doit comporter au minimum 3 chiffres.</span>',
        'mobile.min'             => 'Le champs <span> téléphone mobile </span> doit contenir 10 chiffres.',
        'phone.digits'             => 'Le champs <span> téléphone fixe </span> doit contenir 10 chiffres.',
        'mobile.digits'             => 'Le champs <span> téléphone mobile </span> doit contenir 10 chiffres.',
    ];

}
