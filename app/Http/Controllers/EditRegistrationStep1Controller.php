<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Traits\HasMemberValidation;

class EditRegistrationStep1Controller extends Controller
{

    use HasMemberValidation;

    /**
     * Validate regstration step 1 datas
     *
     * @return redirect view register step 2
     */
    public function save(Request $request)
    {
        $user = Auth::user();

        $validator = Validator::make( $request->all(),
            [
            'title'     =>  ['required', 'string', 'max:255'],
            'firstname' =>  ['required', 'string', 'max:255'],
            'lastname'  =>  ['required', 'string', 'max:255'],
            'function'  =>  ['required', 'string', 'max:255'],
            'birth'     =>  ['required', 'date', 'max:255'],
            'phone'     =>  ['required', 'string', 'max:255'],
            'mobile'    =>  ['required', 'string', 'max:255'],
            'email'     =>  ['required', 'string', 'email', 'max:255', 'unique:users'],
            ]
        );

        $data=$request->all();

        $user->update($data);

        return redirect()->route('registration-2');
    }
}
