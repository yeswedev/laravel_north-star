<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Get Available Points from current user
     *
     * @return Object
     */
    public function getAvailabledPoints()
    {
        $user = Auth::user();
        $points = $user->getAvailabledPoints();
        return json_encode($points);
    }
}
