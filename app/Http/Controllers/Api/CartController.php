<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Variant;

class CartController extends Controller
{
    /**
     * Get user Cart
     *
     * @return Object
     */
    public function getCart()
    {
      $user = Auth::user();
      $cart = $user->getCurrentOrder();
      return json_encode($cart);
    }

    /**
     * Get products in user Cart
     *
     * @return Object
     */
    public function getProducts()
    {
      $user = Auth::user();
      $cart = $user->getCurrentOrder();
      $variants = $cart->variant;
      $products = [];

      foreach ($variants as $variant) {
        $products[] = [
          "product" => [
            "id" => $variant->id,
            "name" => $variant->getCompleteName(),
            "price" => $variant->points,
            "stock" => $variant->stock,
            "size" => $variant->name,
            "img" => [
              "url" => $variant->product->image,
              "alt" => "",
            ],
          ],
          "quantity" => $variant->order_variant->quantity,
        ];
      }

      return json_encode($products);
    }

    /**
     * Gère les requêtes et mets à jour le panier
     *
     * POST: add a product
     * PUT: update product quantity
     *
     * @param Request $request
     */
    public function updateCart(Request $request)
    {
      $user = Auth::user();
      $cart = $user->getCurrentOrder();

      if ( $cart != null ) {
        $id = $request->get('id');
        $product = Variant::find($id);

        if ( $request->method() == 'POST' ) { // Add product
          $quantity = $request->get('quantity');
          $cart->addProduct($product, $quantity);

        } elseif ( $request->method() == 'PUT' ) { // Update product quantity
          $quantity = null;
          $newQty = $request->get('newQty');
          $oldQty = $cart->getVariantQuantity($product);
          switch ($newQty) {
            case 'down':
              $quantity = $oldQty - 1;
            break;
            case 'up':
              $quantity = $oldQty + 1;
            break;
          }
          $cart->setVariantQuantity($product, $quantity);

        } else {
          return;

        }
      }

      return;
    }


    /**
	 * Delete one product from cart
	 *
     * @param Request $request
	 */
	public function deleteProduct(Request $request) {
	    $user = Auth::user();
      $cart = $user->getCurrentOrder();

      if ( $cart != null ) {
          $id = $request->get('id');
          $product = Variant::find($id);
          $cart->removeProduct($product);
      }

		return;
	}


  /**
	 * Delete all products from cart
	 */
	public function deleteAllProducts() {
      $user = Auth::user();
      $cart = $user->getCurrentOrder();

      if ( $cart != null ) {
          $cart->removeAllProducts();
      }

		return;
  }


  /**
   * Validate Order
   *
   * @return void
   */
  public function validateOrder() {
      $user = Auth::user();
      $cart = $user->getCurrentOrder();
      $result = false;
      if ( $cart != null ) {
        $result=$cart->validateOrder();
      }
      if($result){
        return json_encode('ok');
      }
  }


}
