<?php

namespace App\Http\Controllers\Club;

use App\Saler;
use Illuminate\Http\Request;
use App\Traits\HasDepartment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    use HasDepartment;

    /**
     * Show the contacts page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($zip=null)
    {
        if(empty($zip)){        
            $user = Auth::user();
            $zip = $user->company_zipcode;
            $user = $user->setDepartment();
            $dpt = $user->department;
        }
        else{
            $dpt = $this->deduceDepartment($zip);
        }
        $saler=$this->getSalerFromDepartment($dpt);
        return view('user.contacts',
          [
            'zip' =>$zip,
            'saler' => $saler,
          ]
      );
    }

    /**
     * Display Contact after submitting form
     *
     * @param Request
     * @return view Contact
     */
    public function displayContactFromZip(Request $request)
    {
        $request->flash();

        $rules = [
            'zipcode' => ['required', 'digits:5'],
        ];
        $messages = [
            'zipcode.required'      => 'Le champs <span>code postal</span> est obligatoire.',
            'zipcode.digits'        => 'Le champs <span>code postal</span> doit contenir 5 chiffres.',
        ];
        $validator = Validator::make( $request->all(), $rules, $messages );

        if ( $validator->fails() ) {
            return redirect()->route('contacts')->withInput()->withErrors( $validator);
        }

        $zip = $request->input('zipcode');
        return $this->show($zip);
    }
}
