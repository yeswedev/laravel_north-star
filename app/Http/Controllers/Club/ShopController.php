<?php

namespace App\Http\Controllers\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Variant;
use Illuminate\Support\Facades\Auth;

class ShopController extends Controller
{
    /**
     * Show the shop page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show()
    {
        $user = Auth::user();
        $products=Variant::all();
        return view('shop.shop',
          [
            'products'=>$products,
            'user' =>$user
          ]
        );
    }
}
