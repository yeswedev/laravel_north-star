<?php

namespace App\Http\Controllers\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Variant;

class CartController extends Controller
{
    /**
     * Show the shop page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show()
    {
        $user = Auth::user();
        $cart = $user->getCurrentOrder();
        return view('cart.cart',
          [
            'cart' => $cart
          ]
        );
    }
}
