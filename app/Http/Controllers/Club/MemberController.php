<?php

namespace App\Http\Controllers\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\UploadedFile;
use App\Bill;
use Illuminate\Support\Facades\Validator;


class MemberController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show()
    {
        $user = Auth::user();
        return view('user.account',
          [
            'user' =>$user
          ]
        );
    }

    /**
     *  Upload bill from club member account
     *
     * @return view account
     */
    public function uploadBill(Request $request)
    {
        $user = Auth::user();

        $rules = [
            'bill' => ['required', 'file', 'mimes:doc,pdf,jpeg,jpg,png,odt', 'max:1000'],
        ];
        $messages = [
            'bill.required'      => "Vous devez choisir un fichier.",
            'bill.file'          => "L'élément téléchargé doit être un fichier.",
            'bill.mimes'         => "Formats de fichiers autorisés : doc, pdf, jpeg, png, odt.",
            'bill.max'          => 'Le fichier ne doit pas excéder 1mo.'
        ];
        $validator = Validator::make( $request->all(), $rules, $messages );

        if ( $validator->fails() ) {
            return redirect()->back()->withInput()->withErrors( $validator);
        }

        $file =$request->file('bill');
        if($file->isValid()){
            $path = $request->bill->store('', 'bills');
            $bill = new Bill(
                [
                    'doc' => $path,
                    'status' => 0
                ]
            );
            $bill->user()->associate($user);
            $bill->save();
        }else{
            return Redirect::back()->withErrors(['msg', 'Le téléchargement de votre facture a échoué, merci de télécharger un fichier valide']);
        }
        return redirect()->back()->with('success', ['Le téléchargement a réussit, nos équipes vont se charger de traiter la facture pour vous affecter vos points']);
    }
}
