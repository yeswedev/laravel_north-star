<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\HasMemberValidation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;


class EditUserController extends Controller
{

    use HasMemberValidation;

    /**
     * Show the register step 3 page (datas validation).
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show()
    {
      $user = Auth::user();
        return view('user.edit',
          [
            'user' =>$user
          ]
        );
    }

    /**
     * Validate edit step 1 datas
     *
     * @return redirect view subscription
     */
    public function save(Request $request)
    {
      $user = Auth::user();
      $data = $request->all();

      if ($user->email === $data['email']) {
        $validator = Validator::make( $request->all(),
          [
            'title'     =>  ['required', 'string', 'max:255'],
            'firstname' =>  ['required', 'string', 'max:255'],
            'lastname'  =>  ['required', 'string', 'max:255'],
            'function'  =>  ['required', 'string', 'max:255'],
            'birth'     =>  ['required', 'date', 'max:255'],
            'phone'     =>  ['required', 'string', 'max:255'],
            'mobile'    =>  ['required', 'string', 'max:255'],
          ]
        );
      } else {
        $validator = Validator::make( $request->all(),
          [
            'title'     =>  ['required', 'string', 'max:255'],
            'firstname' =>  ['required', 'string', 'max:255'],
            'lastname'  =>  ['required', 'string', 'max:255'],
            'function'  =>  ['required', 'string', 'max:255'],
            'birth'     =>  ['required', 'date', 'max:255'],
            'phone'     =>  ['required', 'string', 'max:255'],
            'mobile'    =>  ['required', 'string', 'max:255'],
            'email'     =>  ['string', 'email', 'max:255', 'unique:users'],
          ]
        );
      }      

      $validator_company = $this->validateCompanyDatas($request);
      if ( $validator->fails() ) {
          return redirect()->route('user-edit')->withInput()->withErrors( $validator);
      }

      $data=$request->all();

      $user->update($data);
      return redirect()->route('home');
    }

}
