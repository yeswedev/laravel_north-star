<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Traits\HasMemberValidation;

class EditRegistrationStep2Controller extends Controller
{

    use HasMemberValidation;

    /**
     * Validate regstration step 2 datas
     *
     * @return redirect view register step 3
     */
    public function save(Request $request)
    {
        $user = Auth::user();

        $validator = $this->validateCompanyDatas($request);
        if ( $validator->fails() ) {
            return redirect()->route('registration-step-2')->withInput()->withErrors( $validator);
        }

        $data=$request->all();
        $user->update($data);
        $user->setDepartment();
        return redirect()->route('registration-3');
    }
}
