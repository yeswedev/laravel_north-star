<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Carbon;
use App\Traits\Refreshable;
use App\Traits\HasPoints;
use App\Traits\HasStatus;

use Illuminate\Support\Facades\Mail;
use App\Mail\BillValidation;

class Bill extends Model
{

    use Refreshable;
    use HasPoints;
    use HasStatus;

    protected $attributes = [
        'user_id'               => null,
        'status'                => null,
        'doc'                   => null,
        'points'                => null,
        'nb_product_UD'         => null,
        'nb_product_UD_plus'    => null,
        'nb_product_NSC_hybrid' => null,
        'amount'                => null,
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'status',
        'doc',
        'points',
        'amount',
        'nb_product_UD',
        'nb_product_UD_plus',
        'nb_product_NSC_hybrid',
    ];

    /**
     * Get the user which it belongsTo bill
     *
     * @return User
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the bill uploaded date.
     *
     * @param  string  $value
     * @return datetime
     */
    public function getCreatedAtAttribute($value)
    {
        return (new Carbon($value))->format('d/m/Y');
    }

    /**
     * Get readable status
     *
     * @return string
     */
    public function getReadableStatusAttribute()
    {
        switch($this->status){
            case 1 :
                $state='validée';
            break;
            case 2 :
                $state='réjétée';
            break;
            default :
                $state='En attente de Validation';
        }
        return $state;
    }


    /**
     * Validate Bill
     *
     * @return Bill or false
     */
    public function validateBill() {
        if($this->points>0){
            $this->setStatus(1);
            $this->sendBillValidationMail();
            return $this->getFreshInstance();
        }
        return false;
    }

    /**
     * Reject Bill
     *
     * @return Bill
     */
    public function rejectBill() {
        $this->setPoints(0);
        $this->setStatus(2);
        $this->sendBillValidationMail();
        return $this->getFreshInstance();
    }

    /**
     * Send bill validation mail
     *
     * @return void
     */
    public function sendBillValidationMail(){
        Mail::to($this->user->email)->send(new BillValidation($this));
    }
}
