<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $attributes = [
      'id'    => null,
      'name'   => null,
      'saler_id' => null,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'id',
      'name',
      'saler_id',
    ];

    /**
     * Get the saler link to the department
     */
    public function saler(){
      return $this->belongsTo('App\Saler');
    }

    public function user() {
      return $this->hasMany('App\User');
    }
    
    /**
     * Get Saler Title
     *
     * @return string
     */
    public function getSalerTitle() {
      if ($this->saler()->first() != null) {
        return $this->saler()->first()->title;
      }
    }

    public $incrementing = false;
}
