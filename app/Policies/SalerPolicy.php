<?php

namespace App\Policies;

use App\User;
use App\Saler;
use Illuminate\Auth\Access\HandlesAuthorization;

class SalerPolicy
{
    use HandlesAuthorization;

    /**
     *  determine whether a user can view any department
     *  @param User $user
     *  @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can view the saler.
     *
     * @param  \App\User  $user
     * @param  \App\Saler  $saler
     * @return mixed
     */
    public function view(User $user, Saler $saler)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can create salers.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can update the saler.
     *
     * @param  \App\User  $user
     * @param  \App\Saler  $saler
     * @return mixed
     */
    public function update(User $user, Saler $saler)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can delete the saler.
     *
     * @param  \App\User  $user
     * @param  \App\Saler  $saler
     * @return mixed
     */
    public function delete(User $user, Saler $saler)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can restore the saler.
     *
     * @param  \App\User  $user
     * @param  \App\Saler  $saler
     * @return mixed
     */
    public function restore(User $user, Saler $saler)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can permanently delete the saler.
     *
     * @param  \App\User  $user
     * @param  \App\Saler  $saler
     * @return mixed
     */
    public function forceDelete(User $user, Saler $saler)
    {
        return $user->hasRole('admin');
    }
}
