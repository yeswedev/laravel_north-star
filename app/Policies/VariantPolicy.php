<?php

namespace App\Policies;

use App\User;
use App\Order;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Variant;

class VariantPolicy
{
    use HandlesAuthorization;

    public function attachAnyOrder(User $user, Variant $variant) {
        return false;
    }

    public function view(User $user, Variant $variant) {
        return true ;
    }

    public function create(User $user) {
        return $user->hasRole('admin');
    }

    public function update(User $user, Variant $variant) {
        return $user->hasRole('admin');
    }

    public function delete(User $user, Variant $variant) {
        return $user->hasRole('admin');
    }

    public function restore(User $user, Variant $variant) {
        return $user->hasRole('admin');
    }

    public function forceDelete(User $user, Variant $variant) {
        return $user->hasRole('admin');
    }
}
