<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MemberPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the member.
     *
     * @param  \App\User  $user
     * @param  \App\User  $member
     * @return mixed
     */
    public function view(User $user, User $member)
    {
        if($user->hasRole('saler')){
            return $member->getContact() == $user->saler;
        }
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can create members.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can update the member.
     *
     * @param  \App\User  $user
     * @param  \App\User  $member
     * @return mixed
     */
    public function update(User $user, User $member)
    {
        if($user->hasRole('saler')){
            return $member->getContact() == $user->saler;
        }
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can delete the member.
     *
     * @param  \App\User  $user
     * @param  \App\User  $member
     * @return mixed
     */
    public function delete(User $user, User $member)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can restore the member.
     *
     * @param  \App\User  $user
     * @param  \App\User  $member
     * @return mixed
     */
    public function restore(User $user, User $member)
    {
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can permanently delete the member.
     *
     * @param  \App\User  $user
     * @param  \App\User  $member
     * @return mixed
     */
    public function forceDelete(User $user, User $member)
    {
        return $user->hasRole('admin');
    }
}
