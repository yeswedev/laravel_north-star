<?php

namespace App;

use Laravel\Spark\User as SparkUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\CanResetPassword;
use App\Traits\HasPoints;
use App\Traits\HasSofteners;
use App\Traits\HasDepartment;
use App\Traits\Refreshable;
use App\Department;
use App\Role;

class User extends SparkUser
{
    use HasPoints;
    use HasSofteners;
    use HasDepartment;
    use Refreshable;
    use Notifiable;

    protected $attributes = [
        'name'                      => null,
        'email'                     => null,
        'title'                     => null,
        'firstname'                 => null,
        'lastname'                  => null,
        'function'                  => null,
        'birth'                     => null,
        'phone'                     => null,
        'mobile'                    => null,
        'department_id'             => null,
        'company_address'           => null,
        'company_address_line_2'    => null,
        'company_zipcode'           => null,
        'company_city'              => null,
        'company_siret'             => null,
        'company_ape'               => null,
        'company_tva'               => null,
        'billing_address'           => null,
        'billing_address_line_2'    => null,
        'billing_city'              => null,
        'billing_zip'               => null,
        'size'                      => false,
        'cgv_agrement'              => false,
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'title',
        'firstname',
        'lastname',
        'function',
        'birth',
        'phone',
        'mobile',
        'department_id',
        'company_address',
        'company_address_line_2',
        'company_zipcode',
        'company_city',
        'company_siret',
        'company_ape',
        'company_tva',
        'billing_address',
        'billing_address_line_2',
        'billing_city',
        'billing_zip',
        'size',
        'cgv_agrement',

    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'authy_id',
        'country_code',
        'two_factor_reset_code',
        'card_brand',
        'card_last_four',
        'card_country',
        'billing_address',
        'billing_address_line_2',
        'billing_city',
        'billing_zip',
        'billing_country',
        'extra_billing_information',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'trial_ends_at' => 'datetime',
        'uses_two_factor_auth' => 'boolean',
        'birth' => 'datetime:Y-m-d',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'birth',
    ];

    /**
     * Get the bills for the user.
     *
     * @return Bill Collection
     */
    public function bill()
    {
        return $this->hasMany('App\Bill');
    }

    /*
     * Get Orders linked to the user
     *
     * @return Order Collection
     */
    public function order() {
        return $this->hasMany('App\Order');
    }

    /*
     * Get Subscription linked to the user
     *
     * @return Subscription Collection
     */
    public function subscriptionRelation() {
        return $this->hasOne('App\Subscription');
    }

    /*
     * Get Saler if user is a Saler
     *
     * @return Saler Collection
     */
    public function saler() {
        return $this->hasOne('App\Saler');
    }

    /*
     * Get Role for a user
     *
     * @return Role Collection
     */
    public function role() {
        return $this->belongsTo('App\Role');
    }

    /*
     * Get Department for a user
     *
     * @return Department Collection
     */
    public function department() {
        return $this->belongsTo('App\Department');
    }

    public function getDepartmentId() {
        return $this->department_id;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullnameAttribute()
    {
        return $this->lastname . ' ' . $this->firstname;
    }

    /**
     * Get fullname + email
     *
     * @return string
     */
    public function getFullnameEmailAttribute()
    {
        return $this->lastname . ' ' . $this->firstname . ' : ' . $this->email;
    }

    /**
     * Get Saler Mail
     *
     *  @return string $mail
     */
    public function getSalerMailAttribute()
    {
        if($this->getContact()){
            $s=$this->getContact();
            $salerDatas=self::find($s->user_id);
            if(isset($salerDatas)){
                return $salerDatas->email;
            }
        }
        return config('contact.other');
    }

    /**
     * Get Contact Saler
     *
     * @return Saler $contact
     */
    public function getContact(){
        if(isset($this->department)){
            return $this->department->saler;
        }
        return false;
    }

    /**
     * Get Total points accumulated with bills upload
     *
     * @return int $points
     */
    public function getTotalPoints()
    {
        return $this->cumulativePoints($this->bill);
    }

    /**
     * Get Total points used in orders
     *
     * @return int $points
     */
    public function getUsedPoints()
    {
        return $this->cumulativePoints($this->order);
    }

    /**
     * Get Available points
     *
     * @return int $points
     */
    public function getAvailabledPoints()
    {
        return $this->getTotalPoints() - $this->getUsedPoints();
    }

    /**
     * Get Cart Order (the current one)
     *
     * @return Order
     */
    public function getCurrentOrder() {
        $order = $this->order()->where('status', 0)->first();
        if ( $order != null ) {
            return $order;
        } else {
            $order = new Order;
            $this->order()->save($order);
            return $this->getCurrentOrder();
        }
    }

    /**
     * Set Department
     *
     * @return User
     */
    public function setDepartment() {
        if(!empty($this->company_zipcode)){
            $dpt=$this->deduceDepartment($this->company_zipcode);
            $this->department()->associate($dpt);
            $this->save();
        }
        return $this->getFreshInstance();
    }

    /**
     * Set Role
     *
     * @param string $roleSlug
     * @return User
     */
    public function setRole($roleSlug = 'member') {
        $role=Role::where('name',$roleSlug)->first();
        if(empty($role)){
            $role=Role::where('name','member')->first();
        }
        $this->role()->associate($role);
        $this->save();
        return $this->getFreshInstance();
    }

    /**
     * Check that current user has access to Nova back-office (static)
     *
     * @return bool
     */
    public static function currentUserHasNovaAccess()
    {
        $user = Auth::user();
        return $user->hasNovaAccess();
    }

    /**
     * Check that current user has access to Nova back-office (dynamic)
     *
     * @return bool
     */
    public function hasNovaAccess()
    {
        return $this->hasRole('admin') ||  $this->hasRole('saler');
    }

    /**
     * Check that current user has the given role
     *
     * @param string
     *
     * @return bool
     */
    public function hasRole(string $role_name) {
        $role = $this->role;
        if ( $role == null ) return false;
        return ( $role->getName() == $role_name );
    }

    /**
     * Get membersList by department if filled
     *
     * @param Array $departments
     * @return Collection User
     */
    public static function membersList($departments=false){
        $memberRole=Role::where('name','member')->first();
        if($departments){
            return User::where('role_id',$memberRole->id)
                    ->whereIn('department_id',$departments)
                    ->get();
        }
        return User::where('role_id',$memberRole->id)->get();
    }

    /**
     * Get membersList by department if filled
     *
     * @param Array $departments
     * @return Collection User
     */
    public static function membersListArray($departments=false){
        $memberRole=Role::where('name','member')->first();
        if($departments){
            return User::select('id')
                    ->where('role_id',$memberRole->id)
                    ->whereIn('department_id',$departments)
                    ->get();
        }
        return User::where('role_id',$memberRole->id)->get();
    }

    /**
     * Get an array of member_id by Saler
     *
     * @return Array $membersId
     */
    public function membersIdArray(){
        $members=[];
        if($this->hasRole('admin')){
            return User::membersListArray();
        }
        elseif($this->hasRole('saler')){
            $dpts=Department::select('id')->where('saler_id',$this->saler->id)->get();
             return User::membersListArray($dpts);

        }
        return $members;
    }

    public function taxPercentage()
    {
        return 20; // Customers should be charged 20% tax
    }
}
