<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

use App\Traits\Refreshable;
use App\Traits\HasPoints;

class Variant extends Model
{

    use Refreshable;
    use HasPoints;

    protected $attributes = [
      'stock'         => 0,
      'break_point'   => 0,
      'points'        => 0,
      'reference'     => null,
      'name'          => null,
      'product_id'    => null,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'stock',
      'break_point',
      'points',
      'reference',
      'name',
      'product_id',
    ];

    /**
     * Get the product which it belongsTo the Variant
     *
     * @return Product
     */
    public function product() {
      return $this->belongsTo('App\Product');
    }

    /**
     * Get the orders link to the instance
     *
     * @return Order Collection
     */
    public function order(){
      return $this->belongsToMany('App\Order')->as('order_variant')
            ->withPivot('quantity')
            ->withTimestamps();
    }

    /**
     * Get the name attribute of Product relationship
     *
     * @return string
     */
    public function getProductName() {
      return $this->product->name;
    }

    /**
     * Get the complete name of a variant (product + variant)
     *
     * @return string
     */
    public function getCompleteName() {
      return $this->getProductName().' '.$this->name;
    }

    /**
     * Get the product image url
     *
     * @return string
     */
    public function getImageUrl() {
      $file=$this->product->image;
      return Storage::url($file);
    }

    /**
     * Set variant sto
     *
     * @param Number $quantity (could be positive when increase stoc and negative to decrease)
     * @return Variant
     */
    public function setStock($quantity){
      $this->stock+=$quantity;
      $this->save();
      return $this->getFreshInstance();
    }
}
