<?php

namespace App\Listeners;

use Laravel\Spark\Events\Subscription\UserSubscribed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Mail;
use App\Mail\NewSubscription;
use App\Mail\SubscriptionConfirmation;



class SendNewSubscriptionNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserSubscribed $event)
    {
        Mail::to($event->user->saler_mail)->queue(new NewSubscription($event->user));
        Mail::to($event->user->email)->queue(new SubscriptionConfirmation($event->user));
    }

}
