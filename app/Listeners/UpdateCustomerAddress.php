<?php

namespace App\Listeners;

use Stripe\Stripe;
use Stripe\Customer;
use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Queue\ShouldQueue;
use Laravel\Spark\Events\PaymentMethod\BillingAddressUpdated;

class UpdateCustomerAddress implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Remember that Stripe unfortunately allows multiple customers to have the same email address.
     * If we ever have more customers with a matching email address than can be returned by a single 'page' of the API request, we'll need a more complicated approach.
     * @see https://stackoverflow.com/a/38492724/470749
     * @param string $emailAddress
     * @return array
     */
    public function getCustomersByEmailAddress($emailAddress) {
        try {
            $response = Customer::all(["limit" => 100, "email" => $emailAddress]);
            return $response->data;
        } catch (\Exception $e) {
            Log::error($e);
            return [];
        }
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(BillingAddressUpdated $event)
    {
        // Set Stripe Api Key
        Stripe::setApiKey(env('STRIPE_SECRET'));

        $email = $event->billable->email;

        $customer = $this->getCustomersByEmailAddress($email);
        $firstCustomer = reset($customer);
        $customerId = null;
        // $customerId = $customer[0]->id;

        if(!empty($firstCustomer)) {
            $customerId = $firstCustomer->id;
        } else {
            return var_dump('Failed to retrieve customer id on Stripe.');
        }

        Customer::update(
            $customerId,
            [
                'name' => $event->billable->lastname .' '. $event->billable->firstname,
                'description' => $event->billable->name,
                'address' => [
                    'line1' => $event->billable->billing_address,
                    'city' => $event->billable->billing_city,
                    'line2' => $event->billable->billing_address_line_2,
                    'postal_code' => $event->billable->billing_zip,
                ],
                'shipping' => [
                    'address' => [
                        'line1' => $event->billable->company_address,
                        'city' => $event->billable->company_city,
                        'line2' => $event->billable->company_address_line_2,
                        'postal_code' => $event->billable->company_zipcode,
                    ],
                    'name' => $event->billable->name,
                    'phone' => $event->billable->phone,
                ],
                'preferred_locales' => [
                    'fr',
                    'en',
                ],
            ]
        );
    }

}
