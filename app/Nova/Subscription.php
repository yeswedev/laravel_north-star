<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Inspheric\Fields\Indicator;

use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\BelongsTo;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;
use Laravel\Nova\Fields\Date;
use App\Nova\Actions\ExportSubscriptions;
use App\Nova\Actions\DownloadInvoice;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Http\Requests\NovaRequest;

class Subscription extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Subscription';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'created_at',
    ];

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'user' => ['name', 'email', 'firstname', 'lastname'],
    ];

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        if($request->user()->hasRole('saler'))
        {
            return $query->whereIn('user_id', $request->user()->membersIdArray());
        }
        return $query;
    }


    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()
                ->hideFromIndex()
                ->hideFromDetail(),

            BelongsTo::make('Société', 'User', 'App\Nova\User')
                ->sortable(),

            BelongsTo::make('Membre', 'User', 'App\Nova\User')
                ->sortable()
                ->display('fullname')
                ->onlyOnDetail()
                ->withMeta(['extraAttributes' => [
                    'readonly' => true,
                ]]),
            
            Text::make('Nom', 'name')
                ->hideFromDetail()
                ->withMeta([
                    'value' => 'default',
                    'extraAttributes' => [
                        'readonly' => true,
                    ]
                ]),
            Text::make('Stripe Identifiant', 'stripe_id')
                ->hideFromDetail()
                ->withMeta([
                    'value' => 'sub_F4RgGRdRn3b5kf',
                    'extraAttributes' => [
                        'readonly' => true,
                    ]
                ]),
            Text::make('Stripe Plan', 'stripe_plan')
                ->hideFromDetail()
                ->withMeta([
                    'value' => 'plan_GBNPzw286Qudty',
                    'extraAttributes' => [
                        'readonly' => true,
                    ]
                ]),
            Number::make('Quantité', 'quantity')
                ->hideFromDetail()
                ->withMeta([
                    'value' => 1,
                    'extraAttributes' => [
                        'readonly' => true,
                    ]
                ]),

            Text::make('Département', 'User', function() {
                return $this->user->getDepartmentId();
            })
                ->sortable()
                ->onlyOnDetail(),

            Indicator::make('Date d\'abonnement', function() {
                    $ends_at = date('Y-m-d', strtotime('+ 365 day', strtotime($this->updated_at)) );
                    $new_ends_at = date('Y-m-d', strtotime('+2 weeks', strtotime($this->updated_at)) );
                    if ( $new_ends_at > date('Y-m-d') ) {
                        return 'new';
                    } elseif ( $ends_at <= date('Y-m-d') ) {
                        return 'old';
                    } else {
                        return 'normal';
                    }
                })
                ->labels([
                    'new' =>  date('d/m/Y', strtotime($this->updated_at)),
                    'old' =>  date('d/m/Y', strtotime($this->updated_at)),
                    'normal' =>  date('d/m/Y', strtotime($this->updated_at)),
                ])
                ->colors([
                    'new' => 'green',
                    'old' => 'red',
                    'normal' => 'blue',
                ])
                ->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new DownloadInvoice)
                ->canSee(function ($request) {
                    return true;
                })
                ->canRun(function ($request, $user) {
                    return true;
                }),
            (new ExportSubscriptions)
                ->askForFilename(__('Nom du fichier'))
                ->askForWriterType(
                    null,
                    __('Choix du fichier')
                )
                ->withHeadings()
                ->only(
                    'id',
                    'Société',
                    'Membre',
                    'Département',
                    'updated_at',
                    'created_at'
                )
                ->onFailure(function() {
                    return Action::danger('Un problème est survenu, votre export n\'a pas abouti.');
                })
                ->canSee(function ($request) {
                    return $request->user()->hasRole('admin');
                }),
        ];
    }

   /**
    * Override displayed name of the ressource
    *
    * @return string
    */
   public static function label() {
       return 'Abonnements';
   }

   /**
    * The logical group associated with the resource.
    *
    * @var string
    */
   public static $group = 'Membres';

   /**
    * Disable Creation
    *
    * @param Request $request
    * @return false
    */
   public static function authorizedToCreate(Request $request) {
       return true;
   }

    /**
     * Determine whether the user can update the word press post.
     *
     * @param  \App\User $user
     *
     * @return bool
     */
    public function update(User $user): bool
    {
        return true;
    }

    /**
     * Disable Update
     *
     * @param Request $request
     * @return false
     */
    public function authorizedToUpdate(Request $request) {
        return false;
    }

    /**
     * Indicates if the resource should be displayed in the sidebar.
     *
     * @var bool
     */
    public static $displayInNavigation = false;

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->user->name . ' du ' . $this->created_at;
    }
}
