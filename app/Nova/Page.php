<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;

class Page extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Page';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'slug',
        'title'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()
                ->hideFromIndex()
                ->hideFromDetail(),

            Text::make('Titre', 'title')
                ->rules('required', 'max:191')
                ->help('<span style="color:#ee5f5b;">Requis</span>')
                ->sortable(),

            Text::make('Texte du lien', 'slug')
                ->withMeta(['extraAttributes' => [
                    'placeholder' => 'titre-de-la-page']
                ])
                ->help(
                    'Le nom de la page, en minuscules, sans accents, mots séparés par des tirets.'
                )
                ->rules('required', 'max:191')
                ->help('<span style="color:#ee5f5b;">Requis</span>')
                ->hideFromIndex(),

            Trix::make('Contenu', 'content')
                ->rules('required')
                ->help('<span style="color:#ee5f5b;">Requis</span>')
                ->hideFromIndex(),

            // Boolean::make('Publiée', function () {
            //     return now()->gt($this->published_at);
            // }),

            Boolean::make('Publiée', 'published'),
            
            DateTime::make('Créée le', 'created_at')
                ->format('DD/MM/YYYY HH:mm')
                ->readonly()
                ->hideWhenCreating()
                ->hideWhenUpdating()
                ->sortable(),
            
            DateTime::make('Mise à jour le', 'updated_at')
                ->format('DD/MM/YYYY HH:mm')
                ->readonly()
                ->hideWhenCreating()
                ->hideWhenUpdating()
                ->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Contenu';

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->title;
    }
}
