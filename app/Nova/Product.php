<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Image;

use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\HasMany;
use App\Nova\Actions\ExportProducts;

class Product extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Product';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'variant' => ['name'],
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()
                ->hideFromIndex()
                ->hideFromDetail(),

            Text::make('Produit', 'name')
                ->sortable()
                ->rules('required', 'max:191')
                ->help('<span style="color:#ee5f5b;">Requis</span>'),

            Image::make('Image')
                ->disk('public')->path('products')
                ->rules('max:2500'),

            HasMany::make('Déclinaisons', 'Variant', 'App\Nova\Variant')
            ->hideFromIndex(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new ExportProducts)
                ->askForFilename(__('Nom du fichier'))
                ->askForWriterType(
                    null,
                    __('Choix du fichier')
                )
                ->withHeadings()
                ->only(
                    'id',
                    'name'
                )
                ->onFailure(function() {
                    return Action::danger('Un problème est survenu, votre export n\'a pas abouti.');
                })
                ->canSee(function ($request) {
                    return $request->user()->hasRole('admin');
                }),
        ];
    }

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->name;
    }

    /**
     * Override displayed name of the ressource
     *
     * @return string
     */
    public static function label() {
      return 'Produits';
    }

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Catalogue';
}
