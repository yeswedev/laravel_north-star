<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\File;
use Inspheric\Fields\Indicator;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use App\Nova\Actions\RejectBill;
use Laravel\Nova\Actions\Action;

use App\Nova\Actions\ValidateBill;
use Laravel\Nova\Fields\BelongsTo;
use App\Nova\Metrics\CountNewBills;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;
use App\Nova\Actions\ExportBills;
use Laravel\Nova\Http\Requests\NovaRequest;

class Bill extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Bill';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'status', 'points'
    ];

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'user' => ['name', 'email', 'firstname', 'lastname'],
    ];

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        if($request->user()->hasRole('saler'))
        {
            return $query->whereIn('user_id', $request->user()->membersIdArray());
        }
        return $query;
    }
    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()
                ->hideFromIndex()
                ->hideFromDetail(),

            BelongsTo::make('Société', 'User', 'App\Nova\User')
                ->withMeta(['extraAttributes' =>[
                    'readonly' => true,
                ]])
                ->sortable(),

            BelongsTo::make('Membre', 'User', 'App\Nova\User')
                ->display('fullname')
                ->withMeta(['extraAttributes' =>[
                    'readonly' => true,
                ]])
                ->hideWhenUpdating()
                ->sortable(),

            File::make('Doc')
                ->disk('bills')
                ->hideWhenUpdating(),

            Number::make('Points')
                ->sortable(),

            Number::make('Montant TTC', 'amount')
                ->hideFromIndex(),

            Number::make('2.0/UD', 'nb_product_UD')
                ->sortable(),
            Number::make('2.0+/UD+', 'nb_product_UD_plus')
                ->sortable(),
            Number::make('NCS Hybrid+', 'nb_product_NSC_hybrid')
                ->sortable(),

            Select::make('Status')
                ->options([
                    0 => 'En attente',
                    1 => 'Traitée',
                    2 => 'Refusée',
                ])
                ->displayUsingLabels()
                ->hideFromIndex()
                ->hideFromDetail()
                ->hideWhenUpdating()
                ->withMeta(['extraAttributes' =>[
                    'readonly' => true,
                ]]),


            Indicator::make('Status')
                ->labels([
                    0 => 'En attente',
                    1 => 'Traitée',
                    2 => 'Refusée',
                ])
                ->colors([
                    0 => 'red',
                    1 => 'green',
                    2 => 'grey',
                ])
                ->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            new CountNewBills()
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new ValidateBill)
                ->canSee(function ($request) {
                    return $request->user()->hasRole('admin');
                })
                ->canRun(function ($request, $user) {
                    return $request->user()->hasRole('admin');
                }),
            (new RejectBill)
                ->canSee(function ($request) {
                    return $request->user()->hasRole('admin');
                })
                ->canRun(function ($request, $user) {
                    return $request->user()->hasRole('admin');
                }),
            (new ExportBills)
                ->askForFilename(__('Nom du fichier'))
                ->askForWriterType(
                    null,
                    __('Choix du fichier')
                )
                ->withHeadings()
                ->only(
                    'id',
                    'Société',
                    'Membre',
                    'status',
                    'nb_product_UD',
                    'nb_product_UD_plus',
                    'nb_product_NSC_hybrid',
                    'amount',
                    'points',
                    'created_at'
                )
                ->onFailure(function() {
                    return Action::danger('Un problème est survenu, votre export n\'a pas abouti.');
                })
                ->canSee(function ($request) {
                    return $request->user()->hasRole('admin');
                }),
        ];
    }

    /**
     * Override displayed name of the ressource
     *
     * @return string
     */
    public static function label() {
      return 'Factures';
    }

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Membres';

    /**
     * Disable Creation
     *
     * @param Request $request
     * @return false
     */
    public static function authorizedToCreate(Request $request) {
        return false;
    }

    /**
     * Disable Update
     *
     * @param Request $request
     * @return false
     */
    public function authorizedToUpdate(Request $request) {
        return false;
    }

    /**
     * Indicates if the resource should be displayed in the sidebar.
     *
     * @var bool
     */
    public static $displayInNavigation = false;

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->user->fullName . ' du ' . $this->created_at;
    }
}
