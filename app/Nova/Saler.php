<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\HasMany;
use App\Nova\Actions\ExportSalers;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Http\Requests\NovaRequest;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class Saler extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Saler';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'user' => ['name', 'email', 'firstname', 'lastname'],
        'department' => ['id', 'name'],
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()
                ->hideFromIndex()
                ->hideFromDetail(),

            Text::make('Nom', 'title')
                ->rules('required')
                ->help('<span style="color:#ee5f5b;">Requis</span>')
                ->sortable(),

            BelongsTo::make('Utilisateur', 'User', 'App\Nova\User')
                ->display('fullnameemail')
                ->help(
                    'Lier le commercial à un utilisateur du site.
                    <br> Le commercial est avant-tout un utilisateur du site, pour qu\'il puisse se connecter, il doit d\'abord avoir un compte utilisateur.
                    <br>Pour cela : se rendre dans la page "Membres" et créer un nouveau membre commercial.'
                )
                ->rules('required')
                ->help('<span style="color:#ee5f5b;">Requis</span>'),

            HasMany::make('Départements', 'Department', 'App\Nova\Department'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new ExportSalers)
                ->askForFilename(__('Nom du fichier'))
                ->askForWriterType(
                    null,
                    __('Choix du fichier')
                )
                ->withHeadings()
                ->only(
                    'id',
                    'title',
                    'Utilisateur',
                    'created_at'
                )
                ->onFailure(function() {
                    return Action::danger('Un problème est survenu, votre export n\'a pas abouti.');
                })
                ->canSee(function ($request) {
                    return $request->user()->hasRole('admin');
                }),
        ];
    }

    /**
     * Override displayed name of the ressource
     *
     * @return string
     */
    public static function label() {
        return 'Commerciaux';
    }

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Commerciaux';

    /**
     * Indicates if the resource should be displayed in the sidebar.
     *
     * @var bool
     */
    public static $displayInNavigation = false;

    /**
     * Build a "relatable" query for the given resource.
     *
     * This query determines which instances of the model may be attached to other resources.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function relatableUsers(NovaRequest $request, $query)
    {
      return $query->where('role_id', 3);
    }

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->title . ' : ' . $this->user->email;
    }
}
