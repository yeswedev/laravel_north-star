<?php

namespace App\Nova;

use Eminiarts\Tabs\Tabs;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use App\Nova\Metrics\CountUsers;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\HasMany;
use App\Nova\Lenses\AdminMembers;

use App\Nova\Lenses\SalerMembers;
use App\Nova\Metrics\UsersByWeek;
use Laravel\Nova\Fields\BelongsTo;

use Laravel\Nova\Http\Requests\NovaRequest;
use App\Nova\Actions\ExportUsers;
use App\Nova\Lenses\MemberMembers;
use App\Nova\Lenses\MemberFilteredByDepartmentMembers;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\PasswordConfirmation;

class User extends Resource
{

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\\User';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name', 'email', 'firstname', 'lastname',
    ];

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'department' => ['id', 'name'],
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            (new Tabs('Informations', [
                'Général'                   => $this->globalFields(),
                'Entreprise'                => $this->companyFields(),
                'Facturation'    => $this->billingAddress(),
            ]))->withToolbar(),

            (new Tabs('', [
                'Factures'                  => [ HasMany::make('Factures', 'Bill', 'App\Nova\Bill') ],
                'Commandes'                 => [ HasMany::make('Commandes', 'Order', 'App\Nova\Order') ],
            ])),
        ];
    }

    /**
     * Get global fields for the resource.
     *
     * @return array
     */
    protected function globalFields()
    {
        return [
            ID::make()
                ->hideFromIndex()
                ->hideFromDetail(),

            Select::make('Role', 'role_id')
                ->options([
                    1 => 'Administrateur',
                    2 => 'Membre',
                    3 => 'Commercial'
                ])
                ->displayUsingLabels()
                ->rules('required')
                ->help('<span style="color:#ee5f5b;">Requis</span>')
                ->hideFromIndex(),

            Text::make('Société', 'name')
                ->sortable()
                ->rules('required', 'max:191')
                ->help('<span style="color:#ee5f5b;">Requis</span>'),

            Text::make('Membre', 'fullName')
                ->hideFromDetail()
                ->hideWhenUpdating()
                ->hideWhenCreating()
                ->withMeta(['extraAttributes' => [
                    'readonly' => true,
                ]])
                ->sortable(),

            Text::make('Commercial', 'Department', function() {
                return $this->department->getSalerTitle();
            })
                ->hideWhenUpdating()
                ->hideWhenCreating()
                ->sortable(),

            BelongsTo::make('Departement', 'department', 'App\Nova\Department')
                ->rules('required')
                ->help('<span style="color:#ee5f5b;">Requis</span>')
                ->sortable(),

            Text::make('Fonction', 'function')
                ->hideFromIndex()
                ->rules('max:191'),

            Text::make('Email')
                ->hideFromIndex()
                ->rules('email', 'max:191', 'required')
                ->help('<span style="color:#ee5f5b;">Requis</span>')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),

            Password::make('Mot de passe', 'password')
                ->onlyOnForms()
                ->hideWhenUpdating()
                ->creationRules('required', 'string', 'min:6', 'confirmed')
                ->help('<span style="color:#ee5f5b;">Requis</span>'),

            PasswordConfirmation::make('Confirmation du mot de passe', 'password_confirmation')
                ->onlyOnForms()
                ->hideWhenUpdating()
                ->creationRules('required', 'string', 'min:6')
                ->help('<span style="color:#ee5f5b;">Requis</span>'),

            Select::make('Civilité', 'title')
                ->options([
                    'Mme' => 'Mme',
                    'Mr' => 'Mr'
                ])
                ->hideFromIndex(),

            Text::make('Nom', 'lastname')
                ->sortable()
                ->hideFromIndex()
                ->rules('required', 'max:191')
                ->help('<span style="color:#ee5f5b;">Requis</span>'),

            Text::make('Prénom', 'firstname')
                ->rules('required', 'max:191')
                ->help('<span style="color:#ee5f5b;">Requis</span>')
                ->hideFromIndex(),

            Date::make('Date de naissance', 'birth')
                ->hideFromIndex(),

            Number::make('Téléphone', 'phone')
                ->hideFromIndex()
                ->rules('max:25')
                ->withMeta([
                    'placeholder' => '0212345678'
                ])
                ->help(
                    "Pas d'espaces et de séparateurs."
                ),

            Number::make('Mobile')
                ->hideFromIndex()
                ->rules('required', 'max:25')
                ->withMeta([
                    'placeholder' => '0612345678'
                ])
                ->help(
                    '<span style="color:#ee5f5b;">Requis pour les commerciaux</span> - Pas d\'espaces et de séparateurs.'
                ),

            // CGV with default value in creating
            Select::make('CGV acceptée', 'cgv_agrement')
                ->options([
                    0 => 'Non',
                    1 => 'Oui',
                ])
                ->withMeta([
                    'value' => 0,
                ])
                ->rules('required')
                ->help('<span style="color:#ee5f5b;">Requis</span>')
                ->displayUsingLabels()
                ->hideWhenUpdating()
                ->hideFromDetail()
                ->hideFromIndex(),

            // CGV displayed in detail
            Select::make('CGV acceptée', 'cgv_agrement')
                ->options([
                    0 => 'Non',
                    1 => 'Oui',
                ])
                ->rules('required')
                ->help('<span style="color:#ee5f5b;">Requis</span>')
                ->displayUsingLabels()
                ->hideWhenUpdating()
                ->hideWhenCreating()
                ->hideFromIndex(),
        ];
    }

    /**
     * Get company fields for the resource.
     *
     * @return array
     */
    protected function companyFields()
    {
        return [
            Text::make('Adresse', 'company_address')
                ->hideFromIndex()
                ->rules('max:191'),

            Text::make('Complément d\'Adresse', 'company_address_line_2')
                ->hideFromIndex()
                ->rules('max:191'),

            Text::make('Code Postal', 'company_zipcode')
                ->hideFromIndex()
                ->rules('digits:5'),

            Text::make('Ville', 'company_city')
                ->hideFromIndex()
                ->rules('max:191'),

            Text::make('SIRET', 'company_siret')
                ->hideFromIndex()
                ->rules('digits_between:12,16'),

            Text::make('N° APE', 'company_ape')
                ->hideFromIndex()
                ->rules('min:3', 'max:7', 'alpha_num'),

            Text::make('N° TVA', 'company_tva')
                ->hideFromIndex()
                ->rules('min:11', 'max:15', 'alpha_num'),

            // Size displayed in detail
            Select::make('Taille', 'size')
                ->options([
                    'L' => 'L',
                    'XL' => 'XL',
                ])
                ->rules('required')
                ->help('<span style="color:#ee5f5b;">Requis</span>')
                ->hideWhenCreating()
                ->hideFromIndex(),

            // Size with default value in creating
            Select::make('Taille', 'size')
                ->options([
                    'L' => 'L',
                    'XL' => 'XL',
                ])
                ->withMeta([
                    'value' => 'L',
                ])
                ->rules('required')
                ->help('<span style="color:#ee5f5b;">Requis</span>')
                ->hideWhenUpdating()
                ->hideFromDetail()
                ->hideFromIndex(),
        ];
    }

    /**
     * Get company fields for the resource.
     *
     * @return array
     */
    protected function billingAddress()
    {
        return [
            Text::make('Adresse', 'billing_address')
                ->hideFromIndex()
                ->rules('max:191'),

            Text::make('Complément d\'Adresse', 'billing_address_line_2')
                ->hideFromIndex()
                ->rules('max:191'),

            Text::make('Code Postal', 'billing_zip')
                ->hideFromIndex()
                ->rules('digits:5'),

            Text::make('Ville', 'billing_city')
                ->hideFromIndex()
                ->rules('max:191'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            new CountUsers(),
            new UsersByWeek(),
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [
            (new SalerMembers)->canSee(function ($request) {
                    return $request->user()->hasRole('admin');
                }),
            (new AdminMembers)->canSee(function ($request) {
                    return $request->user()->hasRole('admin');
                }),
            (new MemberMembers)->canSee(function ($request) {
                    return $request->user()->hasRole('admin');
                }),
            (new MemberFilteredByDepartmentMembers)->canSee(function ($request) {
                    return $request->user()->hasRole('saler');
                }),

        ];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new ExportUsers)
                ->askForFilename(__('Nom du fichier'))
                ->askForWriterType(
                    null,
                    __('Choix du fichier')
                )
                ->withHeadings()
                ->only(
                    'id',
                    'name',
                    'title',
                    'firstname',
                    'lastname',
                    'function',
                    'birth',
                    'email',
                    'phone',
                    'mobile',
                    'department_id',
                    'cgv_agrement',
                    'Commercial',
                    'created_at'
                )
                ->onFailure(function() {
                    return Action::danger('Un problème est survenu, votre export n\'a pas abouti.');
                })
                ->canSee(function ($request) {
                    return $request->user()->hasRole('admin');
                }),
        ];
    }

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->name;
    }

    /**
     * Override displayed name of the ressource
     *
     * @return string
     */
    public static function label() {
        return 'Utilisateurs';
    }

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Membres';

    /**
     * Indicates if the resource should be displayed in the sidebar.
     *
     * @var bool
     */
    public static $displayInNavigation = false;
}
