<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\BelongsTo;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;
use App\Nova\Actions\ExportDepartments;
use Laravel\Nova\Fields\ID;

class Department extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Department';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name'
    ];

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'saler' => ['title'],
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Text::make('Numéro', 'id')
                ->sortable()
                ->readonly(),

            Text::make('Nom', 'name')
                ->sortable()
                ->readonly(),

            BelongsTo::make('Commercial', 'Saler', 'App\Nova\Saler')
                ->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new ExportDepartments)
                ->askForFilename(__('Nom du fichier'))
                ->askForWriterType(
                    null,
                    __('Choix du fichier')
                )
                ->withHeadings()
                ->only(
                    'id',
                    'name',
                    'Commercial'
                )
                ->onFailure(function() {
                    return Action::danger('Un problème est survenu, votre export n\'a pas abouti.');
                }),
        ];
    }

    /**
     * Override displayed name of the ressource
     *
     * @return string
     */
    public static function label() {
        return 'Départements';
    }

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Commerciaux';

    /**
     * Indicates if the resource should be displayed in the sidebar.
     *
     * @var bool
     */
    public static $displayInNavigation = false;

    /**
     * Disable Creation
     *
     * @param Request $request
     * @return false
     */
    public static function authorizedToCreate(Request $request) {
        return false;
    }

    /**
     * Disable Creation
     *
     * @param Request $request
     * @return false
     */
    public function authorizedToDelete(Request $request) {
        return false;
    }

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->id . ' ' . $this->name;
    }
}
