<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Laravel\Nova\Fields\Number;

class ValidateBill extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        foreach ($models as $model) {
            $model->update([
                'points'=>$fields->points,
                'amount'=>$fields->amount,
                'nb_product_UD' => $fields->nb_product_UD,
                'nb_product_UD_plus' => $fields->nb_product_UD_plus,
                'nb_product_NSC_hybrid' => $fields->nb_product_NSC_hybrid
            ]);
            $bill=$model->validateBill();
        }
        if($bill){
            return Action::message('La facture a bien été validée');
        }else{
            return Action::danger('La facture n\'a pas été validée, avez vous bien affecté les points avant de la valider?');
        }

    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Number::make('Points'),
            Number::make('Montant TTC', 'amount'),
            Number::make('2.0/UD', 'nb_product_UD'),
            Number::make('2.0+/UD+', 'nb_product_UD_plus'),
            Number::make('NCS Hybrid+', 'nb_product_NSC_hybrid')
        ];
    }

    /**
     * Get the displayable name of the action.
     *
     * @return string
     */
    public function name()
    {
        return __('Valider la facture');
    }

    /**
     * Indicates if this action is only available on the resource detail view.
     *
     * @var bool
     */
    public $onlyOnDetail = true;

}
