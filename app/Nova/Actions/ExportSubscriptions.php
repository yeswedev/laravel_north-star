<?php

namespace App\Nova\Actions;

use App\Subscription;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class ExportSubscriptions extends DownloadExcel implements WithMapping
{
    /**
     * Get the displayable name of the action.
     *
     * @return string
     */
    public function name()
    {
        return __('Télécharger les abonnements');
    }
}