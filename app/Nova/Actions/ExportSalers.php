<?php

namespace App\Nova\Actions;

use App\Saler;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class ExportSalers extends DownloadExcel implements WithMapping
{
    /**
     * Get the displayable name of the action.
     *
     * @return string
     */
    public function name()
    {
        return __('Télécharger les commerciaux');
    }
}