<?php

namespace App\Nova\Actions;

use App\Order;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class ExportOrders extends DownloadExcel implements WithMapping
{


    /**
     * Get the displayable name of the action.
     *
     * @return string
     */
    public function name()
    {
        return __('Télécharger les commandes');
    }


    /**
     * @param Order $order
     *
     * @return array
     */
    public function map($order): array
    {

        $status = null;
        switch ($order->status) {
            case 0:
                $status = 'En cours';
            break;
            case 1:
                $status = 'En attente';
            break;
            case 2:
                $status = 'Traitée';
            break;
            case 3:
                $status = 'Refusée';
            break;
        }

        $data = [];

        $user = $order->user;

        $data = [
            'Id' => $order->id,
            'Société' => $user->name,
            'Adresse de livraison' => $user->company_address .' '. $user->company_address_line_2 .', '. $user->company_zipcode .' '. $user->company_city,
            'Tel' => $user->phone,
            'Membre' => $user->lastname.' '.$user->firstname,
            'Email' => $user->email,
            'Points' => $order->points,
            'Status' => $status,
            'Créé le' => date('d/m/Y', strtotime($order->created_at)),
        ];

        foreach ($order->variant as $key => $variant) {
            $key = $key + 1;
            $data['Produit '.$key] = $variant->product->name.' '.$variant->name.' x '.$variant->order_variant->quantity;
        }

        return $data;
    }
}