<?php

namespace App\Nova\Actions;

use App\Product;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class ExportProducts extends DownloadExcel implements WithMapping
{
    /**
     * Get the displayable name of the action.
     *
     * @return string
     */
    public function name()
    {
        return __('Télécharger les produits');
    }
}