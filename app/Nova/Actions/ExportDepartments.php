<?php

namespace App\Nova\Actions;

use App\Department;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class ExportDepartments extends DownloadExcel implements WithMapping
{
    /**
     * Get the displayable name of the action.
     *
     * @return string
     */
    public function name()
    {
        return __('Télécharger les départements');
    }
}