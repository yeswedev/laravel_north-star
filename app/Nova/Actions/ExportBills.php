<?php

namespace App\Nova\Actions;

use App\Bill;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class ExportBills extends DownloadExcel implements WithMapping
{
    /**
     * Get the displayable name of the action.
     *
     * @return string
     */
    public function name()
    {
        return __('Télécharger les factures');
    }
}