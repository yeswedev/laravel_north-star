<?php

namespace App\Nova;

use Eminiarts\Tabs\Tabs;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Inspheric\Fields\Indicator;
use Laravel\Nova\Fields\Number;

use Laravel\Nova\Actions\Action;

use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use App\Nova\Actions\ExportVariants;

class Variant extends Resource
{

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Variant';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'points', 'name', 'stock', 'break_point',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()
                ->hideFromIndex()
                ->hideFromDetail(),

            BelongsTo::make('Produit', 'Product', 'App\Nova\Product')
                ->nullable()
                ->sortable(),

            Number::make('Points')
                ->rules('required')
                ->help('<span style="color:#ee5f5b;">Requis</span>')
                ->sortable(),

            Text::make('Variante', 'name')
                ->rules('required')
                ->help('
                    <span style="color:#ee5f5b;">Requis</span><br>
                    <span>La taille du produit. Si le produit n\'a pas différentes tailles, vous pouvez indiquez "Unique" par exemple.'
                )
                ->sortable(),

            Number::make('Stock')
                ->rules('required')
                ->help('<span style="color:#ee5f5b;">Requis</span>')
                ->sortable()
                ->hideFromIndex()
                ->hideFromDetail(),

            new Tabs('Stock', [
                'Stock' => $this->checkStockAlert(),
            ]),

            Number::make('Alerte de stock', 'break_point')
                ->rules('required')
                ->help('<span style="color:#ee5f5b;">Requis</span>')
                ->hideFromIndex(),

            BelongsToMany::make('Commande','Order','App\Nova\Order')->fields(function () {
                    return [
                        Number::make('Quantity'),
                    ];
                }),
        ];
    }

    /**
     * Check stock left to add color
     *
     * @return array
     */
    public function checkStockAlert() {
        if ( $this->stock == 0 ) {
            return [
                Indicator::make('Stock')
                    ->labels([
                        0 => 'Rupture',
                    ])
                    ->colors([
                        0 => 'red'
                    ])
                    ->sortable()
                    ->rules('required')
                    ->help('<span style="color:#ee5f5b;">Requis</span>'),
            ];
        } elseif ( $this->stock <= $this->break_point ) {
            return [
                Indicator::make('Stock')
                    ->labels([
                        $this->stock => $this->stock,
                    ])
                    ->colors([
                        $this->stock => 'orange'
                    ])
                    ->sortable()
                    ->rules('required')
                    ->help('<span style="color:#ee5f5b;">Requis</span>'),
            ];
        } else {
            return [
                Indicator::make('Stock')
                    ->labels([
                        $this->stock => $this->stock,
                    ])
                    ->colors([
                        $this->stock => 'green',
                    ])
                    ->sortable()
                    ->rules('required')
                    ->help('<span style="color:#ee5f5b;">Requis</span>'),
            ];
        }
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new ExportVariants)
                ->askForFilename(__('Nom du fichier'))
                ->askForWriterType(
                    null,
                    __('Choix du fichier')
                )
                ->withHeadings()
                ->only(
                    'id',
                    'Produit',
                    'name',
                    'points',
                    'stock',
                    'break_point'
                )
                ->onFailure(function() {
                    return Action::danger('Un problème est survenu, votre export n\'a pas abouti.');
                })
                ->canSee(function ($request) {
                    return $request->user()->hasRole('admin');
                }),
        ];
    }

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->product->name . ' ' . $this->name;
    }

    /**
     * Override displayed name of the ressource
     *
     * @return string
     */
    public static function label()
    {
      return 'Déclinaisons';
    }

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Catalogue';

    /**
     * Indicates if the resource should be displayed in the sidebar.
     *
     * @var bool
     */
    public static $displayInNavigation = false;
}
