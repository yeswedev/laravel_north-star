<?php

namespace App\Nova;

use Eminiarts\Tabs\Tabs;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Inspheric\Fields\Indicator;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Actions\Action;
use App\Nova\Actions\RejectOrder;

use App\Nova\Metrics\CountOrders;

use Laravel\Nova\Fields\DateTime;
use App\Nova\Metrics\OrdersByWeek;
use Laravel\Nova\Fields\BelongsTo;
use App\Nova\Actions\ValidateOrder;
use App\Nova\Metrics\CountNewOrders;
use Laravel\Nova\Fields\BelongsToMany;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;
use App\Nova\Actions\ExportOrders;
use Laravel\Nova\Http\Requests\NovaRequest;

class Order extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Order';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'status', 'reference', 'points', 'created_at'
    ];

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'user' => ['name', 'email', 'firstname', 'lastname'],
        'variant' => ['name']
    ];

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        if($request->user()->hasRole('saler'))
        {
            return $query->whereIn('user_id', $request->user()->membersIdArray());
        }
        return $query;
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()
                ->hideFromIndex()
                ->hideFromDetail(),

            Text::make('Code commande', 'reference')
                ->sortable(),

            BelongsTo::make('Société', 'User', 'App\Nova\User')
                ->sortable(),

            Number::make('Points')
                ->hideFromIndex()
                ->canSee(function ($request) {
                    return $request->user()->hasRole('admin');
                }),

            DateTime::make('Date', 'created_at')
                ->format('DD/MM/YYYY HH:mm', 'fr')
                ->sortable(),

            new Tabs('Status', [
                'Status' => $this->statusField(),
            ]),

            BelongsToMany::make('Produits commandés','Variant','App\Nova\Variant')->fields(function () {
                    return [
                        Number::make('Quantity'),
                    ];
                }),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            new CountOrders(),
            new OrdersByWeek(),
            new CountNewOrders()
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new ValidateOrder)
                ->canSee(function ($request) {
                    return $request->user()->hasRole('admin');
                })
                ->canRun(function ($request, $user) {
                    return $request->user()->hasRole('admin');
                }),
            (new RejectOrder)
                ->canSee(function ($request) {
                    return $request->user()->hasRole('admin');
                })
                ->canRun(function ($request, $user) {
                    return $request->user()->hasRole('admin');
                }),
            (new ExportOrders)
                ->askForFilename(__('Nom du fichier'))
                ->askForWriterType(
                    null,
                    __('Choix du fichier')
                )
                ->withHeadings()
                ->onFailure(function() {
                    return Action::danger('Un problème est survenu, votre export n\'a pas abouti.');
                })
                ->canSee(function ($request) {
                    return $request->user()->hasRole('admin');
                }),
        ];
    }

    /**
     * Get status fields for the resource.
     *
     * @return array
     */
    protected function statusField() {
        return [
            Select::make('Status')
                ->options([
                    0 => 'En cours',
                    1 => 'En attente',
                    2 => 'Traitée',
                    3 => 'Refusée',
                ])
                ->displayUsingLabels()
                ->hideFromIndex()
                ->hideFromDetail()
                ->hideWhenUpdating()
                ->withMeta(['extraAttributes' =>[
                    'readonly' => true,
                ]]),

            Indicator::make('Status')
                ->labels([
                    0 => 'En cours',
                    1 => 'En attente',
                    2 => 'Traitée',
                    3 => 'Refusée',
                ])
                ->colors([
                    0 => 'blue',
                    1 => 'red',
                    2 => 'green',
                    3 => 'grey',
                ])
                ->sortable()
        ];
    }

    /**
     * Override displayed name of the ressource
     *
     * @return string
     */
    public static function label() {
        return 'Commandes';
    }

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Catalogue';

    /**
     * Disable Creation
     *
     * @param Request $request
     * @return false
     */
    public static function authorizedToCreate(Request $request) {
        return false;
    }

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->user->name . ' du ' . $this->created_at;
    }
}
