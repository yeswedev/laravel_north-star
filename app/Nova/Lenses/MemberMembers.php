<?php

namespace App\Nova\Lenses;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Lenses\Lens;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Http\Requests\LensRequest;

class MemberMembers extends Lens
{
    /**
    * Get the displayable name of the lense.
    *
    * @return string
    */
    public function name()
    {
        return 'Membres';
    }

    /**
     * Get the query builder / paginator for the lens.
     *
     * @param  \Laravel\Nova\Http\Requests\LensRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return mixed
     */
    public static function query(LensRequest $request, $query)
    {
        return $request->withOrdering($request->withFilters(
            $query->where('role_id', 2)
        ));
    }

    /**
     * Get the fields available to the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [            
            ID::make()
                ->hideFromIndex()
                ->hideFromDetail(),

            Text::make('Société', 'name')
                ->sortable()
                ->rules('required', 'max:191'),
            

            Text::make('Membre', 'fullName')
                ->hideFromDetail()
                ->hideWhenUpdating()
                ->hideWhenCreating()
                ->withMeta(['extraAttributes' => [
                    'readonly' => true,
                ]])
                ->sortable(),

            Text::make('Commercial', 'Department', function() {
                return $this->department->getSalerTitle();
            })
                ->hideWhenUpdating()
                ->hideWhenCreating()
                ->sortable(),

            BelongsTo::make('Departement', 'department', 'App\Nova\Department')
                ->rules('required')
                ->sortable(),
        ];
    }

    /**
     * Get the filters available for the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available on the lens.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return parent::actions($request);
    }

    /**
     * Get the URI key for the lens.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'member-members';
    }
}
