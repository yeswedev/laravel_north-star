<?php

namespace App\Providers;

use Laravel\Spark\Spark;
use Laravel\Cashier\Cashier;
use Laravel\Spark\Providers\AppServiceProvider as ServiceProvider;

class SparkServiceProvider extends ServiceProvider
{
    /**
     * Your application and company details.
     *
     * @var array
     */
    protected $details = [
        'vendor' => 'North-Star',
        'product' => 'Abonnement annuel au club',
        'street' => '37 av pierre 1er de serbie',
        'location' => '75008 Paris',
        'phone' => '01 23 45 67 89',
    ];

    /**
     * The address where customer support e-mails should be sent.
     *
     * @var string
     */
    protected $sendSupportEmailsTo = null;

    /**
     * All of the application developer e-mail addresses.
     *
     * @var array
     */
    protected $developers = [
        //
        "claire@yeswedev.fr",
    ];

    /**
     * Indicates if the application will expose an API.
     *
     * @var bool
     */
    protected $usesApi = true;

    /**
     * Finish configuring Spark for the application.
     *
     * @return void
     */
    public function booted()
    {
        Cashier::useCurrency('eur', '€');

        Spark::useStripe()->noCardUpFront()->trialDays(10);

        Spark::collectEuropeanVat('FR');

        Spark::plan('Abonnement annuel club North-Star (HT)', 'plan_GBNPzw286Qudty')
            ->price(125)
            ->yearly();

        Spark::collectBillingAddress();
    }
}
