<?php

namespace App\Providers;

use Laravel\Nova\Nova;
use Laravel\Nova\Cards\Help;
use App\Nova\Metrics\CountUsers;
use App\Nova\Metrics\CountOrders;
use App\Nova\Metrics\UsersByWeek;
use App\Nova\Metrics\OrdersByWeek;
use App\Nova\Metrics\CountNewBills;
use App\Nova\Metrics\CountNewOrders;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\NovaApplicationServiceProvider;
use App\User;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Nova::style('nova-theme', public_path('css/nova.css'));
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
                ->withAuthenticationRoutes()
                ->withPasswordResetRoutes()
                ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', User::class.'@currentUserHasNovaAccess');
    }

    /**
     * Get the cards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function cards()
    {
        return [
            // new Help,
            (new CountUsers())->width('1/2'),
            (new CountOrders())->width('1/2'),
            (new UsersByWeek())->width('1/2'),
            (new OrdersByWeek())->width('1/2'),
            (new CountNewOrders())->width('1/2'),
            (new CountNewBills())->width('1/2'),
        ];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
