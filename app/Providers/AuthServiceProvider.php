<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\User'          => 'App\Policies\MemberPolicy',
        'App\Page'          => 'App\Policies\PagePolicy',
        'App\Department'    => 'App\Policies\DepartmentPolicy',
        'App\Saler'         => 'App\Policies\SalerPolicy',
        'App\Variant'       => 'App\Policies\VariantPolicy',
        'App\Product'       => 'App\Policies\ProductPolicy',
        'App\Subscription'  => 'App\Policies\SubscriptionPolicy',
        'App\Order'         => 'App\Policies\OrderPolicy',
        'App\Bill'          => 'App\Policies\BillPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        //
    }
}
