<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Saler extends Model
{
    protected $attributes = [
      'title'    => null,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'title',
    ];

    /**
     * Get the user link to the saler (= saler info not users in his perimeter)
     */
    public function user(){
      return $this->belongsTo('App\User');
    }

    /*
     * Get Departments linked to the saler
     *
     * @return Order Collection
     */
    public function department() {
        return $this->hasMany('App\Department');
    }

    public function saler() {
      return $this;
    }
}
