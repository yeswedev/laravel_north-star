<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $timestamps = false;

    public $fillable = [ 'name' ];

    /**
     * Get the users associated to current role
     *
     * @return Collection
     */
    public function users() {
        return $this->hasMany(User::class);
    }

    /**
     * Get the name of current role
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }
}
