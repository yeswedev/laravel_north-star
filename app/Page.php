<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $attributes = [
        'title'         => null,
        'slug'          => null,
        'content'       => null,
        'published'     => null,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 
        'slug', 
        'content', 
        'published',
    ];
}
