<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Bill;

class BillValidation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The bill instance.
     *
     * @var Bill
     */
    public $bill;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Bill $bill)
    {
        $this->bill = $bill;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Validation de facture')
                    ->markdown('emails.bills.validation')
                    ->with([
                        'bill' => $this->bill
                    ]);
    }

    
}
