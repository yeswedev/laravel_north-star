<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Traits\Refreshable;
use App\Traits\HasPoints;
use App\Traits\HasStatus;

use Illuminate\Support\Facades\Mail;
use App\Mail\OrderSubmitted;

class Order extends Model
{

    use Refreshable;
    use HasPoints;
    use HasStatus;

    protected $attributes = [
      'points'        => 0,
      'reference'     => null,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'points',
      'status',
      'reference',
    ];

    /**
     * Minimum order Points
     */
    protected $pointsMini = 250;

    /**
     * Get the user which it belongsTo registered the current EPI
     *
     * @return User
     */
    public function user() {
      return $this->belongsTo('App\User');
    }

    /**
     * Get Variants linked to the order
     *
     * @return Variant Collection
     */
    public function variant() {
        return $this->belongsToMany('App\Variant')
            ->as('order_variant')
            ->withPivot('quantity')
            ->withTimestamps();
    }

    /**
     * Get the totat quantity of a product
     *
     * @return number $quantity
     */
    public function getTotalQuantityAttribute(){
        $quantity=0;
        foreach($this->variant as $p){
            $quantity+=$p->order_variant->quantity;
        }
        return $quantity;
    }

    /**
     * Remove all products from the current order
     *
     * @return Order
     */
    public function removeAllProducts() {
        $this->variant()->sync([]);
        return $this->getFreshInstance();
    }

    /**
     * Remove given product from the current order
     *
     * @param OrderItem
     *
     * @return Order
     */
    public function removeProduct(Variant $variant) {
        $ref = $variant->id;
        $this->variant()->detach($ref);
        return $this->getFreshInstance();
    }

    /**
     * Add given product to the current order
     *
     * @param Variant
     * @param int
     *
     * @return Order
     */
    public function addProduct(Variant $variant, int $quantity = 1) {
        $ref = $variant->id;
        $existing_variant = $this->variant()->find($ref);
        if ( $existing_variant != null ) {
            $quantity_current = $existing_variant->order_variant->quantity;
            $quantity_new = $quantity_current + $quantity;
            return $this->setVariantQuantity($variant, $quantity_new);
        } else {
            $attributes = [ 'quantity' => $quantity ];
            $this->variant()->attach($ref, $attributes);
            return $this->getFreshInstance();
        }
    }

    /**
     * Set quantity for given variant in the current order
     *
     * @param Variant
     * @param int
     *
     * @return Order
     */
    public function setVariantQuantity(Variant $variant, int $quantity) {
        $ref = $variant->id;
        $attributes = [ 'quantity' => $quantity ];
        $this->variant()->updateExistingPivot($ref, $attributes);
        return $this->getFreshInstance();
    }

    /**
     * Get quantity for given variant in the current order
     *
     * @param Variant
     *
     * @return Order
     */
    public function getVariantQuantity(Variant $variant) {
        $ref = $variant->id;
        $existing_variant = $this->variant()->find($ref);
        $quantity_current = $existing_variant->order_variant->quantity;
        return $quantity_current;
    }

    /**
     * Get the total points used for an order
     *
     * @return int $points
     */
    public function  getUsedPointsByOrder(){
        $points = 0;
        foreach ($this->variant as $variant) {
            $points+=$variant->points * $variant->order_variant->quantity;
        }
        return $points;
    }

    /**
     * Validate the currentOrder by changing status and saving points
     *
     * @return Order or false
     */
    public function validateOrder(){
        $points=$this->getUsedPointsByOrder();
        if($points<$this->pointsMini){
            return false;
        }
        if($this->status==0){
            if($this->user->getAvailabledPoints()<$points) return false;
            $status=1;

        }else{
            $status=2;
        }
        $this->setStatus($status);
        $this->setPoints($this->getUsedPointsByOrder());
        if($status==1){
            $this->refreshStock();
            $this->sendOrderSubmitMail();
        }
        return $this->getFreshInstance();
    }

    /**
     * Reject an order
     *
     * @return Order
     */
    public function rejectOrder(){
        $this->setStatus(3);
        $this->setPoints(0);
        $this->refreshStock('up');

        return $this->getFreshInstance();
    }

    /**
     * Refresh Stock of every product in the order when validating a cart or rejecting an order
     *
     * @param String $action (up or down)
     * @return boolean true
     */
    protected function refreshStock($action = 'down'){
        foreach($this->variant as $p){
            $qty=$action=='up'?$p->order_variant->quantity:-$p->order_variant->quantity;
            $p->setStock($qty);
        }
        return true;
    }

    /**
     * Send order submiting mail
     *
     * @return void
     */
    public function sendOrderSubmitMail(){
        Mail::to($this->user->email)->send(new OrderSubmitted($this));
    }
}


