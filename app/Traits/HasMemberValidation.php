<?php

namespace App\Traits;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

trait HasMemberValidation
{

    /**
     * Validate regstration step 2 datas
     *
     * @return $validator
     */
    protected function validateCompanyDatas($request)
    {
      $data = $request->all();
      if (array_key_exists('name', $data)) {
        Session::put('name', $data['name']);
      }
      if (array_key_exists('company_address', $data)) {
        Session::put('company_address', $data['company_address']);
      }
      if (array_key_exists('company_address_line_2', $data)) {
        Session::put('company_address_line_2', $data['company_address_line_2']);
      }
      if (array_key_exists('company_zipcode', $data)) {
        Session::put('company_zipcode', $data['company_zipcode']);
      }
      if (array_key_exists('company_city', $data)) {
        Session::put('company_city', $data['company_city']);
      }
      if (array_key_exists('company_siret', $data)) {
        Session::put('company_siret', $data['company_siret']);
      }
      if (array_key_exists('company_ape', $data)) {
        Session::put('company_ape', $data['company_ape']);
      }
      if (array_key_exists('company_tva', $data)) {
        Session::put('company_tva', $data['company_tva']);
      }
      if (array_key_exists('billing_address', $data)) {
        Session::put('billing_address', $data['billing_address']);
      }
      if (array_key_exists('billing_address_line_2', $data)) {
        Session::put('billing_address_line_2', $data['billing_address_line_2']);
      }
      if (array_key_exists('billing_city', $data)) {
        Session::put('billing_city', $data['billing_city']);
      }
      if (array_key_exists('billing_zip', $data)) {
        Session::put('billing_zip', $data['billing_zip']);
      }
      if (array_key_exists('size', $data)) {
        Session::put('size', $data['size']);
      }

      $rules = [
          'name'                      => 'required',
          'company_address'           => 'required',
          'company_zipcode'           => ['required', 'digits:5'],
          'company_city'              => 'required',
          'company_siret'             => ['required', 'digits_between:12,16'],
          'company_ape'               => ['required', 'min:3', 'max:7', 'alpha_num'],
          'company_tva'               => ['required', 'min:11', 'max:15', 'alpha_num'],
          'billing_address'           => 'required',
          'billing_city'              => 'required',
          'billing_zip'               => ['required', 'digits:5'],
          'size'                      => 'required',
        ];
        $messages = [
          'name.required'      => 'Le champs <span>Raison Sosiale</span> est obligatoire.',

          'company_address.required'      => 'Le champs <span>Adresse postale</span> est obligatoire.',

          'company_zipcode.required'  => 'Le champs <span>Code Postal</span> est obligatoire.',

          'company_zipcode.digits'  => 'Le champs <span>Code Postal</span> doit contenir 5 chiffres.',

          'company_city.required' => 'Le champs <span>Ville</span> est obligatoire.',

          'company_siret.required'      => 'Le champs <span>Siret</span> est obligatoire.',

          'company_siret.digits_between'      => 'Le champs <span>Siret</span> doit être composé de 14 chiffres.',

          'company_ape.required'   => 'Le champs <span>Code APE</span> est obligatoire.',

          'company_ape.min'       => 'Le champs <span>Code APE</span> ne correspond pas à un code APE. Il doit contenir 4 chiffres et une lettre.',

          'company_ape.max'       => 'Le champs <span>Code APE</span> ne correspond pas à un code APE. Il doit contenir 4 chiffres et une lettre.',

          'company_ape.alpha_num'       => 'Le champs <span>Code APE</span> ne doit contenir que des chiffres et des lettres.',

          'company_tva.required'  => 'Le champs <span>TVA intracommunautaire</span> est obligatoire.',

          'company_tva.min'       => 'Le champs <span>TVA intracommunautaire</span> ne correspond pas à un code de TVA. Il doit contenir 2 lettres et 11 chiffres.',

          'company_tva.max'       => 'Le champs <span>TVA intracommunautaire</span> ne correspond pas à un code de TVA. Il doit contenir 2 lettres et 11 chiffres.',

          'company_tva.alpha_num'       => 'Le champs <span>TVA intracommunautaire</span> ne doit contenir que des chiffres et des lettres.',

          'billing_address.required'      => 'Le champs <span>Adresse de facturation</span> est obligatoire.',

          'billing_city.required'      => 'Le champs <span>ville de l\'adresse de facturation</span> est obligatoire.',

          'billing_zip.required'   => 'Le champs <span>code postal de l\'adresse de facturation</span> est obligatoire.',

          'billing_zip.digits'  => 'Le champs <span>Code Postal de l\'adresse de facturation</span> doit contenir 5 chiffres.',

          'size.required'   => 'Le champs <span>taille</span> est obligatoire.',
        ];

        return $validator = Validator::make( $request->all(), $rules, $messages );
    }
}
