<?php

namespace App\Traits;

trait HasPoints
{

  /**
   * Get cumulated points for a given user
   *
   *  @return int $points
   */
  private function cumulativePoints($collection){
    $points=0;
    if ( $collection !== null && $collection->count() > 0 ) {
      foreach($collection as $item){
        $points+=$item->points;
      }
    }
    return $points;
  }

  /**
   * Set order points
   * @param int $points
   *
   * @return Order
   */
  public function setPoints(int $points){
      $this->points = $points;
      $this->save();
      return $this;
  }
}
