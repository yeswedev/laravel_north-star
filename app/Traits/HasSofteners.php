<?php

namespace App\Traits;

trait HasSofteners
{
  /**
   * Get total of UD Softener
   *
   * @return int $softener
   */
  public function getUdTotal(){
    return $udArray=$this->softenerTotal('nb_product_UD');
  }

  /**
   * Get total of UD+ Softener
   *
   * @return int $softener
   */
  public function getUdPlusTotal(){
    return $udArray=$this->softenerTotal('nb_product_UD_plus');
  }

    /**
   * Get total of NSC Hybrid Softener
   *
   * @return int $softener
   */
  public function getNcsHybridTotal(){
    return $udArray=$this->softenerTotal('nb_product_NSC_hybrid');
  }

  /**
   * Get total nbr softener bought
   *
   *  @return int $softeners
   */
  private function softenerTotal($softener_column){
    $softeners=0;
    $products=$this->bill()->select($softener_column)->get();
    foreach($products as $item){
      $softeners+=$item->$softener_column;
    }
    return $softeners;
  }
}
