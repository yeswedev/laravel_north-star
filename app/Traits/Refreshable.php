<?php

namespace App\Traits;

trait Refreshable
{
  /**
   * Refresh current instance of model from database
   *
   * @return \Illuminate\Database\Eloquent\Model
   */
  protected function getFreshInstance() {
    $id = $this->id;
    $model = self::find($id);
    return $model;
  }
}
