<?php

namespace App\Traits;

use App\Department;

trait HasDepartment
{

  /**
   * Get Department from a zipcode
   *
   *  @return Department
   */
  public function deduceDepartment($zipcode){
    $code=intval(substr($zipcode, 0, 2));
    if($code == 20){
        $code=$zipcode>=20200?"2B":"2A";
    }
    else if($code == 97){
      $code=intval(substr($zipcode, 0, 3));
    }
    $code= (string)$code;
    $dpt=Department::find($code);
    return $dpt;
  }

  /**
   * Get Saler from Department
   *
   * @param Department
   * @return Saler
   */
  public function getSalerFromDepartment($department)
  {
    if(empty($department)){
      $department=Department::find('75');
    }
    return $department->saler;
  }
}