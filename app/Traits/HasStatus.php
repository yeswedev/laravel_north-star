<?php

namespace App\Traits;

trait HasStatus
{
    /**
   * Set status
   *
   * For order : 0 = current order (cart) - 1 = saved order - 2 = validated order - 3 = rejected order
   * For bill : 0 = uploaded bill - 1 = validated bill - 2 = rejected bill
   *
   * @param int
   *
   * @return Order
   */
  public function setStatus(int $status) {
      $this->status = $status;
      $this->save();
      return $this;
  }
}
