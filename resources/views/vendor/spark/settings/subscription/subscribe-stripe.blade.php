<spark-subscribe-stripe :user="user" :team="team"
                        :plans="plans" :billable-type="billableType" inline-template>

    <div>
        <!-- Common Subscribe Form Contents -->
    @include('spark::settings.subscription.subscribe-common')

    <!-- Billing Information -->
        <div class="card card-default" v-show="selectedPlan">
            <div class="card-header">
                <h5 class="h5">{{__('Paiement par carte bancaire')}}</h5>
            </div>

            <div class="card-body">
                <!-- Generic 500 Level Error Message / Stripe Threw Exception -->
                <div class="alert alert-danger" v-if="form.errors.has('form')">
                    {{__('We had trouble validating your card. It\'s possible your card provider is preventing us from charging the card. Please contact your card provider or customer support.')}}
                </div>

                <form role="form">
                    <!-- Payment Method -->
                    <div class="form-group row" v-if="hasPaymentMethod()">
                        <label for="use_existing_payment_method" class="col-md-4 col-form-label text-md-right">{{__('Payment Method')}}</label>

                        <div class="col-md-6">
                            <select name="use_existing_payment_method" v-model="form.use_existing_payment_method" id="use_existing_payment_method" class="form-control">
                                <option value="1">{{__('Use existing payment method')}}</option>
                                <option value="0">{{__('Use a different method')}}</option>
                            </select>
                        </div>
                    </div>

                    <!-- Cardholder's Name -->
                    <div class="form-group" v-show="form.use_existing_payment_method != '1'">
                        <label for="name" class="form-field pay-field">
                            <span>{{__('Nom du porteur de la carte')}}</span>
                            <input type="text" name="name" v-model="cardForm.name">
                        </label>
                    </div>

                    <!-- Card Details -->
                    <div class="form-group" v-show="form.use_existing_payment_method != '1'">
                        <label for="name" class="form-field pay-field">
                            <span>{{__('Carte')}}</span>
                            <div id="subscription-card-element"></div>
                            <div class="invalid-feedback" v-show="cardForm.errors.has('card')">
                                @{{ cardForm.errors.get('card') }}
                            </div>
                        </label>
                    </div>

                    <!-- Billing Address Fields -->
                    @if (Spark::collectsBillingAddress())
                        @include('spark::settings.subscription.subscribe-address')
                    @endif

                    <!-- Tax / Price Information -->
                    <div class="form-group row" v-if="spark.collectsEuropeanVat && countryCollectsVat && selectedPlan">
                        <label class="col-md-4 col-form-label text-md-right">&nbsp;</label>

                        <div class="col-md-6">
                            <div class="alert alert-info" style="margin: 0;">
                                <strong>{{__('Tax')}}:</strong> @{{ taxAmount(selectedPlan) | currency }}
                                <br><br>
                                <strong>{{__('Total Price Including Tax')}}:</strong>
                                @{{ priceWithTax(selectedPlan) | currency }}
                                @{{ selectedPlan.type == 'user' && spark.chargesUsersPerSeat ? '/ '+ spark.seatName : '' }}
                                @{{ selectedPlan.type == 'user' && spark.chargesUsersPerTeam ? '/ '+ __('teams.team') : '' }}
                                @{{ selectedPlan.type == 'team' && spark.chargesTeamsPerSeat ? '/ '+ spark.teamSeatName : '' }}
                                @{{ selectedPlan.type == 'team' && spark.chargesTeamsPerMember ? '/ '+ __('teams.member') : '' }}
                                / @{{ __(selectedPlan.interval) | capitalize }}
                            </div>
                        </div>
                    </div>

                    <!-- Subscribe Button -->
                    <div class="form-group flex justify-content-end">
                        <button type="submit" class="button button-valid has-m-top-2" @click.prevent="subscribe" :disabled="form.busy">
                        <span v-if="form.busy">
                            <i class="fa fa-btn fa-spinner fa-spin"></i> {{__('En cours de validation')}}
                        </span>
                        <span v-else>
                            {{__('Valider l\'abonnement')}}
                        </span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</spark-subscribe-stripe>
