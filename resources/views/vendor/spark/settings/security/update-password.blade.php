<spark-update-password inline-template>
    <div class="card">
        <h2 class="has-uppercase h2 password-reset__title">{{__('Update Password')}}</h2>

        <div class="card-body password-reset__card">
            <!-- Success Message -->
            <div class="alert alert-success" v-if="form.successful">
                {{__('Your password has been updated!')}}
            </div>
                <a href="{{route('home')}}" class="button login__block" v-if="form.successful">{{__('Retour à l\'espace Mon compte')}}</a>
                <br>
                <br>

            <form role="form">
                <!-- Current Password -->
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">{{__('Current Password')}}</label>

                    <div class="col-md-6">
                        <input type="password" class="form-control input input--grey login__input--grey  login__block" name="current_password" v-model="form.current_password" :class="{'is-invalid': form.errors.has('current_password')}">

                        <span class="invalid-feedback" v-show="form.errors.has('current_password')">
                            @{{ form.errors.get('current_password') }}
                        </span>
                    </div>
                </div>

                <!-- New Password -->
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">{{__('Password')}}</label>

                    <div class="col-md-6">
                        <input type="password" class="form-control input input--grey login__input--grey  login__block" name="password" v-model="form.password" :class="{'is-invalid': form.errors.has('password')}">

                        <span class="invalid-feedback" v-show="form.errors.has('password')">
                            @{{ form.errors.get('password') }}
                        </span>
                    </div>
                </div>

                <!-- New Password Confirmation -->
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">{{__('Confirm Password')}}</label>

                    <div class="col-md-6">
                        <input type="password" class="form-control input input--grey login__input--grey  login__block" name="password_confirmation" v-model="form.password_confirmation" :class="{'is-invalid': form.errors.has('password_confirmation')}">

                        <span class="invalid-feedback" v-show="form.errors.has('password_confirmation')">
                            @{{ form.errors.get('password_confirmation') }}
                        </span>
                    </div>
                </div>

                <!-- Update Button -->
                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="button login__block login__input--submit"
                                @click.prevent="update"
                                :disabled="form.busy">

                            {{__('Update')}}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</spark-update-password>
