<!DOCTYPE html>
<html lang="en" class="h-full font-sans antialiased">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=1280">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ Nova::name() }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,800,800i,900,900i" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('app.css', 'vendor/nova') }}">

    <!-- Tool Styles -->
    @foreach(Nova::availableStyles(request()) as $name => $path)
        <link rel="stylesheet" href="/nova-api/styles/{{ $name }}">
    @endforeach
</head>
<body class="min-w-site bg-40 text-black min-h-full">
    <div id="nova">
        <div v-cloak class="flex min-h-screen">
            <!-- Sidebar -->
            <div class="min-h-screen flex-none pt-header min-h-screen w-sidebar bg-grad-sidebar px-6">
                <a href="{{ Nova::path() }}">
                    <div class="absolute pin-t pin-l pin-r bg-logo flex items-center w-sidebar h-header px-6 text-white">
                       @include('nova::partials.logo')
                    </div>
                </a>

                @foreach (Nova::availableTools(request()) as $tool)
                    {!! $tool->renderNavigation() !!}
                @endforeach

                <h4 class="ml-8 mb-4 text-xs text-white-50% uppercase tracking-wide">Membres</h4>
                <ul class="list-reset mb-8">
                    <li class="leading-tight mb-4 ml-8 text-sm">
                        <a href="{{ Nova::path() }}/resources/subscriptions" class="text-white text-justify no-underline dim">Abonnements</a>
                    </li>
                    <li class="leading-tight mb-4 ml-8 text-sm">
                        <a href="{{ Nova::path() }}/resources/bills" class="text-white text-justify no-underline dim">Factures</a>
                    </li>
                    <li class="leading-tight mb-4 ml-8 text-sm">
                        <a href="{{ Nova::path() }}/resources/users/lens/member-members" class="text-white text-justify no-underline dim">Membres</a>
                    </li>
                </ul>
                @php($userAuth=Auth::user())
                @if($userAuth->hasRole('admin'))
                <h4 class="ml-8 mb-4 text-xs text-white-50% uppercase tracking-wide">Commerciaux</h4>
                <ul class="list-reset mb-8">
                    <li class="leading-tight mb-4 ml-8 text-sm">
                        <a href="{{ Nova::path() }}/resources/departments" class="text-white text-justify no-underline dim">Départements</a>
                    </li>
                    <li class="leading-tight mb-4 ml-8 text-sm">
                        <a href="{{ Nova::path() }}/resources/users/lens/saler-members" class="text-white text-justify no-underline dim">Commerciaux</a>
                    </li>
                </ul>

                <h4 class="ml-8 mb-4 text-xs text-white-50% uppercase tracking-wide">Actions</h4>
                <ul class="list-reset mb-8">
                    <li class="leading-tight mb-4 ml-8 text-sm">
                        <a href="{{ Nova::path() }}/resources/users/new?viaResource=&viaResourceId=&viaRelationship=" class="text-white text-justify no-underline dim">Créer un utilisateur</a>
                    </li>
                </ul>
                @endif

                <div class="logo-bg-white">
                    <img src="{{ asset('/img/logo_north-star_team_experts.svg') }}" alt="Logotype de la team experts de North-Star.">
                </div>
            </div>

            <!-- Content -->
            <div class="content">
                <div class="flex items-center relative shadow h-header bg-white z-20 px-6">
                    <a v-if="'{{ Nova::name() }}'" href="{{ Config::get('nova.url') }}" class="no-underline dim font-bold text-90 mr-6">
                        {{ Nova::name() }}
                    </a>

                    @if (count(Nova::globallySearchableResources(request())) > 0)
                        <global-search></global-search>
                    @endif

                    <dropdown class="ml-auto h-9 flex items-center dropdown-right">
                        @include('nova::partials.user')
                    </dropdown>
                </div>

                <div data-testid="content" class="px-view py-view mx-auto">
                    @yield('content')

                    @include('nova::partials.footer')
                </div>
            </div>
        </div>
    </div>

    <script>
        window.config = @json(Nova::jsonVariables(request()));
    </script>

    <!-- Scripts -->
    <script src="{{ mix('manifest.js', 'vendor/nova') }}"></script>
    <script src="{{ mix('vendor.js', 'vendor/nova') }}"></script>
    <script src="{{ mix('app.js', 'vendor/nova') }}"></script>

    <!-- Build Nova Instance -->
    <script>
        window.Nova = new CreateNova(config)
    </script>

    <!-- Tool Scripts -->
    @foreach (Nova::availableScripts(request()) as $name => $path)
        @if (starts_with($path, ['http://', 'https://']))
            <script src="{!! $path !!}"></script>
        @else
            <script src="/nova-api/scripts/{{ $name }}"></script>
        @endif
    @endforeach

    <!-- Start Nova -->
    <script>
        Nova.liftOff()
    </script>
</body>
</html>
