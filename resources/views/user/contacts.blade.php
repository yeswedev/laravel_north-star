
@extends('layouts.app')


@section('content')


    @component('components.hero', [
        'img_bg' => '/img/hero_bg.png',
        'img_content' => '/img/logo_north-star_team_experts.svg',
        'content' =>    '<h1 class="h1">Mes contacts</h1>'
    ])
    @endcomponent

    <section style="
        background: url({{ asset('/img/registration_bg.png') }});
        background-size: cover;
        background-position: center;
        ">
        <div class="after-hero before-footer">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-md-8 col-sm-10 col-12">


                        <section class="contacts-search">
                            <form method="POST" action="{{ route('saler') }}">
                                @csrf

                                <label for="zipcode" class="form-field">
                                    <input class="has-color-grey" id="zipcode" name="zipcode" type="number" value="{{$zip}}">
                                    <span>{{__('Votre code postal')}}</span>
                                </label>

                                <button class="button has-m-top-2" type="submit">{{__('Valider')}}</button>
                            </form>
                        </section>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{!! $error !!}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        <section class="contacts-list">
                            <article class="contact has-m-top-4">

                                <h2 class="h3">{{__('Le contact de votre région est : ')}}</h2>

                                <h3 class="h3 has-bold">{{$saler->user->firstname}} <span class="has-uppercase">{{$saler->user->lastname}}</span></h3>

                                <p class="has-color-accent">{{$saler->user->mobile}}</p>

                                <p><a href="mailto:{{$saler->user->email}}">{{$saler->user->email}}</a></p>

                                <p><i>Retrouvez-nous aussi sur <a href="http://www.north-star.fr">www.north-star.fr</a></i></p>

                            </article>
                        </section>


                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection
