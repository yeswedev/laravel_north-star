
@php
    $civilities = ['Mme','Mr'];
@endphp

@extends('layouts.user-edit')


@section('main-content')

    <section class="registration-form">
        <form method="POST" action="{{ route('save-edit') }}">
            @method('PUT')
            @csrf

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {{-- BEGIN VOS INFORMATIONS --}}
            <section class="registration-form-section">
                <div class="row">

                    <div class="col-12">
                        <p class="form-notification form-notification--info">* champs obligatoires</p>
                        <h4 class="h4">Vos informations</h4>
                    </div>

                    <div class="col-md-6 col-12">
                        <label for="title" class="form-field label-hidden">
                            <select name="title" id="title" required>
                                <option value="{{__('Civilité')}}" disabled selected>{{__('Civilité')}} *</option>
                                @foreach ($civilities as $civility)
                                    @if ($civility == $user->title)
                                        <option selected value="{{$civility}}">{{$civility}}</option>
                                    @else
                                        <option value="{{$civility}}">{{$civility}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <span>{{__('Civilité')}} *</span>
                        </label>

                        <label for="lastname" class="form-field label-hidden">
                            <input type="text" name="lastname" id="lastname" placeholder="{{__('Nom')}} *" value="{{$user->lastname}}" required>
                            <span>{{__('Nom')}} *</span>
                        </label>

                        <label for="firstname" class="form-field label-hidden">
                            <input type="text" name="firstname" id="firstname" placeholder="{{__('Prénom')}} *" value="{{$user->firstname}}" required>
                            <span>{{__('Prénom')}} *</span>
                        </label>

                        <label for="birth" class="form-field">
                            <input type="date" name="birth" id="birth" value="{{ date('Y-m-d', strtotime($user->birth)) }}" required>
                            <span>{{__('Date de naissance')}} *</span>
                        </label>
                    </div>

                    <div class="col-md-6 col-12">
                        <label for="function" class="form-field label-hidden">
                            <input type="text" name="function" id="function" placeholder="{{__('Fonction')}} *" value="{{$user->function}}" required>
                            <span>{{__('Fonction')}} *</span>
                        </label>

                        <label for="phone" class="form-field label-hidden">
                            <input type="tel" name="phone" id="phone" placeholder="{{__('Téléphone fixe')}}" value="{{$user->phone}}">
                            <span>{{__('Téléphone fixe')}}</span>
                        </label>

                        <label for="mobile" class="form-field label-hidden">
                            <input type="tel" name="mobile" id="mobile" placeholder="{{__('Téléphone portable')}}" value="{{$user->mobile}}">
                            <span>{{__('Téléphone portable')}}</span>
                        </label>

                        <label for="email" class="form-field label-hidden">
                            <input type="email" name="email" id="email" placeholder="{{__('Email')}} *" value="{{$user->email}}"  required>
                            <span>{{__('Email')}} *</span>
                        </label>
                    </div>

                </div>
            </section>
            {{-- ENDS VOS INFORMATIONS --}}

            {{-- BEGIN VOTRE ENTREPRISE --}}
            <section class="registration-form-section">
                <div class="row">

                    <div class="col-12">
                        <h4 class="h4">Votre entreprise</h4>
                        <p class="form-notification form-notification--info">Adresse utilisée pour la livraison des cadeaux.</p>
                    </div>

                    <div class="col-md-6 col-12">
                        <label for="name" class="form-field label-hidden">
                        <input type="text" name="name" id="name" placeholder="Raison Sociale *" value="{{$user->name}}" required>
                            <span>Raison Sociale *</span>
                        </label>

                        <label for="company_address" class="form-field label-hidden">
                            <input type="text" name="company_address" id="company_address" placeholder="Adresse Postale *" value="{{$user->company_address}}" required>
                            <span>Adresse Postale *</span>
                        </label>

                        <label for="company_address_line_2" class="form-field label-hidden">
                            <input type="text" name="company_address_line_2" id="company_address_line_2" placeholder="Complément d’adresse" value="{{$user->company_address_line_2}}">
                            <span>Complément d’adresse</span>
                        </label>

                        <label for="company_city" class="form-field label-hidden">
                            <input type="text" name="company_city" id="company_city" placeholder="Ville *" value="{{$user->company_city}}" required>
                            <span>Ville *</span>
                        </label>

                        <label for="company_zipcode" class="form-field label-hidden">
                            <input type="text" name="company_zipcode" id="company_zipcode" placeholder="Code Postal *" value="{{$user->company_zipcode}}" required>
                            <span>Code Postal *</span>
                        </label>
                    </div>

                    <div class="col-md-6 col-12">
                        <label for="company_siret" class="form-field label-hidden">
                            <input type="text" name="company_siret" id="company_siret" placeholder="N° SIRET *" value="{{$user->company_siret}}" required>
                            <span>N° SIRET *</span>
                        </label>

                        <label for="company_ape" class="form-field label-hidden">
                            <input type="text" name="company_ape" id="company_ape" placeholder="Code APE *" value="{{$user->company_ape}}" required>
                            <span>Code APE *</span>
                        </label>

                        <label for="company_tva" class="form-field label-hidden">
                            <input type="text" name="company_tva" id="company_tva" placeholder="N° TVA intracommunautaire *" value="{{$user->company_tva}}" required>
                            <span>N° TVA intracommunautaire *</span>
                        </label>
                    </div>

                </div>
            </section>
            {{-- ENDS VOTRE ENTREPRISE --}}


            {{-- BEGIN ADRESSE DE FACTURATION --}}
            <section class="registration-form-section">
                <div class="row">

                    <div class="col-12">
                        <h4 class="h4">Adresse de facturation</h4>
                    </div>

                    <div class="col-md-6 col-12">
                        <label for="billing_address" class="form-field label-hidden">
                            <input type="text" name="billing_address" id="billing_address" placeholder="Adresse *" value="{{$user->billing_address}}" required>
                            <span>Adresse *</span>
                        </label>

                        <label for="billing_address_line_2" class="form-field label-hidden">
                            <input type="text" name="billing_address_line_2" id="billing_address_line_2" placeholder="Complément d'adresse" value="{{$user->billing_address_line_2}}">
                            <span>Complément d'adresse</span>
                        </label>
                    </div>

                    <div class="col-md-6 col-12">
                        <label for="billing_city" class="form-field label-hidden">
                            <input type="text" name="billing_city" id="billing_city" placeholder="Ville *" value="{{$user->billing_city}}" required>
                            <span>Ville *</span>
                        </label>
                        
                        <label for="billing_zipcode" class="form-field label-hidden">
                            <input type="text" name="billing_zip" id="billing_zip" placeholder="Code Postal *" value="{{$user->billing_zip}}" required>
                            <span>Code Postal *</span>
                        </label>
                    </div>

                </div>
            </section>
            {{-- ENDS ADRESSE DE FACTURATION --}}

            <div class="row justify-content-end">
                {{-- NEED SUBMIT WITH LATER --}}
                <div class="col-12">
                    <div class="flex justify-content-between">
                        <a href="{{route('home')}}" class="link-back has-m-top-2"><i class="fas fa-chevron-circle-left"></i> Retour</a>
                        <button type="submit" class="button has-m-top-2">{{__('Valider les modifications')}}</button>
                    </div>
                </div>
            </div>


        </form>
    </section>

@endsection
