@extends('layouts.container-sidebar-left')

@section('top-content')
    @component('components.hero', [
        'img_bg' => '/img/hero_bg.png',
        'img_content' => '/img/logo_north-star_team_experts.svg',
        'content' =>    '<h1 class="h1">Mon Compte</h1>',
        'user_name' => $user->name
    ])
    @endcomponent
@endsection


@section('sidebar-left')

    @include('user.partials.sidebar')

@endsection


@section('main-content')

    @include('user.partials.account-main-content')

@endsection

@section('bottom-content')
@endsection
