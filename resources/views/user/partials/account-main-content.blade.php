<h2 class="h2">{{__('Mon historique')}}</h2>

<div class="row">


    <div class="col-12 has-m-bottom-2">
        <small>
            <p>{{__('Cumulez vos points pour bénéficier de la boutique Team Experts NORTH STAR.')}}</p>
            <ul class="has-list-style-type">
                <li>{{__('Un produit de la gamme d’adoucisseurs 2.0')}} <span class="has-color-accent">= 100 {{__('Points')}}</span></li>
                <li>{{__('Un produit de la gamme d’adoucisseurs 2.0+')}}<span class="has-color-accent">= 110 Points</span></li>
                <li>{{__('Un produit de la gamme d’adoucisseurs NSC Hybrid+')}} <span class="has-color-accent">= 150 Points</span></li>
            </ul>
            <a href="{{ url('pages/conditions-generales-de-vente') }}" class="link">{{__('Voir nos Conditions Générales de Ventes')}}</a>
        </small>
    </div>


    @include('components.blocks.flash-message')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="col-12">
        <article class="billing-upload has-m-bottom-4">
            <div>
                <button id="show_billing_upload" class="button">{{__('Télécharger une facture')}}</button>
            </div>
            <div id="billing_upload">
                <form method="POST" action="{{ route('bill') }}" enctype="multipart/form-data">

                    @csrf
                    <label for="bill" class="form-field label-hidden">
                        <input type="file" id="bill" name="bill">
                        <span>{{__('Choisir un fichier')}}</span>
                    </label>

                    <button type="submit" class="button">{{__('Envoyer')}}</button>
                </form>
            </div>
        </article>
    </div>


    <div class="col-md-6 col-12">
        <article class="table-sales">
            <table class="table">
                <thead>
                    <tr>
                        <th class="has-text-align-left">{{__('Achats Adoucisseurs')}}</th>
                        <th class="has-text-align-center">{{__('Quantité')}}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="has-uppercase">{{__('2.0')}}</td>
                        <td class="has-text-align-center">{{$user->getUdTotal()}}</td>
                    </tr>
                    <tr>
                        <td class="has-uppercase">{{__('2.0+')}}</td>
                        <td class="has-text-align-center">{{$user->getUdPlusTotal()}}</td>
                    </tr>
                    <tr>
                        <td class="has-uppercase">{{__('NSC Hybrid+')}}</td>
                        <td class="has-text-align-center">{{$user->getNcsHybridTotal()}}</td>
                    </tr>
                </tbody>
            </table>
        </article>
    </div>


    <div class="col-md-6 col-12">
        <article class="table-points">
            <table class="table">
                <thead>
                    <tr>
                        <th class="has-text-align-left">{{__('Cumul Point')}}s</th>
                        <th class="has-text-align-center">{{__('Quantité')}}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="has-uppercase">{{__('Total')}}</td>
                        <td class="has-text-align-center">{{$user->getTotalPoints()}}</td>
                    </tr>
                    <tr>
                        <td class="has-uppercase">{{__('Disponibles')}}</td>
                        <td class="has-text-align-center">{{$user->getAvailabledPoints()}}</td>
                    </tr>
                    <tr>
                        <td class="has-uppercase">{{__('Utilisés')}}</td>
                        <td class="has-text-align-center">{{$user->getUsedPoints()}}</td>
                    </tr>
                </tbody>
            </table>
        </article>
    </div>
    

    {{-- COMMANDES BEGIN --}}
    <div class="col-12">
        <h4 class="h4 has-m-top-4 has-m-bottom-2">{{__('Mes commandes')}}</h4>
        <article class="table-orders">

            <table id="table-orders" class="table table-large">
                <thead>
                    <tr>
                        <th class="has-text-align-left">{{__('N° de commande')}}</th>
                        <th class="has-text-align-center">{{__('Date')}}</th>
                        <th class="has-text-align-center">{{__('Produits')}}</th>
                        <th class="has-text-align-center">{{__('Points')}}</th>
                        <th class="has-text-align-center">{{__('Status')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($user->order->whereIn('status', [1, 2, 3])->reverse() as $order)
                        <tr>
                            <td class="has-text-align-left">{{ $order->reference }}</td>
                            <td class="has-text-align-center">{{ date('m/d/Y', strtotime($order->created_at)) }}</td>
                            <td class="has-text-align-center">
                                @foreach ($order->variant as $variant)
                                    <small>{{ $variant->product->name }} {{ $variant->name }} x {{ $variant->order_variant->quantity }} <br></small>
                                @endforeach
                            </td>
                            @if ($order->status == 3)
                                <td class="has-text-align-center invalid">{{__('Non disponible')}}</td>
                            @else 
                                <td class="has-text-align-center">{{ $order->points }}</td>
                            @endif
                            @switch($order->status)
                                @case(1)
                                    <td class="has-text-align-center info">{{__('En attente')}}</td>
                                @break
                                @case(2)
                                    <td class="has-text-align-center valid">{{__('Validée')}}</td>
                                @break
                                @case(3)
                                    <td class="has-text-align-center invalid">{{__('Refusée')}}</td>
                                @break
                                @default
                                    <td class="has-text-align-center warn">{{__('Non disponible')}}</td>
                            @endswitch
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="table-small">
                <div class="flex justify-content-between table-small__head">
                    <span>{{__('N° de commande')}}</span>&nbsp
                    <span>{{__('Date')}}</span>
                </div>
                @foreach ($user->order->whereIn('status', [1, 2, 3])->reverse() as $order)
                <article class="table-item">
                    <div class="flex justify-content-between table-item__head">
                        @if ($order->reference)
                            <span class="has-underline">
                                {{ $order->reference }}
                            </span>&nbsp
                        @else
                            <span class="has-underline">
                                <i>{{__('En attente')}}</i>
                            </span>&nbsp
                        @endif
                        <span>{{ date('m/d/Y', strtotime($order->created_at)) }}</span>
                    </div>
                    <section class="table-item__content">
                        <div class="flex justify-content-between">
                            {{__('Produits')}} :
                            <div>
                                @foreach ($order->variant as $variant)
                                <small style="display: block;">
                                    {{ $variant->product->name }} {{ $variant->name }} x {{ $variant->order_variant->quantity }}
                                </small>
                                @endforeach
                            </div>
                        </div>
                        <div class="flex justify-content-between">
                            <span>{{__('Points')}} : </span>&nbsp
                            @if ($order->status == 3)
                                <span>{{__('Non disponible')}}</span>
                            @else 
                                <span>{{ $order->points }}</span>
                            @endif   
                        </div>
                        <div class="flex justify-content-between">
                            <span>{{__('Status')}} : </span>&nbsp
                            @switch($order->status)
                                @case(1)
                                    <span class="info">{{__('En attente')}}</span>
                                @break
                                @case(2)
                                    <span class="valid">{{__('Validée')}}</span>
                                @break
                                @case(3)
                                    <span class="invalid">{{__('Refusée')}}</span>
                                @break
                                @default
                                    <span class="warn">{{__('Non disponible')}}</span>
                            @endswitch                 
                        </div>
                    </section>
                </article>
                @endforeach
            </div>

        </article>
    </div>
    {{-- COMMANDES ENDS --}}


    {{-- FACTURES BEGIN --}}
    <div class="col-12">
        <h4 class="h4 has-m-top-4 has-m-bottom-2">{{__('Mes factures')}}</h4>
        <article class="table-billings">

            <table id="table-billings" class="table table-large">
                <thead>
                    <tr>
                        <th class="has-text-align-left">{{__('N° Facture - Fournisseur')}}</th>
                        <th class="has-text-align-center">{{__('Montant TTC')}}</th>
                        <th class="has-text-align-center">{{__('Date téléchargement')}}</th>
                        <th class="has-text-align-center">{{__('Points')}}</th>
                        <th class="has-text-align-center">{{__('État')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($user->bill->reverse() as $billing)
                        <tr>
                            @if ($billing->id && $user->name)
                                <td class="has-uppercase">
                                    {{$billing->id}} - {{$user->name}}
                                </td>
                            @else
                                <td>
                                    <i>{{__('Facture téléchargée')}}</i>
                                </td>
                            @endif
                            <td class="has-text-align-center">@if(!empty($billing->amount)){{$billing->amount}} € @endif</td>
                            <td class="has-text-align-center">{{$billing->created_at}}</td>
                            <td class="has-text-align-center">{{$billing->points}}</td>
                            @switch($billing->status)
                                @case(1)
                                    <td class="has-text-align-center has-uppercase valid">{{__('Validée')}}</td>
                                @break
                                @case(2)
                                    <td class="has-text-align-center has-uppercase invalid">{{__('Refusée')}}</td>
                                @break
                                @case(0)
                                    <td class="has-text-align-center has-uppercase info">{{__('En attente')}}</td>
                                @break
                                @default
                                    <td class="has-text-align-center has-uppercase warn">{{__('Non disponible')}}</td>
                            @endswitch
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="table-small">
                <div class="flex justify-content-between table-small__head">
                    <span>{{__('N° Facture - Fournisseur')}}</span>&nbsp
                    <span>{{__('Montant TTC')}}</span>
                </div>
                @foreach ($user->order->whereIn('status', [1, 2, 3])->reverse() as $order)
                <article class="table-item">
                    <div class="flex justify-content-between table-item__head">
                        @if ($billing->id && $user->name)
                            <span class="has-uppercase has-underline">
                                {{$billing->id}} - {{$user->name}}
                            </span>&nbsp
                        @else
                            <span>
                                <i>{{__('Facture téléchargée')}}</i>
                            </span>&nbsp
                        @endif
                        <span>@if(!empty($billing->amount)){{$billing->amount}} € @endif</span>
                    </div>
                    <section class="table-item__content">
                        <div class="flex justify-content-between">
                            <span>{{__('Date téléchargement')}} : </span>&nbsp
                            <span>{{$billing->created_at}}</span>
                        </div>
                        <div class="flex justify-content-between">
                            <span>{{__('Points')}} : </span>&nbsp
                            <span>{{$billing->points}}</span>
                        </div>
                        <div class="flex justify-content-between">
                            <span>{{__('État')}} : </span>&nbsp
                            @switch($billing->status)
                                @case(1)
                                    <span class="has-uppercase valid">{{__('Validée')}}</span>
                                @break
                                @case(2)
                                    <span class="has-uppercase invalid">{{__('Refusée')}}</span>
                                @break
                                @case(0)
                                    <span class="has-uppercase info">{{__('En attente')}}</span>
                                @break
                                @default
                                    <span class="has-uppercase warn">{{__('Non disponible')}}</span>
                            @endswitch             
                        </div>
                    </section>
                </article>
                @endforeach
            </div>

        </article>
    </div>
    {{-- FACTURES ENDS --}}


</div>
