<h2 class="h2">{{__('Mon profil')}}</h2>


<article class="user-sidebar-element">
    <span>{{$user->id}}</span>
</article>


<article class="user-sidebar-element">
    <span>{{$user->name}}</span>
</article>


<article class="user-sidebar-element">
    <span>{{$user->billing_address}}, {{$user->billing_address_line_2}}</span>
</article>


<article class="user-sidebar-element">
    <span>{{$user->billing_city}}</span>
</article>

<article class="user-sidebar-element">
    <span>{{$user->billing_zip}}</span>
</article>


<article class="user-sidebar-element">
    <a href="{{route('user-edit')}}" class="user-action-link link-simple">{{__('Modifier mes données')}}</a>
    <a href="{{ route('update-password')}}" class="user-action-link link-simple">{{__('Modifier mon mot de passe')}}</a>
</article>
