@extends('layouts.registration')

@section('registration-step-content')

    @include('registration.partials.navigation', [
        $current_step = 3
    ])
    
    @include('registration.partials.form')

@endsection