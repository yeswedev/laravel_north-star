@extends('layouts.registration')

@section('registration-step-content')

    @include('registration.partials.navigation', [
        $current_step = 1
    ])
    
    @include('registration.partials.form', [
        $civilities = ['Mme','Mr']
    ])

@endsection