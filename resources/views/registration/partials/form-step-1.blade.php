<section class="registration-form">
    @if (Auth::check())
        <form method="POST" action="{{ route('edit-registration-step-1') }}">
        @method('PUT')
    @else 
        <form method="POST" action="{{ route('register') }}">
    @endif


        @csrf


        <input type="hidden" name="user_role" value="member">


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        
        {{-- BEGIN VOS INFORMATIONS --}}
        <section class="registration-form-section">
            <div class="row">

                <div class="col-12">
                    <p class="form-notification form-notification--info">* champs obligatoires</p>
                    <h4 class="h4">{{__('Vos informations')}}</h4>
                </div>

                <div class="col-md-6 col-12">
                    <label for="title" class="form-field label-hidden">
                        <select name="title" id="title" required>
                            <option value="{{__('Civilité')}} *" disabled selected>{{__('Civilité')}} *</option>
                            @foreach ($civilities as $civility)
                                @if (isset($user_full->title) && $user_full->title == $civility)
                                    <option value="{{$civility}}" selected>{{$civility}}</option>
                                @elseif ( Session::has('title') && Session::get('title') == $civility )
                                    <option value="{{$civility}}" selected>{{$civility}}</option>
                                @else
                                    <option value="{{$civility}}">{{$civility}}</option>
                                @endif
                            @endforeach
                        </select>
                        <span>{{__('Civilité')}} *</span>
                    </label>

                    <label for="lastname" class="form-field label-hidden">
                        <input 
                            type="text" name="lastname" id="lastname" 
                            placeholder="{{__('Nom')}} *" 
                            @if (isset($user_full->lastname))
                                value="{{$user_full->lastname}}"
                            @elseif (Session::has('lastname'))
                                value="{{Session::get('lastname')}}"
                            @else 
                                value=""
                            @endif
                            required>
                        <span>{{__('Nom')}} *</span>
                    </label>

                    <label for="firstname" class="form-field label-hidden">
                        <input 
                            type="text" name="firstname" id="firstname" 
                            placeholder="{{__('Prénom')}} *" 
                            @if (isset($user_full->firstname))
                                value="{{$user_full->firstname}}"
                            @elseif (Session::has('firstname'))
                                value="{{Session::get('firstname')}}"
                            @else 
                                value=""
                            @endif
                            required>
                        <span>{{__('Prénom')}} *</span>
                    </label>

                    <label for="birth" class="form-field">
                        <input 
                            type="date" name="birth" id="birth" 
                            placeholder="{{__('Date de naissance')}} *" 
                            @if (isset($user_full->birth))
                                value="{{date('Y-m-d', strtotime($user_full->birth))}}"
                            @elseif (Session::has('birth'))
                                value="{{ Session::get('birth') }}"
                            @else 
                                value=""
                            @endif
                            min="1910-01-01" max="<?php echo date('Y-m-d'); ?>" 
                            required>
                        <span>{{__('Date de naissance')}} *</span>
                    </label>
                </div>

                <div class="col-md-6 col-12">
                    <label for="function" class="form-field label-hidden">
                        <input 
                            type="text" name="function" id="function" 
                            placeholder="{{__('Fonction')}} *" 
                            @if (isset($user_full->function))
                                value="{{$user_full->function}}"
                            @elseif (Session::has('function'))
                                value="{{Session::get('function')}}"
                            @else 
                                value=""
                            @endif
                            required>
                        <span>{{__('Fonction')}} *</span>
                    </label>

                    <label for="phone" class="form-field label-hidden">
                        <input 
                            type="tel" name="phone" id="phone" 
                            placeholder="{{__('Téléphone fixe')}}" 
                            @if (isset($user_full->phone))
                                value="{{$user_full->phone}}"
                            @elseif (Session::has('phone'))
                                value="{{Session::get('phone')}}"
                            @else 
                                value=""
                            @endif >
                        <span>{{__('Téléphone fixe')}}</span>
                    </label>

                    <label for="mobile" class="form-field label-hidden">
                        <input 
                            type="tel" name="mobile" id="mobile" 
                            placeholder="{{__('Téléphone portable *')}}" 
                            @if (isset($user_full->mobile))
                                value="{{$user_full->mobile}}"
                            @elseif (Session::has('mobile'))
                                value="{{Session::get('mobile')}}"
                            @else 
                                value=""
                            @endif
                            required>
                        <span>{{__('Téléphone portable *')}}</span>
                    </label>

                    <label for="email" class="form-field label-hidden">
                        <input 
                            type="email" name="email" id="email" 
                            placeholder="{{__('Email')}} *" 
                            @if (isset($user_full->email))
                                value="{{$user_full->email}}"
                            @elseif (Session::has('email'))
                                value="{{Session::get('email')}}"
                            @else 
                                value=""
                            @endif
                            required>
                        <span>{{__('Email')}} *</span>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </label>
                </div>

            </div>
        </section>
        {{-- ENDS VOS INFORMATIONS --}}


        @if(empty($user_full))
        {{-- BEGIN MOT DE PASSE --}}
        <section class="registration-form-section">
            <div class="row">

                <div class="col-12">
                    <h4 class="h4">{{__('Choisissez un mot de passe')}}</h4>
                </div>

                <div class="col-md-6 col-12">
                    <label for="password" class="form-field label-hidden">
                        <input type="password" name="password" id="password" placeholder="{{__('Choisissez un mot de passe')}} *" required>
                        <span>{{__('Choisissez un mot de passe')}} *</span>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <p class="form-notification form-notification--info">Minimum 9 caractères dont 3 chiffres.</p>
                    </label>
                </div>

                <div class="col-md-6 col-12">
                    <label for="password-confirm" class="form-field label-hidden">
                        <input id="password-confirm" type="password" placeholder="{{__('Confirmez le mot de passe')}} *" name="password_confirmation" required>
                        <span>{{__('Confirmez le mot de passe')}} *</span>
                    </label>
                </div>

            </div>
        </section>
        {{-- ENDS MOT DE PASSE --}}
        @endif

        <div class="row justify-content-end">
            <div class="col-12">
                <div class="flex justify-content-end">
                     <button type="submit" class="button has-m-top-2">{{__('Étape suivante')}}</button>
                </div>
            </div>
        </div>


    </form>
</section>
