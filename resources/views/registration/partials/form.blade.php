@if (url()->current() == route('registration'))
    @include('registration.partials.form-step-1')
@endif


@if (url()->current() == route('registration-2'))
    @include('registration.partials.form-step-2')
@endif


@if (url()->current() == route('registration-3'))
    @include('registration.partials.form-step-3')
@endif