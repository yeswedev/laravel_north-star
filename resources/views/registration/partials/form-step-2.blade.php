<section class="registration-form">
    @if ($user_full->billing_address !== null)
        <form method="POST" action="{{ route('edit-registration-step-2') }}">
    @else 
        <form method="POST" action="{{ route('company-registration') }}">
    @endif

        @method('PUT')
        @csrf

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {{-- BEGIN VOTRE ENTREPRISE --}}
        <section class="registration-form-section">
            <div class="row">

                <div class="col-12">
                    <p class="form-notification form-notification--info">* champs obligatoires</p>
                    <h4 class="h4">Votre entreprise</h4>
                    <p class="form-notification form-notification--info">Adresse utilisée pour la livraison des cadeaux.</p>
                </div>

                <div class="col-md-6 col-12">
                    <label for="name" class="form-field label label-hidden">
                        <input type="text" name="name" id="name" placeholder="Raison Sociale *" 
                            @if (Session::has('name'))
                                value="{{Session::get('name')}}"
                            @else 
                                value=""
                            @endif  
                            required>
                        <span>Raison Sociale *</span>
                    </label>

                    <label for="company_address" class="form-field label-hidden">
                        <input type="text" name="company_address" id="company_address" placeholder="Adresse postale *" 
                            @if (isset($user_full->company_address))
                                value="{{$user_full->company_address}}"
                            @elseif (Session::has('company_address'))
                                value="{{Session::get('company_address')}}"
                            @else 
                                value=""
                            @endif  
                            required>
                        <span>Adresse postale *</span>
                    </label>

                    <label for="company_address_line_2" class="form-field label-hidden">
                        <input type="text" name="company_address_line_2" id="company_address_line_2" placeholder="Complément d’adresse"
                            @if (isset($user_full->company_address_line_2))
                                value="{{$user_full->company_address_line_2}}"
                            @elseif (Session::has('company_address_line_2'))
                                value="{{Session::get('company_address_line_2')}}"
                            @else 
                                value=""
                            @endif  
                            >
                        <span>Complément d’adresse</span>
                    </label>

                    <label for="company_city" class="form-field label-hidden">
                        <input type="text" name="company_city" id="company_city" placeholder="Ville *"
                            @if (isset($user_full->company_city))
                                value="{{$user_full->company_city}}"
                            @elseif (Session::has('company_city'))
                                value="{{Session::get('company_city')}}"
                            @else 
                                value=""
                            @endif  
                            required>
                        <span>Ville *</span>
                    </label>
                    
                    <label for="company_zipcode" class="form-field label-hidden">
                        <input type="text" name="company_zipcode" id="company_zipcode" placeholder="Code Postal *"
                            @if (isset($user_full->company_zipcode))
                                value="{{$user_full->company_zipcode}}"
                            @elseif (Session::has('company_zipcode'))
                                value="{{Session::get('company_zipcode')}}"
                            @else 
                                value=""
                            @endif  
                            required>
                        <span>Code Postal *</span>
                    </label>
                </div>

                <div class="col-md-6 col-12">
                    <label for="company_siret" class="form-field label-hidden">
                        <input type="text" name="company_siret" id="company_siret" placeholder="N° SIRET *"
                            @if (isset($user_full->company_siret))
                                value="{{$user_full->company_siret}}"
                            @elseif (Session::has('company_siret'))
                                value="{{Session::get('company_siret')}}"
                            @else 
                                value=""
                            @endif  
                            onfocus="this.placeholder='exemple : 12345682400034'"
                            onblur="this.placeholder='N° SIRET *'"
                            required>
                        <span>N° SIRET *</span>
                    </label>

                    <label for="company_ape" class="form-field label-hidden">
                        <input type="text" name="company_ape" id="company_ape" placeholder="Code APE *"
                            @if (isset($user_full->company_ape))
                                value="{{$user_full->company_ape}}"
                            @elseif (Session::has('company_ape'))
                                value="{{Session::get('company_ape')}}"
                            @else 
                                value=""
                            @endif  
                            onfocus="this.placeholder='exemple : 9609z'"
                            onblur="this.placeholder='Code APE *'"
                            required>
                        <span>Code APE *</span>
                    </label>

                    <label for="company_tva" class="form-field label-hidden">
                        <input type="text" name="company_tva" id="company_tva" placeholder="N° TVA intracommunautaire *"
                            @if (isset($user_full->company_tva))
                                value="{{$user_full->company_tva}}"
                            @elseif (Session::has('company_tva'))
                                value="{{Session::get('company_tva')}}"
                            @else 
                                value=""
                            @endif  
                            onfocus="this.placeholder='exemple : FR40123456824'"
                            onblur="this.placeholder='N° TVA intracommunautaire *'"
                            required>
                        <span>N° TVA intracommunautaire *</span>
                    </label>

                    <label for="size" class="form-field label-hidden">
                        <select name="size" id="size" required>
                            <option value="{{__('Taille')}} *" disabled selected>{{__('Taille (polos/tee-shirts)')}} *</option>
                            @foreach ($sizes as $size)
                                @if (isset($user_full->size) && $user_full->size == $size)
                                    <option value="{{$size}}" selected>{{$size}}</option>
                                @elseif (Session::has('size') && Session::get('size') == $size)
                                    <option value="{{$size}}" selected>{{$size}}</option>
                                @else 
                                    <option value="{{$size}}">{{$size}}</option>
                                @endif
                            @endforeach
                        </select>
                        <span>{{__('Taille souhaitée pour commande de polos/tee-shirts')}} *</span>
                    </label>
                    <span class="form-notification form-notification--info">Taille utilisée pour l’envoi de votre pack Bienvenue.</span>
                </div>

            </div>
        </section>
        {{-- ENDS VOTRE ENTREPRISE --}}


        {{-- BEGIN ADRESSE DE FACTURATION --}}
        <section class="registration-form-section">
            <div class="row">

                <div class="col-12">
                    <h4 class="h4">Adresse de facturation</h4>
                </div>

                <div class="col-md-6 col-12">
                    <label for="billing_address" class="form-field label-hidden">
                        <input type="text" name="billing_address" id="billing_address" placeholder="Adresse *"
                            @if (isset($user_full->billing_address))
                                value="{{$user_full->billing_address}}"
                            @elseif (Session::has('billing_address'))
                                value="{{Session::get('billing_address')}}"
                            @else 
                                value=""
                            @endif  
                            required>
                        <span>Adresse *</span>
                    </label>
                    <label for="billing_address_line_2" class="form-field label-hidden">
                        <input type="text" name="billing_address_line_2" id="billing_address_line_2" placeholder="Complément d'adresse"
                            @if (isset($user_full->billing_address_line_2))
                                value="{{$user_full->billing_address_line_2}}"
                            @elseif (Session::has('billing_address_line_2'))
                                value="{{Session::get('billing_address_line_2')}}"
                            @else 
                                value=""
                            @endif
                            >
                        <span>Complément d'adresse</span>
                    </label>
                </div>

                <div class="col-md-6 col-12">                    
                    <label for="billing_city" class="form-field label-hidden">
                        <input type="text" name="billing_city" id="billing_city" placeholder="Ville *"
                            @if (isset($user_full->billing_city))
                                value="{{$user_full->billing_city}}"
                            @elseif (Session::has('billing_city'))
                                value="{{Session::get('billing_city')}}"
                            @else 
                                value=""
                            @endif  
                            required>
                        <span>Ville *</span>
                    </label>
                    <label for="billing_zip" class="form-field label-hidden">
                        <input type="text" name="billing_zip" id="billing_zip" placeholder="Code Postal *"
                            @if (isset($user_full->billing_zip))
                                value="{{$user_full->billing_zip}}"
                            @elseif (Session::has('billing_zip'))
                                value="{{Session::get('billing_zip')}}"
                            @else 
                                value=""
                            @endif  
                            required>
                        <span>Code Postal *</span>
                    </label>
                </div>

            </div>
        </section>
        {{-- ENDS ADRESSE DE FACTURATION --}}


        <div class="row justify-content-end">
            <div class="col-12">
                <div class="flex justify-content-between">
                    <a href="{{route('registration')}}" class="link-back has-m-top-2"><i class="fas fa-chevron-circle-left"></i> Retour</a>
                    <button type="submit" class="button has-m-top-2">{{__('Étape suivante')}}</button>
                </div>
            </div>
        </div>


    </form>
</section>
