<section class="registration-form">
    <form method="POST" action="{{route('confirmation-registration')}}">
        @method('PUT')
        @csrf

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <section class="registration-form-section">
                <div class="row">

                    <div class="col-12">
                        <h2 class="h2">{{__('Récapitulatif de votre abonnement')}}</h2>
                        <p class="form-notification form-notification--info">* champs obligatoires</p>
                    </div>

                    <div class="col-md-6 col-12">
                        <div class="registration-informations">
                            <h4 class="h4 has-m-top-2">Vos informations</h4>
                            <p>{{$user['title']}} {{$user['firstname']}} {{$user['lastname']}}</p>
                            <p>{{$user['function']}}</p>
                            <p>Société {{$user['name']}}</p>
                            <p>{{$user['company_address']}}, {{$user['company_address_line_2']}}</p>
                            <p>{{$user['company_zipcode']}} {{$user['company_city']}}</p>
                            <p>Tel. fixe : {{$user['phone']}}</p>
                            <p>Tel. mobile : {{$user['mobile']}}</p>
                            <p>Email : {{$user['email']}}</p>
                        </div>
                    </div>

                    <div class="col-md-6 col-12">
                        <div class="registration-informations">
                            <h4 class="h4 has-m-top-2">Adresse de facturation</h4>
                            <p>Société {{$user['name']}}</p>
                            <p>{{$user['billing_address']}}, {{$user['billing_address_line_2']}}</p>
                            <p>{{$user['billing_zipcode']}} {{$user['billing_city']}}</p>
                            <p>N° SIRET : {{$user['company_siret']}}</p>
                            <p>Code APE : {{$user['company_ape']}}</p>
                            <p>N° TVA Intracommunautaire : {{$user['company_tva']}}</p>

                            <label for="cgv_agrement" class="has-m-top-2">
                                <input type="checkbox" id="cgv_agrement" name="cgv_agrement" required value="on"    >
                                <span>J’ai lu et j’accepte les conditions genérales de vente *</span>
                            </label>

                            <div>
                                <small>Votre compte bancaire sera débité sous 48h.</small>
                            </div>
                        </div>

                    </div>

                </div>
        </section>


        <div class="row justify-content-end">
            <div class="col-12">
                <div class="flex justify-content-between">
                    <a href="{{route('registration-2')}}" class="link-back has-m-top-2"><i class="fas fa-chevron-circle-left"></i> Retour</a>
                    <button type="submit" class="button has-m-top-2">{{__('Valider')}}</button>
                </div>
            </div>
        </div>


    </form>
</section>
