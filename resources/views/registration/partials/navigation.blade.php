@if ($current_step == 1)
    <section class="registration-navigation">
        <nav>
            <ul class="arrow-steps">    
                <li class="step current">
                    <span class="step-name">Étape</span> 
                     <span class="step-number">1</span>
                    <a href="#"></a>
                </li>    
                <li class="step">
                    <span class="step-name">Étape</span> 
                     <span class="step-number">2</span>
                    <a href="#"></a>
                </li>    
                <li class="step">
                    <span class="step-name">Étape</span> 
                     <span class="step-number">3</span>
                    <a href="#"></a>
                </li>    
                <li class="step">
                    <span>Paiement</span>
                    <a href="#"></a>
                </li>    
            </ul>
        </nav>
    </section>

@elseif ($current_step == 2)

    <section class="registration-navigation">
        <nav>
            <ul class="arrow-steps">    
                <li class="step done">
                    <span class="step-name">Étape</span> 
                     <span class="step-number">1</span>
                    <a href="{{route('registration')}}"></a>
                </li>    
                <li class="step current">
                    <span class="step-name">Étape</span> 
                     <span class="step-number">2</span>
                    <a href="#"></a>
                </li>    
                <li class="step">
                    <span class="step-name">Étape</span> 
                     <span class="step-number">3</span>
                    <a href="#"></a>
                </li>    
                <li class="step">
                    <span>Paiement</span>
                    <a href="#"></a>
                </li>    
            </ul>
        </nav>
    </section>

@elseif ($current_step == 3)

    <section class="registration-navigation">
        <nav>
            <ul class="arrow-steps">    
                <li class="step done">
                    <span class="step-name">Étape</span> 
                     <span class="step-number">1</span>
                    <a href="{{route('registration')}}"></a>
                </li>    
                <li class="step done">
                    <span class="step-name">Étape</span> 
                     <span class="step-number">2</span>
                    <a href="{{route('registration-2')}}"></a>
                </li>    
                <li class="step current">
                    <span class="step-name">Étape</span> 
                     <span class="step-number">3</span>
                    <a href="#"></a>
                </li>    
                <li class="step">
                    <span>Paiement</span>
                    <a href="#"></a>
                </li>    
            </ul>
        </nav>
    </section>

@elseif ($current_step == 4)

    <section class="registration-navigation">
        <nav>
            <ul class="arrow-steps">    
                <li class="step done">
                    <span class="step-name">Étape</span> 
                     <span class="step-number">1</span>
                    <a href="{{route('registration')}}"></a>
                </li>    
                <li class="step done">
                    <span class="step-name">Étape</span> 
                     <span class="step-number">2</span>
                    <a href="{{route('registration-2')}}"></a>
                </li>    
                <li class="step done">
                    <span class="step-name">Étape</span> 
                     <span class="step-number">3</span>
                    <a href="{{route('registration-3')}}"></a>
                </li>    
                <li class="step current">
                    <span>Paiement</span>
                    <a href="#"></a>
                </li>    
            </ul>
        </nav>
    </section>
@endif