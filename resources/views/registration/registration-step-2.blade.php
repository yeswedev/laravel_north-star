@extends('layouts.registration')

@section('registration-step-content')

    @include('registration.partials.navigation', [
        $current_step = 2
    ])
    
    @include('registration.partials.form')

@endsection