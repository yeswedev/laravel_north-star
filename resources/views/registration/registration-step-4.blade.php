@extends('layouts.registration')

@push('scripts')
    @if (Spark::billsUsingStripe())
        <script src="https://js.stripe.com/v3/"></script>
    @else
        <script src="https://js.braintreegateway.com/v2/braintree.js"></script>
    @endif
@endpush

@section('registration-step-content')

    @include('registration.partials.navigation', [
        $current_step = 4
    ])
    <spark-settings :user="user" :teams="teams" inline-template>
        <div class="spark-screen container">
                <!-- Tabs -->
                <div class="spark-settings-tabs">
                    <!-- Billing Tabs -->
                    @if (Spark::canBillCustomers())
                      @if (Spark::hasPaidPlans())
                          <a class="nav-link" href="#subscription" aria-controls="subscription" role="tab" data-toggle="tab">
                              <h4 class="h4 has-m-top-2">{{__('Abonnement')}}</h4>
                          </a>
                      @endif
                    @endif
                </div>

                <!-- Tab cards -->
                <div class="">
                    <div class="tab-content">
                    <!-- Billing Tab Panes -->
                        @if (Spark::canBillCustomers())
                            @if (Spark::hasPaidPlans())
                            <!-- Subscription -->
                                <div role="tabcard" class="tab-pane" id="subscription">
                                    <div v-if="user">
                                        @include('spark::settings.subscription')
                                    </div>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </spark-settings>

    <div class="col-lg-8 col-md-10 col-12">
        <a href="{{route('registration-3')}}" class="link-back has-m-top-2"><i class="fas fa-chevron-circle-left"></i> Retour</a>
    </div>

@endsection
