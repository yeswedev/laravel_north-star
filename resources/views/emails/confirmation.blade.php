@component('mail::message')
# Bonjour {{$user->title}} {{$user->lastname}},

Toute l’équipe de North Star&trade; vous remercie pour votre adhésion à Team Experts et à notre charte des membres.

Vous recevrez votre Pack de Bienvenue très prochainement.

Le fonctionnement de Team Expert est simple :

1/ Achetez des produits North Star 2.0 ou   2.0+ ou   NSC Hybrid+

2/ Téléchargez vos factures d’achats

3/ Cumulez vos points pour bénéficier de la boutique Team Experts NORTH STAR&trade;

4/ Commandez grâce à vos points des dotations au choix dans notre boutique Team Experts de North StarTM
-    Un produit de la gamme d’adoucisseurs 2.0 = 100 Points
-    Un produit de la gamme d’adoucisseurs 2.0+ = 110 Points
-    L’Hybrid+ by North Star&trade; = 150 Points

Votre commercial régional {{$user->getContact()->title}}, vous contactera et se tient à votre disposition pour tous renseignements.

{{$user->getContact()->title}}

{{$user->saler_mail}}

{{$user->getContact()->user->mobile}}


A bientôt,<br>
## L'équipe North Star
[{{env('APP_URL')}}]({{env('APP_URL')}} "Club North Star")
@endcomponent
