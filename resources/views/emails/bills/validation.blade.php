@component('mail::message')
# Bonjour {{$bill->user->title}} {{$bill->user->lastname}},

L’équipe North Star vous confirme que la facture du {{$bill->created_at}} a été {{$bill->readable_status}}

Celle-ci vous donne droit à {{$bill->points}} points à utiliser au sein Club TEAM EXPERTS

A bientôt,<br>
## L'équipe North Star
[{{env('APP_URL')}}]({{env('APP_URL')}} "Club North Star")
@endcomponent