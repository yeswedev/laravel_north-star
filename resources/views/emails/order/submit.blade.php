@component('mail::message')
# Votre commande sur le Club North Star a été soumise à nos équipes


@component('mail::panel')
### Récapitulatif de la commande :

  + __Référence : __{{$order->user->name}} | {{$order->id}}
  + __Nombre de Points utilisé : __{{$order->getUsedPointsByOrder()}}

@if($order->variant)

@component('mail::table')
| Produit                   | Quantité                         |
| ------------------------- | -------------------------------- |
        @foreach($order->variant as $p)
| {{$p->getCompleteName()}} | {{$p->order_variant->quantity}}  |
        @endforeach
@endcomponent

@endif

@endcomponent

Nous nous efforcerons de la traiter dans les plus brefs délais.

N'hésitez pas à contacter votre commercial North Star pour toute informations concernant cette dernière,<br>
## Le Club North Star
[{{env('APP_URL')}}]({{env('APP_URL')}} "Club North Star")
@endcomponent
