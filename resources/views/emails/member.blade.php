@component('mail::message')
# Un nouveau membre dans votre périmètre vient de s'inscrire au Club North Star

@component('mail::panel')
### Vous trouverez ci-dessous les informations le concernant :

  * __Membre : __ {{$user->fullname}}
  * __Société : __ {{$user->name}}
  * __Adresse : __ {{$user->company_address}}
  * __Code Postal : __ {{$user->company_zipcode}}
  * __Ville : __ {{$user->company_city}}
  * __Département : __ {{$user->department->id}} | {{$user->department->name}}

@endcomponent

## Club North Star
[{{env('APP_URL')}}]({{env('APP_URL')}} "Club North Star")
@endcomponent


