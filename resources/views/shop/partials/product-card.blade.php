<div class="product-container">
    <article class="product">


        <header class="product-header">
            <div class="product-header-col">
                <h1 class="product-header__name">{{$product->product->name}} </h1>
                <span class="product-header__name--variant">{{$product->name}}</span>
                <span class="product-header__material"></span>
            </div>
            <div class="product-header-col">
                <span class="product-header__price">{{$product->points}} points</span>
                @if ($product->stock == 0)
                    <span class="product-header__quantity invalid">En rupture</span>
                @elseif ($product->stock <= $product->break_point)
                    <span class="product-header__quantity warn">Dernières pièces</span>
                @else
                <span class="product-header__quantity info">Disponible</span>
                @endif
            </div>
        </header>


        <section class="product-img">
        <img src="{{$product->getImageUrl()}}" alt="{{$product->product->name.' '.$product->name}}">
        </section>


        <footer class="product-footer">

            <product-card-form :product-data="{{ json_encode($product) }}"></product-card-form>

        </footer>


    </article>
</div>
