<section class="products-list">
    <div class="row">

        @foreach ($products as $product)
            <div class="col-sm-6 col-12">

                @component('shop.partials.product-card', [
                    'product' => $product
                ])
                @endcomponent

            </div>
        @endforeach

    </div>
</section>
