<section id="product-cart" class="product-cart">


    <div class="product-cart-header">
        <h4 class="h4">Mon panier</h4>
    </div>


    <div class="product-cart-list">
        @foreach ($cart as $cart_item)
            <div class="product-cart-item">                
                <span class="product-cart-item__name">
                    <div data-product-id="{{$cart_item['product']['id']}}" class="delete-item">-</div>
                    {{$cart_item['product']['name']}}
                </span>
                <span class="product-cart-item__quantity has-color-grey">{{$cart_item['quantity']}}</span>
            </div>
        @endforeach
    </div>


    <div class="product-cart-points">
        @php
            $total = 0;
            foreach ($cart as $cart_item) {
                $cart_item_total = $cart_item['product']['price'] * $cart_item['quantity'];
                $total = $total + $cart_item_total;
            }
            $points_left = $user_points - $total;
        @endphp
        <div class="product-cart-points__used">
            <span>Points de la commande</span>
            <span class="has-bold">{{$total}}</span>
        </div>
        <div class="product-cart-points__left">
            <span>Points restants</span>
            @if ($points_left < 0)
                <span class="has-bold invalid">{{$points_left}}</span>
            @else
                <span class="has-bold">{{$points_left}}</span>
            @endif
        </div>
    </div>


    <div class="product-cart-footer">
        @if ($points_left < 0)
            <button class="button is-fullwidth" disabled>Valider</button>
        @else
            <button class="button is-fullwidth">Valider</button>
        @endif
    </div>


</section>