@extends('layouts.container-sidebar-right')


@section('content')

    @component('components.hero', [
        'img_bg' => '/img/hero_bg.png',
        'img_content' => '/img/logo_north-star_team_experts.svg',
        'content' =>    '<h1 class="h1">Ma boutique</h1>',
        'user_name' => $user->name
    ])
    @endcomponent

    <div class="container">
        <div class="row">

            <div class="col-lg-8 col-12">
                <section class="main-content after-hero before-footer">
                    <flash-message></flash-message>
                    @include('shop.partials.products-list')
                </section>
            </div>

            <div class="col-lg-4 col-12">
                <section class="sidebar after-hero">                    
                    <div class="sidebar-container">
                        <shop-points></shop-points>
                        <div class="cart-notif">
                            <span class="info">Minimum de commande = 250 POINTS.</span>
                        </div>
                        <a href="{{route('cart')}}" class="button is-fullwidth has-m-bottom-4">Valider mon panier</a>
                    </div>
                </section>
            </div>    
            
        </div>
    </div>

@endsection
