<section class="cart">




    <table class="table table-large">
        <thead>
            <tr>
                <th class="has-text-align-left">Produits selectionnés</th>
                <th class="has-text-align-center">Quantité</th>
                <th class="has-text-align-center">Points</th>
                <th class="has-text-align-center">Supprimer</th>
            </tr>
        </thead>
        <tbody is="user-cart-products" ></tbody>
    </table>



    <div class="table-small">
        <h4 class="h4">Produits selectionnés</h4>

        <user-cart-products-small></user-cart-products-small>

    </div>



</section>
