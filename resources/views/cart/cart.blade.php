

@extends('layouts.container-sidebar-right')


@section('top-content')
    @component('components.hero', [
        'img_bg' => '/img/hero_bg.png',
        'img_content' => '/img/logo_north-star_team_experts.svg',
        'content' =>    '<h1 class="h1">Mon panier</h1>',
        'user_name' => $cart->user->name
    ])
    @endcomponent
@endsection


@section('main-content')

    <flash-message></flash-message>

    <div class="cart-notif">
        <span class="info">Minimum de commande = 250 POINTS.</span>
    </div>

    @include('cart.partials.products')

    <user-cart-notif></user-cart-notif>

    <a class="button has-m-top-2" href="{{route('shop')}}">Ajouter des cadeaux</a>

@endsection


@section('sidebar-right')

    <user-cart-points></user-cart-points>

@endsection


@section('bottom-content')
@endsection
