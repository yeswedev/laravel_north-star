<section class="hero" style="
    background: url({{ asset($img_bg) }});
    background-size: cover;
    background-position: center top;
    ">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-12 @if(url()->current() == route('home'))page-content--home @endif">

                @section('hero-content')
                <article class="hero-content">

                    <div class="hero-content__img">
                        <img src="{{ asset($img_content) }}" alt="Logotype de la Team Experts de North Star.">
                    </div>
                    @if(url()->current() == route('login'))

                        <div class="hero-content__text">
                            <p class="hero-text--home">
                                Cumulez vos points pour bénéficier
                                des avantages du Club Team Experts
                                de NORTH STAR
                            </p>
                        </div>

                    @elseif(url()->current() == route('shop'))

                        <div class="hero-content__text">
                            {!! $content !!}
                            <a href="{{route('home')}}"><span class="user-id">{{$user_name}}</span></a>
                        </div>

                    @elseif(url()->current() == route('home'))

                        <div class="hero-content__text">
                            {!! $content !!}
                            <a href="{{route('home')}}"><span class="user-id">{{$user_name}}</span></a>
                        </div>

                    @elseif(url()->current() == route('cart'))

                        <div class="hero-content__text">
                            {!! $content !!}
                            <a href="{{route('home')}}"><span class="user-id">{{$user_name}}</span></a>
                        </div>

                    @else

                        <div class="hero-content__text">
                            {!! $content !!}
                        </div>

                    @endif
                </article>
                @show

            </div>
        </div>
    </div>
</section>
