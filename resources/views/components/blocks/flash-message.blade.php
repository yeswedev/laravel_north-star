@if ($message = Session::get('success'))
<div class="flash-popup pop-yes valid">
  @foreach($message as $item)
    <span>{{$item}}</span>
  @endforeach
</div>
@endif
@if ($message = Session::get('error'))
<div class="flash-popup pop-yes invalid">
  @foreach($message as $item)
      <span>{{ $item }}</span>
  @endforeach
</div>
@endif
@if ($errors->any())
<div class="flash-popup pop-yes warn">
  <span>{{__('Nous ne pouvons traiter votre demande, Il y a des erreurs dans votre formulaire')}}</span>
</div>
@endif
