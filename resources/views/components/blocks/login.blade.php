<section class="login">
  <form action="/login" method="POST" enctype="multipart/form-data" class="flex flex-column">
    @csrf
    <h2 class="has-uppercase h2 login__title">{{__('S\'identifier')}}</h2>
    <p class="has-uppercase login__catchline">{{__('Pas encore membre')}}</p>
    <p class="has-uppercase login__catchline has-color-primary has-semi-bold">{{__('Abonnez-vous')}} <span class="has-light">au club</span></p>
    <p class="login__catchline has-uppercase has-color-primary has-semi-bold">{{__('Team experts - 125')}}<span class="minus">,00<span class="index">€</span></span>{{__(' ht / an')}}</p>
    <label class="form-field login__label label-hidden" for="email">
      <input type="email" name="email" class="input input--grey login__input--grey  login__block @error('email') is-invalid @enderror" placeholder="Email ou identifiant" value="{{ old('email') }}" required >
      <span class="placeholder">{{__('Email')}}</span>
    </label>
    <label class="form-field login__label label-hidden" for="password">
      <input type="password" name="password" class="input input--grey login__input--grey login__block @error('password') is-invalid @enderror " placeholder="Mot de Passe">
      <span class="placeholder login__placeholder">{{__('Mot de Passe')}}</span>
      <p class="alert hidden"></p>
    </label>
    @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    <button type="submit" class="button login__block login__input--submit" value="S'identifier">{{__('S\'identifier')}}</button>
  </form>
  <div class="login__links flex">
    @if (Route::has('password.request'))
    <a href="{{ route('password.request')}}" class="link link--blue login__link has-display-block">{{__('Mot de passe oublié')}}</a>
    @endif
    <a href="{{route('registration')}}" class="button login__block">{{__('Abonnez-vous')}}</a>
  </div>

  <small>
    <p class="has-m-top-2">
        En vous inscrivant à Team Experts de North Star, vous acceptez les conditions stipulées dans nos <a href="{{ url('pages/conditions-generales-de-vente') }}" class="link">conditions générales</a>, notre <a href="{{ url('pages/charte-des-membres') }}" class="link">charte de protection des données personnelles</a> (notamment l’utilisation de cookies et autres technologies et <a href="{{ url('pages/charte-des-membres') }}" class="link">nos conditions d’utilisation.</a>
    </p>
  </small>
</section>
