@extends('layouts.app')


@section('content')

    @component('components.hero', [
        'img_bg' => '/img/hero_bg.png',
        'img_content' => '/img/logo_north-star_team_experts.svg',
        'content' =>    '<h1 class="h1">'.$page["title"].'</h1>'
    ])
    @endcomponent

    <div class="after-hero before-footer">
        <div class="container">
            <section class="cms-page">
                <article>{!! $page['content'] !!}</article>
            </section>
        </div>
    </div>

@endsection
