@extends('layouts.app')


@section('content')

    @component('components.hero', [
        'img_bg' => '/img/hero_bg.png',
        'img_content' => '/img/logo_north-star_team_experts.svg',
        'content' =>    '<h1 class="h1">Erreur 419</h1>'
    ])
    @endcomponent

    <div class="after-hero before-footer">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-8 col-sm-10 col-12">

                <h2 class="h2 has-m-top-4 has-m-bottom-4">{{__('Page expirée')}}</h2>
                <p class="has-m-top-4 has-m-bottom-4"><strong>{{__('La page que vous cherchez n\'est plus accessible. Pensez à recharger la page ou y revenir en suivant le bon lien tout en étant connecté.')}}</strong></p>

                <a class="button has-m-bottom-4" href="{{route('home')}}">Retour à l'accueil</a>

                </div>
            </div>
        </div>
    </div>

@endsection
