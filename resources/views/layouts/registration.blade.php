@extends('layouts.app')

@section('content')
    <div class="registration-page">


        @component('components.hero', [
            'img_bg' => '/img/hero_bg.png',
            'img_content' => '/img/logo_north-star_team_experts.svg', 
            'content' =>    '<h1 class="h1">Abonnez-vous</h1>
                            <p class="hero-text">pour 150,00<sup>€</sup>/an TTC</p>
                            <p class="hero-text">reservé aux professionnels</p>'
        ])
        @endcomponent

        <div class="registration-step" style="
            background: url({{ asset('/img/registration_bg.png') }});
            background-size: cover;
            background-position: center;
            ">        
            <div class="after-hero before-footer">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 col-md-10 col-12">
                                
                            @yield('registration-step-content')     
                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
@endsection