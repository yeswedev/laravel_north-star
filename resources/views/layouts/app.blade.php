<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('partials.head')
    <body>
        <div id="app">
            <div id="spark-app" v-cloak>
                @include('partials.header')
                <main class="main">
                    @yield('content')
                </main>
                            <!-- Application Level Modals -->
                @if (Auth::check())
                    @include('spark::modals.notifications')
                    @include('spark::modals.support')
                    @include('spark::modals.session-expired')
                @endif
                @include('partials.footer')
            </div>
        </div>
    </body>
    @include('partials.scripts')
</html>
