@extends('layouts.app')


@section('content')
    

    @component('components.hero', [
        'img_bg' => '/img/hero_bg.png',
        'img_content' => '/img/logo_north-star_team_experts.svg', 
        'content' =>    '<h1 class="h1">Modifier</h1><p class="hero-text">mes données</p>'
    ])
    @endcomponent

    <section style="
        background: url({{ asset('/img/registration_bg.png') }});
        background-size: cover;
        background-position: center;
        ">
        <div class="after-hero before-footer">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8 col-md-10 col-12">
                        
                        @yield('main-content')   
                            
                    </div>
                </div>
            </div>
        </div>
    </section>
    


@endsection