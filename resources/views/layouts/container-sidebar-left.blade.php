@extends('layouts.app')


@section('content')


    @yield('top-content')

    <div class="container-sidebar container-sidebar--left"  style="
        background: url({{ asset('/img/registration_bg.png') }});
        background-size: cover;
        background-position: center;
        ">
        <div class="container">
            <div class="row">

                <div class="col-lg-4 col-12">
                    <section class="sidebar after-hero">
                        @yield('sidebar-left')
                    </section>
                </div>
    
                <div class="col-lg-8 col-12">
                    <section class="main-content after-hero before-footer">
                        @yield('main-content')
                    </section>
                </div>
                
            </div>
        </div>
    </div>

    @yield('bottom-content')
        

@endsection