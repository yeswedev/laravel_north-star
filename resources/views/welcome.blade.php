@extends('layouts.app')

@section('content')
  <div class="home-page">
      @component('components.hero', [
          'img_bg' => '/img/hero_bg.png',
          'img_content' => '/img/logo_north-star_team_experts.svg'
      ])
      @section('hero-content')
        @parent
        <article class="home__hero--right-side">
        @component('components.blocks.login')
        @endcomponent
        </article>
      @endsection
    @endcomponent
  </div>
@endsection
