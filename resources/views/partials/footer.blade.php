<footer class="footer">
  <div class="container">
    <div class="footer__content has-color-white d-flex align-items-start">


      <div class="footer__logo">
        <img src="{{ asset('img/logo_north-star.svg') }}" alt="North Star Water treatment systems">
        <div class="footer-contact">contact : <a class="has-color-white" href="mailto:info@north-star.fr">info@north-star.fr</a></div>
      </div>

      <div class="footer__block--right flex flex-column">

        <div class="footer__paiement flex">
          <div class="paiement__logos align-items-end">
            <span class="paiement__logo paiement__logo--visa">Visa</span>
            <span class="paiement__logo paiement__logo--mastercard">MasterCard</span>
            <span class="paiement__logo paiement__logo--amex">American express</span>
          </div>
          <span class="paiement__title has-uppercase has-color-accent">Paiement Sécurisé</span>
        </div>        

        <div class="footer__links flex flex-column">
          @foreach ($pages as $page)
            <a href="{{ url('pages/'.$page->slug) }}" class="footer__link has-uppercase">{{ $page->title }}</a>
          @endforeach
        </div>
        
      </div>   


    </div>
  </div>
</footer>
