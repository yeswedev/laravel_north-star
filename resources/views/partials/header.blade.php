<header class="header-main">
    <div class="container">
        <div class="header-container">


            <div class="header-logo">
                <a href="{{route('home')}}">
                    <img src="{{ asset('/img/logo_north-star.svg') }}" alt="Logotype de North Star.">
                </a>
            </div>

            {{-- HIDE EVERYTHING WHEN USER NOT CONNECTED --}}


                @if (
                    Auth::check()
                )
                    @if (
                        Route::currentRouteName() != 'registration' &&
                        Route::currentRouteName() != 'registration-2' &&
                        Route::currentRouteName() != 'company-registration' &&
                        Route::currentRouteName() != 'registration-3' &&
                        Route::currentRouteName() != 'confirmation-registration' && 
                        Route::currentRouteName() != 'subscription'
                    )
                        <nav id="main-nav" class="header-nav-container">
                            <ul class="header-nav">
                                <li class="header-nav__item">
                                <a href="{{route('shop')}}">{{__('Ma boutique')}}</a>
                                </li>
                                <li class="header-nav__item">
                                <a href="{{route('contacts')}}">{{__('Mon contact')}}</a>
                                </li>
                            </ul>
                        </nav>
    
                        @include('spark::nav.user')
    
                        <div class="header-cart">
                            <a href="{{route('cart')}}">
                                <i class="fas fa-shopping-cart"></i>                    
                                <header-cart></header-cart>
                                <div class="header-cart__tooltip">
                                    <p>{{__('Consultez votre panier')}}</p>
                                </div>
                            </a>
                        </div>
                        
                        {{-- Only on LG break --}}
                        <div id="main-burger" class="header-burger-container">
                            <div class="header-burger"></div>
                        </div>
                    @else 
                        <div class="user-icon-only">
                            @include('spark::nav.user')
                        </div>
                    @endif
                @endif


        </div>
    </div>
</header>
