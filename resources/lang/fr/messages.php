<?php
return [
    'If you’re having trouble clicking the :actionText button, copy and paste the URL below\n' => 'Si le bouton  :actionText ci-dessus ne fonctionne pas, veuillez copier-coller le liens ci-dessous \n',
    'All rights reserved.'=>'Tous Droits réservés',
];
