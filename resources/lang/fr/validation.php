<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Le champs :attribute doit être accepté.',
    'active_url'           => 'Le champs :attribute ne contient pas une url valide.',
    'after'                => 'Le champs :attribute doit contenir une date postérieure à :date.',
    'after_or_equal'       => 'Le champs :attribute doit être postérieur ou égal à :date.',
    'alpha'                => 'Le champs :attribute doit contenir uniquement des lettres.',
    'alpha_dash'           => 'Le champs :attribute doit contenir uniquement des lettres, chiffres, et tirets.',
    'alpha_num'            => 'Le champs :attribute doit contenir uniquement des lettres et des chiffres.',
    'array'                => 'Le champs :attribute doit être un tableau.',
    'before'               => 'Le champs :attribute doit être une date antérieure à :date.',
    'before_or_equal'      => 'Le champs :attribute doit être une date antérieure ou égale à :date.',
    'between'              => [
        'numeric' => 'Le champs :attribute doit être entre :min et :max.',
        'file'    => 'Le champs :attribute doit être entre :min et :max kilobytes.',
        'string'  => 'Le champs :attribute doit être entre :min et :max caractères.',
        'array'   => 'Le champs :attribute doit avoir entre :min et :max éléments.',
    ],
    'boolean'              => 'Le champs :attribute doit être vrai ou faux.',
    'confirmed'            => 'La confirmation du champs :attribute ne correspond pas.',
    'country'              => 'Le champs :attribute n\'est pas un pays valide.',
    'date'                 => 'Le champs :attribute n\'est pas une date valide.',
    'date_equals'          => 'Le champs :attribute doit être une date égale à :date.',
    'date_format'          => 'Le champs :attribute ne correspond pas au format :format.',
    'different'            => 'Le champs :attribute et :other doivent être différents.',
    'digits'               => 'Le champs :attribute doit être de :digits chiffres.',
    'digits_between'       => 'Le champs :attribute doit être entre :min et :max chiffres.',
    'distinct'             => 'Le champs :attribute à une valeur dupliquée.',
    'email'                => 'Le champs :attribute doit être une adresse email valide.',
    'exists'               => 'Le champs :attribute selectionné est invalide.',
    'filled'               => 'Le champs :attribute est requis.',
    'gt' => [
        'numeric' => 'Le champs :attribute doit être plus grand que :value.',
        'file'    => 'Le champs :attribute doit être plus grand que :value kilobytes.',
        'string'  => 'Le champs :attribute doit être plus grand que :value caractères.',
        'array'   => 'Le champs :attribute doit avoir plus de :value éléments.',
    ],
    'gte' => [
        'numeric' => 'Le champs :attribute doit être plus grand ou égal à :value.',
        'file'    => 'Le champs :attribute doit être plus grand ou égal à :value kilobytes.',
        'string'  => 'Le champs :attribute doit être plus grand ou égal à :value caractères.',
        'array'   => 'Le champs :attribute doit avoir :value éléments ou plus.',
    ],
    'image'                => 'Le champs :attribute doit être une image.',
    'in'                   => 'Le champs selected :attribute est invalide.',
    'in_array'             => 'Le champs :attribute n\'existe pas dans :other.',
    'integer'              => 'Le champs :attribute doit être un entier.',
    'ip'                   => 'Le champs :attribute must be a valid IP address.',
    'ipv4'                 => 'Le champs :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'Le champs :attribute must be a valid IPv6 address.',
    'json'                 => 'Le champs :attribute must be a valid JSON string.',
    'lt' => [
        'numeric' => 'Le champs :attribute must be less than :value.',
        'file'    => 'Le champs :attribute must be less than :value kilobytes.',
        'string'  => 'Le champs :attribute must be less than :value characters.',
        'array'   => 'Le champs :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'Le champs :attribute must be less than or equal :value.',
        'file'    => 'Le champs :attribute must be less than or equal :value kilobytes.',
        'string'  => 'Le champs :attribute must be less than or equal :value characters.',
        'array'   => 'Le champs :attribute must not have more than :value items.',
    ],
    'max'                  => [
        'numeric' => 'Le champs :attribute doit être plus peti que :max.',
        'file'    => 'Le champs :attribute may not be greater than :max kilobytes.',
        'string'  => 'Le champs :attribute ne doit pas dépasser :max caractères.',
        'array'   => 'Le champs :attribute may not have more than :max items.',
    ],
    'mimes'                => 'Le champs :attribute must be a file of type: :values.',
    'mimetypes'            => 'Le champs :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'Le champs :attribute must be at least :min.',
        'file'    => 'Le champs :attribute must be at least :min kilobytes.',
        'string'  => 'Le champs :attribute doit comporter au minimum :min caractères.',
        'array'   => 'Le champs :attribute must have at least :min items.',
    ],
    'not_in'               => 'Le champs selected :attribute is invalid.',
    'not_regex'            => 'Le format du champs :attribute est invalide.',
    'numeric'              => 'Le champs :attribute doit être un nombre.',
    'present'              => 'Le champs :attribute field must be present.',
    'regex'                => 'Le format du champs :attribute est invalide.',
    'required'             => 'Le champs :attribute est obligatoire.',
    'required_if'          => 'Le champs :attribute field is required when :other is :value.',
    'required_unless'      => 'Le champs :attribute field is required unless :other is in :values.',
    'required_with'        => 'Le champs :attribute field is required when :values is present.',
    'required_with_all'    => 'Le champs :attribute field is required when :values is present.',
    'required_without'     => 'Le champs :attribute field is required when :values is not present.',
    'required_without_all' => 'Le champs :attribute field is required when none of :values are present.',
    'same'                 => 'Le champs :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'Le champs :attribute must be :size.',
        'file'    => 'Le champs :attribute must be :size kilobytes.',
        'string'  => 'Le champs :attribute must be :size characters.',
        'array'   => 'Le champs :attribute must contain :size items.',
    ],
    'starts_with'          => 'Le champs :attribute must start with one of the following: :values',
    'state'                => 'This state is not valid for the specified country.',
    'string'               => 'Le champs :attribute must be a string.',
    'timezone'             => 'Le champs :attribute must be a valid zone.',
    'unique'               => 'Le champs :attribute has already been taken.',
    'uploaded'             => 'Le champs :attribute failed to upload.',
    'url'                  => 'Le champs :attribute format is invalid.',
    'vat_id'               => 'This VAT identification number is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'otp' => 'This token is invalid',
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'team' => 'team'
    ],

];
