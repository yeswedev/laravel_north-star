<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Le mot de passe doit contenir au minimum 6 caractères et correspondre à la confirmation.',
    'reset' => 'Votre mot de passe a été réinitialisé!',
    'sent' => 'Nous vous avons envoyé un lien de réinitialisation',
    'token' => 'Le jeton de réinitialisation est invalide.',
    'user' => "Nous ne trouvons pas d'utilisateur avec cet adresse email.",
    'email adress' => "adresse email",
    'This password reset link will expire in :count minutes.' => "Ce lien expirera dans :count minutes."


];
