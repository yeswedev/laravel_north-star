<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'L\'authentification a échouée.',
    'throttle' => 'Trop de connexion en même temps. Veuillez réessayer dans :seconds secondes.',
    'email' => 'Cet email n\'est pas enregistré,veuillez vérifier que vous avez saisi la bonne adresse ou vous inscrire en cliquant sur le bouton "abonnez-vous"',
    'password' => 'Le mot de passe ne correspond pas, veuillez réessayer ou cliquer sur le lien "mot de passe oublié"'

];
