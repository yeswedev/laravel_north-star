
/*
 |--------------------------------------------------------------------------
 | Laravel Spark Bootstrap
 |--------------------------------------------------------------------------
 |
 | First, we will load all of the "core" dependencies for Spark which are
 | libraries such as Vue and jQuery. This also loads the Spark helpers
 | for things such as HTTP calls, forms, and form validation errors.
 |
 | Next, we'll create the root Vue application for Spark. This will start
 | the entire application and attach it to the DOM. Of course, you may
 | customize this script as you desire and load your own components.
 |
 */

/**
 * Vue
 */

import store from './vuex/store';



/**
 * Javascript dependencies
 */
require('spark-bootstrap');
require('./components/bootstrap');
require('./bootstrap');
require('es6-promise');
require('datatables.net-dt');


/**
 * Launch Vue
 */

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component(
//     'example-component',
//     require('./components/ExampleComponent.vue').default
// );

const app = new Vue({
    mixins: [require('spark')],
    el: '#app',
    store,
});


/**
 * Custom
 */
import './partials/header';
import './user/account';

