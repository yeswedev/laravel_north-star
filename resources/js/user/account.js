document.addEventListener("DOMContentLoaded", function(event) { 


    // BEGIN PAGED TABLE
    $('#table-billings').DataTable({
        lengthChange: false,
        pageLength: 6,
        bFilter: false, //hide Search bar
        bInfo: false, // hide showing entries
        //Set DOM order : https://datatables.net/examples/basic_init/dom.html
        dom: 'pt',
        order: [[1, "asc"]],
        language: {
            processing:     "Traitement en cours...",
            search:         "Rechercher&nbsp;:",
            lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
            info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix:    "",
            loadingRecords: "Chargement en cours...",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable:     "Aucune donnée disponible dans le tableau",
            paginate: {
                first:      "Premier",
                previous:   "Pr&eacute;c&eacute;dent",
                next:       "Suivant",
                last:       "Dernier"
            },
            aria: {
                sortAscending:  ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
    });

    $('#table-billings').DataTable().on('draw', function() {
        $('.paginate_button.previous, .paginate_button.next').empty();
    })

    // Remove text in previous/next buttons
    $('.paginate_button.previous, .paginate_button.next').empty();
    // ENDS PAGED TABLE


    // BEGIN TABLE SMALL SHOW / HIDE
    $('.table-item__head').on('click', toggleTableItemContent);

    function toggleTableItemContent() {
        $(this).next('.table-item__content').toggleClass('active');
    }
    // ENDS TABLE SMALL SHOW / HIDE


    // BEGIN FILE UPLOAD
    $('#show_billing_upload').on('click', toggleUploadBilling);

    function toggleUploadBilling() {
        $('#billing_upload').addClass('active');
        $('#show_billing_upload').attr('disabled', true);
    }
    // ENDS FILE UPLOAD


});