document.addEventListener("DOMContentLoaded", function(event) { 


    var mainBurger = $("#main-burger");
    var mainNav = $("#main-nav");

    mainBurger.on('click', function() {
        mainBurger.toggleClass("active");
        mainNav.toggleClass("active");
    });


});