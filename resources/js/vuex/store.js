import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        products: [],
        totalQty: 0,
        points: 0,
        pointsUsed: 0,
        pointsLeft: 0,
        pointsMini:250,
        message: {
            classPop: 'no',
            class: 'info',
            text: ''
        }
    },
    mutations: {
        setProduct(state, payload) {
            state.products.push(payload.product);
        },
        setProducts(state, payload) {
            state.products = payload.products;
        },
        setTotalQty(state, payload) {
            state.totalQty = payload.totalQty;
        },
        setProductQty(state, payload) {
            let productIndex = state.products.findIndex(function(element) {
                return element.product.id == payload.productId;
            });
            let product = state.products[productIndex];
            let newQty;
            switch(payload.newQty) {
                case 'down' :
                    newQty = product.quantity - 1;
                break;
                case 'up' :
                    newQty = product.quantity + 1;
                break;
            }
            Vue.set(product, 'quantity', newQty);
        },
        deleteProduct(state, payload) {
            let productIndex = state.products.findIndex(function(element) {
                return element.product.id == payload.productId;
            });
            Vue.delete(state.products, productIndex);
        },
        setPoints(state, payload) {
            state.points = payload.points;
        },
        setPointsUsed(state, payload) {
            state.pointsUsed = payload.pointsUsed;
        },
        setPointsLeft(state, payload) {
            state.pointsLeft = payload.pointsLeft;
        },
        setMessage(state, payload) {
            state.message = payload.message;
        }
    },
    actions: {
        loadProducts(context, payload) {
            axios
                .get('/api/cart/products')
                .then(response => {
                    if ( response.status === 200 ) {
                        context.commit({
                            type: 'setProducts',
                            products: response.data,
                        });
                        this.dispatch({ type: 'loadTotalQty' })
                        this.dispatch({ type: 'loadPointsUsed' })
                        this.dispatch({ type: 'loadPointsLeft' })
                    }
                })
                .catch(error => {
                    console.log(error);
                });
        },
        loadTotalQty(context, payload) {
            var totalQtyLoad = 0;
            this.state.products.forEach(item => {
                totalQtyLoad = totalQtyLoad + +item.quantity;
            });
            context.commit({
                type: 'setTotalQty',
                totalQty: totalQtyLoad
            })
        },
        addProduct(context, payload) {
            axios
                .post('/api/cart/product', {
                    id: payload.product.id,
                    quantity: payload.product.quantity
                })
                .then(response => {
                    if (response.status == 200) {
                        this.dispatch({ type: 'loadProducts' })
                        context.commit({
                            type: 'setMessage',
                            message: {
                                classPop: 'yes',
                                class: 'valid',
                                text: 'Produit(s) ajouté(s) au panier avec succès.'
                            }
                        });
                    }
                })
                .catch(error => {
                    console.log(error);
                    context.commit({
                        type: 'setMessage',
                        message: {
                            classPop: 'yes',
                            class: 'invalid',
                            text: 'Une erreur s\'est produite lors de l\'ajout au panier.'
                        }
                    });
                });
        },
        updateProductQty(context, payload) {
            axios
                .put('/api/cart/product', {
                    id: payload.productId,
                    newQty: payload.newQty
                })
                .then(response => {
                    if(response.status == 200) {
                        context.commit({
                            type: 'setProductQty',
                            productId: payload.productId,
                            newQty: payload.newQty
                        });
                        this.dispatch({ type: 'loadTotalQty' })
                        this.dispatch({ type: 'loadPointsUsed' })
                        this.dispatch({ type: 'loadPointsLeft' })
                    }
                })
                .catch(error => {
                    console.log(error);
                });
        },
        deleteProduct(context, payload) {
            axios
                .put('/api/cart/product/delete', {
                    id: payload.productId
                })
                .then(response => {
                    if (response.status == 200) {
                        context.commit({
                            type: 'deleteProduct',
                            productId: payload.productId
                        });
                        this.dispatch({ type: 'loadTotalQty' })
                        this.dispatch({ type: 'loadPointsUsed' })
                        this.dispatch({ type: 'loadPointsLeft' })
                    }
                })
                .catch(error => {
                    console.log(error);
                });
        },
        // deleteAll(context, payload) {
        //     axios
        //         .put('/api/cart/products/delete', {
        //             data: {
        //                 id: payload.productId
        //             }
        //         })
        //         .then(response => {
        //             if (response.status == 200) {
                        // this.dispatch({ type: 'loadPoints' })
                        // this.dispatch({ type: 'loadProducts' })
        //             }
        //         })
        //         .catch(error => {
        //             console.log(error);
        //         });
        // },
        loadPoints(context, payload) {
            axios
                .get('/api/user/points')
                .then(response => {
                    if ( response.status === 200 ) {
                        context.commit({
                            type: 'setPoints',
                            points: response.data
                        })
                    }
                })
                .catch(error => {
                    console.log(error);
                });
        },
        loadPointsUsed(context, payload) {
            let pointsUsed = 0;
            this.state.products.forEach(item => {
                let totalPrice = item.product.price * item.quantity;
                pointsUsed = pointsUsed + totalPrice;
            });
            context.commit({
                type: 'setPointsUsed',
                pointsUsed: pointsUsed
            });
        },
        loadPointsLeft(context, payload) {
            let pointsLeft = this.state.points - this.state.pointsUsed;
            context.commit({
                type: 'setPointsLeft',
                pointsLeft: pointsLeft
            });
        },
        validateOrder(context, payload) {
            if (this.state.pointsUsed >= this.state.pointsMini) {
                axios
                    .put('/api/cart/validate')
                    .then(response => {
                        if (response.status == 200) {
                            this.dispatch({ type: 'loadPoints' })
                            this.dispatch({ type: 'loadProducts' })

                            if(response.data == 'ok'){                                
                                this.dispatch({ type: 'loadPoints' })
                                this.dispatch({ type: 'loadProducts' })
                                context.commit({
                                    type: 'setMessage',
                                    message: {
                                        classPop: 'yes',
                                        class: 'valid',
                                        text: 'Votre commande à bien été prise en compte. Elle est maintenant en attente de validation.'
                                    }
                                });
                            }else{
                                context.commit({
                                    type: 'setMessage',
                                    message: {
                                        classPop: 'yes',
                                        class: 'invalid',
                                        text: 'Votre commande n\'a pas pu être validée. Veuillez vérifier que vos points disponibles sont suffisants ou que votre commande dépasse le minimum de commande de '+this.state.pointsMini+'.'
                                    }
                                });
                            }
                        }
                    })
                    .catch(error => {
                        console.log(error);
                    })
            } else {
                context.commit({
                    type: 'setMessage',
                    message: {
                        classPop: 'yes',
                        class: 'invalid',
                        text: 'Votre commande doit être de '+this.state.pointsMini+' points minimum.'
                    }
                });
            }
        }
    },
});

export default store;
