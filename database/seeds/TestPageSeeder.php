<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            'title' => 'Page d\'exemple',
            'slug' => 'page-exemple',
            'content' => "<h1>Titre d'exemple</h1><div>Texte d'exemple</div>",
            'published' => true,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
    }
}
