<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            'reference' => 'ref123456',
            'user_id' => 2,
            'status' => 0,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
    }
}
