<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

use App\User;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Check that the admin account exists, create it if missing
    	$email = config('admin.email');
    	$password = config('admin.password');
    	$name = config('admin.name');
    	$user = User::firstOrNew([ 'email' => $email ]);
    	$user->fill([
                'password' => Hash::make($password),
                'name'     => $name,
            ]);
        $user->save;
        // ByPass email verification
        $user->email_verified_at=Carbon::now();
        $user->save();
    }
}
