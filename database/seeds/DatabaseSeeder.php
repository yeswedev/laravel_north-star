<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminUserSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(DefaultSalerSeeder::class);
        $this->call(DepartmentsTableSeeder::class);
        $this->call(TestPageSeeder::class);
        $this->call(DefaultPagesSeeder::class);

        if(env('APP_ENV')!='production'){
            $this->call(TestUserSeeder::class);
            $this->call(TestProductSeeder::class);
            $this->call(TestVariantSeeder::class);
            $this->call(TestBillSeeder::class);
            // $this->call(TestOrderSeeder::class);
            // $this->call(TestOrderVariantSeeder::class);
            $this->call(TestSubscriptionsSeeder::class);
            $this->call(TestSalerSeeder::class);
        }
    }
}
