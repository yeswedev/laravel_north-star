<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestSubscriptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $updated_at = date('Y-m-d');
        $ends_at = date('Y-m-d', strtotime('+1 year', strtotime($updated_at)) );
        DB::table('subscriptions')->insert([
            'user_id' => 2,
            'name' => 'default',
            'stripe_id' => 'sub_F4RgGRdRn3b5kf',
            'stripe_plan' => 'plan_GBNPzw286Qudty',
            'quantity' => 1,
            'ends_at' => $ends_at,
            'created_at' => date('Y-m-d'),
            'updated_at' => $updated_at,
        ]);
    }
}
