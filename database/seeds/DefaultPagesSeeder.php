<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DefaultPagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            'title' => 'Mentions Légales',
            'slug' => 'mentions-legales',
            'content' => "<h1>Mentions Légales</h1><div>Texte d'exemple</div>",
            'published' => true,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('pages')->insert([
            'title' => 'Conditions Générales de Vente',
            'slug' => 'conditions-generales-de-vente',
            'content' => "<h1>Conditions Générales de Vente</h1><div>Texte d'exemple</div>",
            'published' => true,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('pages')->insert([
            'title' => 'Charte des membres',
            'slug' => 'charte-des-membres',
            'content' => "<h1>Charte des membres</h1><div>Texte d'exemple</div>",
            'published' => true,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
    }
}
