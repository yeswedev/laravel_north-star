<?php

use App\User;

use App\Saler;
use App\Department;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class TestSalerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $salers = [
            [
                'firstname' =>'John',
                'lastname'  =>'Dae',
                'email'     => 'john.doe1@ns.fr',
                'mobile'    => '0123456789',
                'phone'     => '0645123789',
                'dpts'       => [51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,76,79,80,81,82,83,84,85,86,87,88,89,90,'2A','2B',971,972,973,974,976],
            ],
                        [
                'firstname' =>'John',
                'lastname'  =>'De',
                'email'     => 'john.doe2@ns.fr',
                'mobile'    => '0123456789',
                'phone'     => '0645123789',
                'dpts'       => [41,42,43,44,45,46,47,48,49,50],
            ],
                        [
                'firstname' =>'John',
                'lastname'  =>'Die',
                'email'     => 'john.doe3@ns.fr',
                'mobile'    => '0123456789',
                'phone'     => '0645123789',
                'dpts'       => [31,32,33,34,35,36,37,38,39,40],
            ],
                        [
                'firstname' =>'John',
                'lastname'  =>'Doe',
                'email'     => 'john.doe4@ns.fr',
                'mobile'    => '0123456789',
                'phone'     => '0645123789',
                'dpts'       => [21,22,23,24,25,26,27,28,29,30],
            ],
                        [
                'firstname' =>'John',
                'lastname'  =>'Due',
                'email'     => 'john.doe5@ns.fr',
                'mobile'    => '0123456789',
                'phone'     => '0645123789',
                'dpts'       => [11,12,13,14,15,16,17,18,19,20]
            ],
                        [
                'firstname' =>'John',
                'lastname'  =>'Dye',
                'email'     => 'john.doe6@ns.fr',
                'mobile'    => '0123456789',
                'phone'     => '0645123789',
                'dpts'       => [2,4,6,8,10]
            ],
                        [
                'firstname' =>'John',
                'lastname'  =>'Doye',
                'email'     => 'john.doe7@ns.fr',
                'mobile'    => '0123456789',
                'phone'     => '0645123789',
                'dpts'       => [1,3,5,7,9,94,75,91,92,93,95,78,77]
            ],
        ];
        foreach($salers as $saler){
            $user = User::firstOrNew([ 'email' => $saler['email'] ]);
            $user->fill([
                'password'  => Hash::make('test123'),
                'name'      => 'Saler',
                'firstname' => $saler['firstname'],
                'lastname'  => $saler['lastname'],
                'mobile'    => $saler['mobile'],
                'phone'     => $saler['phone'],
            ]);
            $user->setRole('saler');
            $user->save();
            foreach($saler['dpts'] as $dpt){
                $d=Department::find($dpt);
                if(!empty($d)){
                    $d->saler()->associate($user->saler);
                    $d->save();
                }
            }
        }
    }
}
