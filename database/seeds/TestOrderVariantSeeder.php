<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestOrderVariantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_variant')->insert([
            'quantity' => 5,
            'order_id' => 1,
            'variant_id' => 1,
        ]);
    }
}
