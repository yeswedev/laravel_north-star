<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory;

class TestProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        
        DB::table('products')->insert([
            'name' => 'Casquette',
            'image' => $faker->image('storage/app/public/products',400,300, 'cats', false),
        ]);
    }
}
