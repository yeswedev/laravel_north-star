<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\User;

class TestUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $members=[
            [
                'name' => 'Bob & Co',
                'email' => 'bob@dupont.fr',
                'password' => bcrypt('bobdupont'),
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
                'title' => 'Mr',
                'firstname' => 'Bob',
                'lastname' => 'Dupont',
                'function' => 'Membre Test',
                'birth' => '1980-12-29',
                'phone' => '0202020202',
                'mobile' => '0606060606',
                'department_id' => '75',
                'company_address' => '23 rue du Lorem',
                'company_zipcode' => '75000',
                'company_city' => 'Paris',
                'company_siret' => '123456789123',
                'company_ape' => '12345Z',
                'company_tva' => '123456789012',
                'billing_address' => '23 rue du Lorem',
                'billing_city' => 'Paris',
                'billing_zip' => '75000',
                'cgv_agrement' => 1,
                'role_id' => 2,
                'size' => 'L',
            ]
        ];
        foreach($members as $member){
            $user=new User(
                $member
            );
            $user->setDepartment();
            $user->setRole('member');
            $user->save();
        }
    }
}
