<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Saler;

class DefaultSalerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultUser=User::first();
        $saler=new Saler(['title'=>'contact']);
        $saler->user()->associate($defaultUser);
        $saler->save();
    }
}
