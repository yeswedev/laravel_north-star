<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory;

class TestBillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        DB::table('bills')->insert([
            'user_id' => 2,
            'status' => 1,
            'doc' => $faker->image('storage/app/public/bills',400,300, 'cats', false),
            'points' => 1110,
            'amount' => 5550.00,
            'nb_product_UD' => 1,
            'nb_product_UD_plus' => 0,
            'nb_product_NSC_hybrid' => 0,
        ]);
    }
}
