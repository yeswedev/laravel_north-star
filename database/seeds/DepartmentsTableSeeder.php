<?php

use Illuminate\Database\Seeder;

use App\Saler;
use App\Department;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dpts=[
          [
            'id' => '1' ,
            'name' => 'Ain'
          ],
          [
            'id' => '10' ,
            'name' => 'Aube'
          ],
          [
            'id' => '11' ,
            'name' => 'Aude'
          ],
          [
            'id' => '12' ,
            'name' => 'Aveyron'
          ],
          [
            'id' => '13' ,
            'name' => 'Bouches du Rhone'
          ],
          [
            'id' => '14' ,
            'name' => 'Calvados'
          ],
          [
            'id' => '15' ,
            'name' => 'Cantal'
          ],
          [
            'id' => '16' ,
            'name' => 'Charente'
          ],
          [
            'id' => '17' ,
            'name' => 'Charente Maritime'
          ],
          [
            'id' => '18' ,
            'name' => 'Cher'
          ],
          [
            'id' => '19' ,
            'name' => 'Correze'
          ],
          [
            'id' => '2' ,
            'name' => 'Aisne'
          ],
          [
            'id' => '21' ,
            'name' => 'Cote d\'Or'
          ],
          [
            'id' => '22' ,
            'name' => 'Cotes d\'Armor'
          ],
          [
            'id' => '23' ,
            'name' => 'Creuse'
          ],
          [
            'id' => '24' ,
            'name' => 'Dordogne'
          ],
          [
            'id' => '25' ,
            'name' => 'Doubs'
          ],
          [
            'id' => '26' ,
            'name' => 'Drome'
          ],
          [
            'id' => '27' ,
            'name' => 'Eure'
          ],
          [
            'id' => '28' ,
            'name' => 'Eure-et-Loir'
          ],
          [
            'id' => '29' ,
            'name' => 'Finistere'
          ],
          [
            'id' => '2A' ,
            'name' => 'Corse du Sud'
          ],
          [
            'id' => '2B' ,
            'name' => 'Haute-Corse'
          ],
          [
            'id' => '3' ,
            'name' => 'Allier'
          ],
          [
            'id' => '30' ,
            'name' => 'Gard'
          ],
          [
            'id' => '31' ,
            'name' => 'Haute-Garonne'
          ],
          [
            'id' => '32' ,
            'name' => 'Gers'
          ],
          [
            'id' => '33' ,
            'name' => 'Gironde'
          ],
          [
            'id' => '34' ,
            'name' => 'Herault'
          ],
          [
            'id' => '35' ,
            'name' => 'Ille-et-Vilaine'
          ],
          [
            'id' => '36' ,
            'name' => 'Indre'
          ],
          [
            'id' => '37' ,
            'name' => 'Indre-et-Loire'
          ],
          [
            'id' => '38' ,
            'name' => 'Isere'
          ],
          [
            'id' => '39' ,
            'name' => 'Jura'
          ],
          [
            'id' => '4' ,
            'name' => 'Alpes de Haute-Provence'
          ],
          [
            'id' => '40' ,
            'name' => 'Landes'
          ],
          [
            'id' => '41' ,
            'name' => 'Loir-et-Cher'
          ],
          [
            'id' => '42' ,
            'name' => 'Loire'
          ],
          [
            'id' => '43' ,
            'name' => 'Haute-Loire'
          ],
          [
            'id' => '44' ,
            'name' => 'Loire-Atlantique'
          ],
          [
            'id' => '45' ,
            'name' => 'Loiret'
          ],
          [
            'id' => '46' ,
            'name' => 'Lot'
          ],
          [
            'id' => '47' ,
            'name' => 'Lot-et-Garonne'
          ],
          [
            'id' => '48' ,
            'name' => 'Lozere'
          ],
          [
            'id' => '49' ,
            'name' => 'Maine-et-Loire'
          ],
          [
            'id' => '5' ,
            'name' => 'Hautes-Alpes'
          ],
          [
            'id' => '50' ,
            'name' => 'Manche'
          ],
          [
            'id' => '51' ,
            'name' => 'Marne'
          ],
          [
            'id' => '52' ,
            'name' => 'Haute-Marne'
          ],
          [
            'id' => '53' ,
            'name' => 'Mayenne'
          ],
          [
            'id' => '54' ,
            'name' => 'Meurthe-et-Moselle'
          ],
          [
            'id' => '55' ,
            'name' => 'Meuse'
          ],
          [
            'id' => '56' ,
            'name' => 'Morbihan'
          ],
          [
            'id' => '57' ,
            'name' => 'Moselle'
          ],
          [
            'id' => '58' ,
            'name' => 'Nievre'
          ],
          [
            'id' => '59' ,
            'name' => 'Nord'
          ],
          [
            'id' => '6' ,
            'name' => 'Alpes-Maritimes'
          ],
          [
            'id' => '60' ,
            'name' => 'Oise'
          ],
          [
            'id' => '61' ,
            'name' => 'Orne'
          ],
          [
            'id' => '62' ,
            'name' => 'Pas-de-Calais'
          ],
          [
            'id' => '63' ,
            'name' => 'Puy-de-Dome'
          ],
          [
            'id' => '64' ,
            'name' => 'Pyrenees-Atlantiques'
          ],
          [
            'id' => '65' ,
            'name' => 'Hautes-Pyrenees'
          ],
          [
            'id' => '66' ,
            'name' => 'Pyrenees-Orientales'
          ],
          [
            'id' => '67' ,
            'name' => 'Bas-Rhin'
          ],
          [
            'id' => '68' ,
            'name' => 'Haut-Rhin'
          ],
          [
            'id' => '69' ,
            'name' => 'Rhone'
          ],
          [
            'id' => '7' ,
            'name' => 'Ardeche'
          ],
          [
            'id' => '70' ,
            'name' => 'Haute-Saone'
          ],
          [
            'id' => '71' ,
            'name' => 'Saone-et
            -Loire'],
          [
            'id' => '72' ,
            'name' => 'Sarthe'
          ],
          [
            'id' => '73' ,
            'name' => 'Savoie'
          ],
          [
            'id' => '74' ,
            'name' => 'Haute-Savoie'
          ],
          [
            'id' => '75' ,
            'name' => 'Paris'
          ],
          [
            'id' => '76' ,
            'name' => 'Seine-Maritime'
          ],
          [
            'id' => '77' ,
            'name' => 'Seine-et-Marne'
          ],
          [
            'id' => '78' ,
            'name' => 'Yvelines'
          ],
          [
            'id' => '79' ,
            'name' => 'Deux-Sevres'
          ],
          [
            'id' => '8' ,
            'name' => 'Ardennes'
          ],
          [
            'id' => '80' ,
            'name' => 'Somme'
          ],
          [
            'id' => '81' ,
            'name' => 'Tarn'
          ],
          [
            'id' => '82' ,
            'name' => 'Tarn-et
            -Garonne'],
          [
            'id' => '83' ,
            'name' => 'Var'
          ],
          [
            'id' => '84' ,
            'name' => 'Vaucluse'
          ],
          [
            'id' => '85' ,
            'name' => 'Vendee'
          ],
          [
            'id' => '86' ,
            'name' => 'Vienne'
          ],
          [
            'id' => '87' ,
            'name' => 'Haute-Vienne
            '],
          [
            'id' => '88' ,
            'name' => 'Vosges'
          ],
          [
            'id' => '89' ,
            'name' => 'Yonne'
          ],
          [
            'id' => '9' ,
            'name' => 'Ariege'
          ],
          [
            'id' => '90' ,
            'name' => 'Territoire-de-Belfort'
          ],
          [
            'id' => '91' ,
            'name' => 'Essonne'
          ],
          [
            'id' => '92' ,
            'name' => 'Hauts-de-Seine'
          ],
          [
            'id' => '93' ,
            'name' => 'Seine-St-Denis'
          ],
          [
            'id' => '94' ,
            'name' => 'Val-de
            -Marne'],
          [
            'id' => '95' ,
            'name' => 'Val-dOise
            '],
          [
            'id' => '971' ,
            'name' => 'Guadeloupe'
          ],
          [
            'id' => '972' ,
            'name' => 'Martinique'
          ],
          [
            'id' => '973' ,
            'name' => 'Guyane'
          ],
          [
            'id' => '974' ,
            'name' => 'La Reunion
            '],
          [
            'id' => '976' ,
            'name' => 'Mayotte'
          ],
        ];

        foreach($dpts as $item){
          $dpt=new Department();
          $dpt->id=$item['id'];
          $dpt->name=$item['name'];
          $defaultSaler=Saler::first();
          $dpt->saler()->associate($defaultSaler);
          $dpt->save();
        }
    }
}
