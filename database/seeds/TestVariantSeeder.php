<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestVariantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('variants')->insert([
            'stock' => 100,
            'break_point' => 50,
            'points' => 110,
            'name' => 'S',
            'product_id' => 1,
        ]);

        DB::table('variants')->insert([
            'stock' => 30,
            'break_point' => 50,
            'points' => 110,
            'name' => 'M',
            'product_id' => 1,
        ]);

        DB::table('variants')->insert([
            'stock' => 0,
            'break_point' => 50,
            'points' => 110,
            'name' => 'L',
            'product_id' => 1,
        ]);
    }
}
