<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\User;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles=[
          'admin',
          'member',
          'saler'
        ];
        foreach($roles as $role){
          $r=new Role([
            'name' => $role
          ]);
          $r->save();
        };
        $admin=User::where('email',config('admin.email'))->first();
        $admin->setRole('admin');
        $admin->save();
    }
}
