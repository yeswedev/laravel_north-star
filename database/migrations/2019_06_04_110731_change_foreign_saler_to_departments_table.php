<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeForeignSalerToDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Can't change foreign key, has to drop it before
        if (Schema::hasColumn('departments', 'saler_id')) {
            Schema::table('departments', function(Blueprint $table) {
                $table->dropForeign(['saler_id']);
                $table->dropColumn('saler_id');
            });
        }
        // New foreign key
        Schema::table('departments', function (Blueprint $table) {            
            $table->integer('saler_id')->unsigned()->nullable();
            $table->foreign('saler_id')->references('id')->on('salers')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('departments', function (Blueprint $table) {
            //
        });
    }
}
