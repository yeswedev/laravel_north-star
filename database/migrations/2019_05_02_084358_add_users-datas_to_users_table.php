<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersDatasToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('title')->nullable();
            $table->char('firstname')->nullable();
            $table->char('lastname')->nullable();
            $table->char('function')->nullable();
            $table->date('birth')->nullable();
            $table->char('mobile',25)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('firstname');
            $table->dropColumn('lastname');
            $table->dropColumn('function');
            $table->dropColumn('birth');
            $table->dropColumn('mobile');
        });
    }
}
