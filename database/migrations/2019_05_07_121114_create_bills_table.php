<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('user_id')->unsigned();
            $table->tinyInteger('status')->default(0);
            $table->string('doc', 255);
            $table->integer('points')->nullable();
            $table->integer('produit1')->nullable();
            $table->integer('produit2')->nullable();
            $table->integer('produit3')->nullable();
        });

        Schema::table('bills', function($table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bills', function($table) {
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('bills');
    }
}
