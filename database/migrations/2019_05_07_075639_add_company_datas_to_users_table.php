<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyDatasToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('department_id')->nullable();
            $table->string('company_address')->nullable();
            $table->string('company_address_line_2')->nullable();
            $table->char('company_zipcode')->nullable();
            $table->char('company_city')->nullable();
            $table->char('company_siret')->nullable();
            $table->char('company_ape')->nullable();
            $table->char('company_tva')->nullable();
            $table->boolean('cgv_agrement')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('department_id');
            $table->dropColumn('company_address');
            $table->dropColumn('company_address_line_2');
            $table->dropColumn('company_zipcode');
            $table->dropColumn('company_city');
            $table->dropColumn('company_siret');
            $table->dropColumn('company_ape');
            $table->dropColumn('company_tva');
            $table->dropColumn('cgv_agrement');
        });
    }
}
