<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->char('id',3)->unique();
            $table->char('name');
            $table->timestamps();
        });
        Schema::table('departments', function(Blueprint $table) {
            $table->integer('saler_id')->unsigned();
            $table->foreign('saler_id')->references('id')->on('salers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('departments', 'saler_id')) {
            Schema::table('departments', function(Blueprint $table) {
                $table->dropForeign(['saler_id']);
            });
        }        
        Schema::dropIfExists('departments');
    }
}