<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameProductsOnBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bills', function (Blueprint $table) {
            $table->renameColumn('produit1','nb_product_UD');
            $table->renameColumn('produit2','nb_product_UD_plus');
            $table->renameColumn('produit3','nb_product_NSC_hybrid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bills', function (Blueprint $table) {
            $table->renameColumn('nb_product_UD','produit1');
            $table->renameColumn('nb_product_UD_plus','produit2');
            $table->renameColumn('nb_product_NSC_hybrid','produit3');
        });
    }
}
